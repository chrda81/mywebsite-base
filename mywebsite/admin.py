"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2021 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  For more information about permissions, see https://realpython.com/manage-users-in-django-admin/
--------------------------------------------------------------
"""

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from django_otp.admin import AdminSite, OTPAdmin
from django_otp.models import OTPSecrets

# User model alias
User = get_user_model()

# Unregister the provided model admin
admin.site.unregister(User)


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'is_active')

    def get_changelist(self, request, **kwargs):
        changelist = super(CustomUserAdmin, self).get_changelist(request, **kwargs)
        is_superuser = request.user.is_superuser

        # Prevent non-superusers from quick editing users via changelist
        if is_superuser:
            self.list_editable = ('is_staff', 'is_active')
        else:
            self.list_editable = ()

        return changelist

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        is_superuser = request.user.is_superuser
        disabled_fields = set()  # type: Set[str]

        # Global preventions for non-superusers
        if not is_superuser:
            disabled_fields |= {
                'is_active',
                'is_staff',
                'is_superuser',
                'username',
                'groups',
                'user_permissions',
                'last_login',
                'date_joined',
            }

        # # Prevent non-superusers from editing their own permissions
        # if (
        #     not is_superuser
        #     and obj is not None
        #     and obj == request.user
        # ):
        #     disabled_fields |= {
        #         'is_active',
        #     }

        for f in disabled_fields:
            if f in form.base_fields:
                form.base_fields[f].disabled = True

        return form

    def has_change_permission(self, request, obj=None):
        is_superuser = request.user.is_superuser

        # # Prevent non-superusers from editing superusers
        # if (
        #     not is_superuser
        #     and obj is not None
        #     and obj.is_superuser
        # ):
        #     return False

        # # Prevent non-superusers from editing other users password
        # if (
        #     not is_superuser
        #     and obj is not None
        #     and obj != request.user
        #     and request.resolver_match.url_name == 'auth_user_password_change'
        # ):
        #     return False

        # Prevent non-superusers from editing other users
        if (
            not is_superuser
            and obj is not None
            and obj != request.user
        ):
            return False

        return True

    def has_delete_permission(self, request, obj=None):
        is_superuser = request.user.is_superuser

        # Prevent non-superusers from deleting other users
        if not is_superuser:
            return False

        return True


# enable 2FA
if not admin.site.is_registered(OTPSecrets):
    OTPAdmin.enable()
admin.site = AdminSite()
admin.site.login_template = 'django_otp/admin_login.html'
