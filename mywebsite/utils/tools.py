# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# For more information, please visit
# https://www.codingforentrepreneurs.com/blog/a-unique-slug-generator-for-django/

import locale
import random
import string
import unicodedata

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.utils import OperationalError
from django.template import TemplateSyntaxError, engines
from django.utils.encoding import force_text
from django.utils.functional import Promise
from django.utils.text import slugify

import lxml.html
import lxml.html.clean
import pytz

try:
    from reversion.models import Version
except ImproperlyConfigured:
    pass


def get_default_language():
    try:
        if not hasattr(settings, 'PARLER_DEFAULT_LANGUAGE_CODE'):
            settings.PARLER_DEFAULT_LANGUAGE_CODE = settings.LANGUAGE_CODE
        return settings.PARLER_DEFAULT_LANGUAGE_CODE
    except LookupError:
        return settings.LANGUAGE_CODE


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance
    has a model with a slug field and a title character (char) field.
    """

    if new_slug is not None:
        slug = new_slug
    else:
        # standard scenario
        if not hasattr(instance, '_parler_meta'):
            if hasattr(instance, 'title'):
                slug = slugify(instance.title)
            elif hasattr(instance, 'name'):
                slug = slugify(instance.name)

        else:
            # special scenario: instance has parler translations
            lang_code = instance.get_current_language()
            if not lang_code:
                lang_code = locale.getlocale()[0]
                if lang_code and '_' in lang_code:
                    lang_code = lang_code.split('_')[0]

            # get title or name from depending translations table ProductTranslations
            for meta in instance._parler_meta:
                for meta_field in meta.model._meta.get_fields():
                    if meta_field.name == 'title':
                        slug = slugify(instance.safe_translation_getter('title', language_code=lang_code))
                        break
                    elif meta_field.name == 'name':
                        slug = slugify(instance.safe_translation_getter('name', language_code=lang_code))
                        break

    if not slug:
        # handle scenario for restoring from Version model of revision package
        for version in Version.objects.get_for_object(instance):
            if hasattr(version, 'object_repr'):
                slug = slugify(version.object_repr)
                break

    if not slug:
        new_slug = "instance-{randstr}".format(
            randstr=random_string_generator(size=4)
        )
        return unique_slug_generator(instance, new_slug=new_slug)

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug,
            randstr=random_string_generator(size=4)
        )
        return unique_slug_generator(instance, new_slug=new_slug)

    return slug


def tinymce_word_count(html_string):
    doc = lxml.html.fromstring(html_string)
    cleaner = lxml.html.clean.Cleaner(style=True)
    doc = cleaner.clean_html(doc)
    words = doc.text_content()
    words = unicodedata.normalize("NFKD", words)
    count = len(words.split())

    return count


def template_from_string(template_string, using=None):
    """
    Convert a string into a template object,
    using a given template engine or using the default backends
    from settings.TEMPLATES if no engine was specified.
    """
    # This function is based on django.template.loader.get_template,
    # but uses Engine.from_string instead of Engine.get_template.
    chain = []
    engine_list = engines.all() if using is None else [engines[using]]
    for engine in engine_list:
        try:
            return engine.from_string(template_string)
        except TemplateSyntaxError as e:
            chain.append(e)
    raise TemplateSyntaxError(template_string, chain=chain)


def make_tz_aware(dt, tz='UTC', is_dst=None):
    """Add timezone information to a datetime object, only if it is naive."""
    tz = dt.tzinfo or tz
    try:
        tz = pytz.timezone(tz)
    except AttributeError:
        pass
    return tz.localize(dt, is_dst=is_dst)


def get_object_or_none(classmodel, **kwargs):
    try:
        return classmodel.objects.get(**kwargs)
    except (classmodel.DoesNotExist, ObjectDoesNotExist, OperationalError):
        return None


class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_text(obj)
        return super(LazyEncoder, self).default(obj)


class OrderedStack:
    def __init__(self):
        self._items = []

    def extend(self, list):
        self._items.extend(list)

    def push(self, item):
        self._items.append(item)

    def pushBefore(self, existingItem, newItem):
        for index, item in enumerate(self._items):
            if existingItem == item:
                self._items.insert(index, newItem)
                break

    def pushAfter(self, existingItem, newItem):
        for index, item in enumerate(self._items):
            if existingItem == item:
                self._items.insert(index + 1, newItem)
                break

    def pop(self):
        try:
            return self._items.pop()
        except IndexError:
            print("Empty ordered stack")

    def __len__(self):
        return len(self._items)

    def __repr__(self):
        return f"OrderedStack({self._items})"
