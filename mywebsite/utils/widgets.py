# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from __future__ import unicode_literals

import json

from django import forms
from django.conf import settings
from django.forms.utils import flatatt
from django.template.loader import render_to_string
from django.utils.encoding import smart_text
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from jsmin import jsmin
from tinymce import settings as tinymce_settings
from tinymce.widgets import TinyMCE

from mywebsite_home.templatetags import core_settings_tags, websettings_tags


class TinyMCEWidget(TinyMCE):
    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = ""
        final_attrs = self.build_attrs(self.attrs, attrs)
        final_attrs["name"] = name
        if final_attrs.get("class", None) is None:
            final_attrs["class"] = "tinymce"
        else:
            final_attrs["class"] = " ".join(final_attrs["class"].split(" ") + ["tinymce"])
        assert "id" in final_attrs, "TinyMCE widget attributes must contain 'id'"
        mce_config = self.get_mce_config(final_attrs)

        # collect all stylesheets from WebSettings database table
        content_style = ''
        content_css = []
        for stylesheet_settings in core_settings_tags.getCoreSettingItemsFor(_('Script Reference'), 'App Stylesheets'):
            for stylesheet in websettings_tags.getWebSettingsAttribItems(stylesheet_settings):
                # set style content
                if 'content' in websettings_tags.getAttribItemKey(stylesheet):
                    content = websettings_tags.handleWebSettingsType(
                        websettings_tags.getAttribItemValue(stylesheet))
                    content_style += content + '\n'
                # set style urls
                elif 'href' in websettings_tags.getAttribItemKey(stylesheet):
                    content_css.append(websettings_tags.handleWebSettingsType(
                        websettings_tags.getAttribItemValue(stylesheet)))

        # add collected stylesheets to iframe of TinyMCE
        mce_config.update({'content_style': content_style})
        if mce_config.get('content_css', None):
            content_css.extend(mce_config['content_css'])
        mce_config.update({'content_css': content_css})

        mce_json = json.dumps(mce_config)
        if tinymce_settings.USE_COMPRESSOR:
            compressor_config = {
                "plugins": mce_config.get("plugins", ""),
                "themes": mce_config.get("theme", "advanced"),
                "languages": mce_config.get("language", ""),
                "diskcache": True,
                "debug": False,
            }
            final_attrs["data-mce-gz-conf"] = json.dumps(compressor_config)
        final_attrs["data-mce-conf"] = mce_json
        html = [f"<textarea{flatatt(final_attrs)}>{escape(value)}</textarea>"]
        return mark_safe("\n".join(html))


# https://github.com/vahaah/django-ace-editor
class AceWidget(forms.Textarea):
    def __init__(
        self,
        mode=None,
        theme=None,
        wordwrap=False,
        width="500px",
        height="300px",
        minlines=None,
        maxlines=None,
        showprintmargin=True,
        showinvisibles=False,
        usesofttabs=True,
        tabsize=None,
        *args,
        **kwargs
    ):
        self.mode = mode
        self.theme = theme
        self.wordwrap = wordwrap
        self.width = width
        self.height = height
        self.minlines = minlines
        self.maxlines = maxlines
        self.showprintmargin = showprintmargin
        self.showinvisibles = showinvisibles
        self.tabsize = tabsize
        self.usesofttabs = usesofttabs
        super(AceWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, renderer=None):
        attrs = attrs or {}

        ace_attrs = {
            "name": name,
            "class": "django-ace-widget loading",
            "style": "width:%s; height:%s" % (self.width, self.height),
        }

        toolbar_modes = getattr(settings, 'DJACE_TOOLBAR_MODES', {})

        if self.mode:
            ace_attrs["data-mode"] = self.mode
            if self.mode not in toolbar_modes:
                toolbar_modes[self.mode] = self.mode
        if self.theme:
            ace_attrs["data-theme"] = self.theme
        if self.wordwrap:
            ace_attrs["data-wordwrap"] = "true"
        if self.minlines:
            ace_attrs["data-minlines"] = str(self.minlines)
        if self.maxlines:
            ace_attrs["data-maxlines"] = str(self.maxlines)
        if self.tabsize:
            ace_attrs["data-tabsize"] = str(self.tabsize)

        ace_attrs["data-showprintmargin"] = "true" if self.showprintmargin else "false"
        ace_attrs["data-showinvisibles"] = "true" if self.showinvisibles else "false"
        ace_attrs["data-usesofttabs"] = "true" if self.usesofttabs else "false"

        textarea = super(AceWidget, self).render(name, value, attrs, renderer)

        html = '<div%s><div></div></div>%s' % (flatatt(ace_attrs), textarea)
        toolbar_mode_options = ''
        for k, v in toolbar_modes.items():
            toolbar_mode_options += '<option value="%s">%s</option>' % (v, k)

        # add toolbar
        html = '<div id="id_ace_%s" class="django-ace-editor">' \
            '<div style="width: %s" class="django-ace-toolbar">' \
            '<select class="django-ace-mode_select">%s</select>' \
            '<a href="./" class="django-ace-max_min"></a>' \
            '</div>%s</div>' % (name, self.width, toolbar_mode_options, html)
        return mark_safe(html)


# https://github.com/jmrivas86/django-json-widget
class JSONEditorWidget(forms.Widget):
    # CSS and JS for jsoneditor is loaded via webpack
    template_name = 'widgets/django_json_widget.html'

    def __init__(self, attrs=None, mode='code', readonly_fields=[], editable_fields=['__all__']):
        if mode not in ['text', 'code', 'tree', 'form', 'view', 'preview']:
            mode = 'code'
        self.mode = mode
        self.readonly_fields = readonly_fields
        self.editable_fields = editable_fields

        super().__init__(attrs=attrs)

    def render(self, name, value, attrs=None, renderer=None):
        context = {
            'data': value,
            'name': name,
            'mode': self.mode,
            'readonly_fields': self.readonly_fields,
            'editable_fields': self.editable_fields
        }

        return mark_safe(render_to_string(self.template_name, context))
