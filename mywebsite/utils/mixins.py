# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import csv
import json
from collections import namedtuple

from django.conf import settings
from django.contrib import admin
from django.contrib.admin.utils import quote, unquote
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _

import reversion
from parler import cache
from reversion.models import Revision
from reversion.signals import pre_revision_commit
from reversion_compare.compare import CompareObjects
from reversion_compare.forms import SelectDiffForm

from mywebsite.utils.tools import LazyEncoder

current_revision_instance = None


class AjaxFormMixin(object):
    def form_invalid(self, form):
        response = super(AjaxFormMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super(AjaxFormMixin, self).form_valid(form)
        if self.request.is_ajax():
            print(form.cleaned_data)
            data = {
                'message': _("Successfully submitted form data.")
            }
            return JsonResponse(data, encoder=LazyEncoder)
        else:
            return response


class TranslateableReversionAdminMixin(object):
    """
    Mixin for connecting django-parler with django-reversion
    """

    def reversion_register(self, model, **kwargs):
        """Add signals to fields of translation model"""
        # If the model is translated with django-parler, register the
        # translation model to be tracked as well, by following all placeholder
        # fields, if any.
        if hasattr(model, '_parler_meta'):
            root_model = model._parler_meta.root_model

            # Also add the translations to the models to follow.
            reversion.register(model, follow=[model._parler_meta.root_rel_name])

            # Connect to singal pre_revision_commit
            pre_revision_commit.connect(self._pre_revision_commit)

            # And make sure that when we revert them, we update the translations
            # cache (this is normally done in the translation `save_base`
            # method, but it is not called when reverting changes).
            post_save.connect(self._update_cache, sender=root_model)
            pre_save.connect(self._pre_revision_save, sender=root_model)

            return super().reversion_register(
                root_model,
                for_concrete_model=False,
                ignore_duplicates=True,
                **kwargs
            )

    def _pre_revision_commit(self, sender, revision, versions, **kwargs):
        """Modify the revision object to contain correct parler information"""
        global current_revision_instance
        if current_revision_instance is not None:
            for version in versions:
                if self.model == version.object._meta.model:
                    comment = revision.comment
                    lang_str = '(LANG_CODE: %s)' % current_revision_instance.language_code.upper()
                    revision.comment = '%s %s' % (comment, lang_str)

    def _pre_revision_save(self, sender, instance, raw, **kwargs):
        global current_revision_instance
        # memorize current changed revision instance
        current_revision_instance = instance

    def _update_cache(self, sender, instance, raw, **kwargs):
        """Update the translations cache when restoring from a revision."""
        if raw:
            # Raw is set to true (only) when restoring from fixtures or,
            # django-reversion
            cache._cache_translation(instance)

    def _get_action_list(self, request, object_id, extra_context=None):
        """Override function from reversion_compare/admin.py"""
        # get all parent versions for current object_id
        revisions = [
            filtered_version.revision_id for filtered_version
            in VersionProxy.objects.get_for_object_reference(self.model, object_id).select_related("revision__user")
        ]

        # now, collect all parent and child versions for same revision_id
        filtered_versions = VersionProxy.objects.filter(revision_id__in=revisions)

        # build up action_list
        opts = self.model._meta
        action_list = [
            {
                "version": version,
                "revision": version.revision,
                "url": reverse("%s:%s_%s_revision" % (self.admin_site.name, opts.app_label, opts.model_name),
                               args=(quote(version.master_id), version.id))
            }
            for version in filtered_versions
        ]
        return action_list

    def compare_view(self, request, object_id, extra_context=None):
        """Override function from reversion_compare/admin.py"""
        if self.compare is None:
            raise Http404("Compare view not enabled.")

        form = SelectDiffForm(request.GET)
        if not form.is_valid():
            msg = "Wrong version IDs."
            if settings.DEBUG:
                msg += " (form errors: %s)" % ", ".join(form.errors)
            raise Http404(msg)

        version_id1 = form.cleaned_data["version_id1"]
        version_id2 = form.cleaned_data["version_id2"]

        if version_id1 > version_id2:
            # Compare always the newest one (#2) with the older one (#1)
            version_id1, version_id2 = version_id2, version_id1

        object_id = unquote(object_id)  # Underscores in primary key get quoted to "_5F"
        obj = get_object_or_404(self.model, pk=object_id)
        # collect all parent and child versions for same object_id
        master_id_str = '"master": %s' % (object_id)
        queryset = VersionProxy.objects.filter(Q(serialized_data__contains=master_id_str) | Q(object_id=object_id))
        version1 = get_object_or_404(queryset, pk=version_id1)
        version2 = get_object_or_404(queryset, pk=version_id2)

        next_version = queryset.filter(pk__gt=version_id2).last()
        prev_version = queryset.filter(pk__lt=version_id1).first()

        compare_data, has_unfollowed_fields = self.compare(obj, version1, version2)

        opts = self.model._meta

        context = {
            "opts": opts,
            "app_label": opts.app_label,
            "model_name": capfirst(opts.verbose_name),
            "title": _("Compare %(name)s") % {"name": version1.object_repr},
            "obj": obj,
            "compare_data": compare_data,
            "has_unfollowed_fields": has_unfollowed_fields,
            "version1": version1,
            "version2": version2,
            "changelist_url": reverse("%s:%s_%s_changelist" % (self.admin_site.name, opts.app_label, opts.model_name)),
            "change_url": reverse("%s:%s_%s_change" % (self.admin_site.name, opts.app_label, opts.model_name),
                                  args=(quote(obj.pk),)),
            "original": obj,
            "history_url": reverse("%s:%s_%s_history" % (self.admin_site.name, opts.app_label, opts.model_name),
                                   args=(quote(obj.pk),))
        }

        # don't use urlencode with dict for generate prev/next-urls
        # Otherwise we can't unitests it!
        if next_version:
            next_url = "?version_id1=%i&version_id2=%i" % (
                version2.id, next_version.id
            )
            context.update({'next_url': next_url})
        if prev_version:
            prev_url = "?version_id1=%i&version_id2=%i" % (
                prev_version.id, version1.id
            )
            context.update({'prev_url': prev_url})

        extra_context = extra_context or {}
        context.update(extra_context)
        return render(request, self.compare_template or self._get_template_list("compare.html"),
                      context)

    def compare(self, obj, version1, version2):
        """Override function from reversion_compare/mixins.py"""
        diff = []

        # Create a list of all normal fields and append many-to-many fields
        fields = [field for field in obj._meta.fields]
        concrete_model = obj._meta.concrete_model
        fields += concrete_model._meta.many_to_many

        # Get all translations fields from django-parler
        if hasattr(obj, '_parler_meta'):
            for field in obj._parler_meta.root_model._meta.fields:
                if not any(x.name == field.name for x in fields):
                    fields.append(field)

        self.reverse_fields = []
        for field in obj._meta.get_fields(include_hidden=True):
            f = getattr(field, 'field', None)
            if isinstance(f, models.ForeignKey) and f not in fields:
                self.reverse_fields.append(f.remote_field)

        fields += self.reverse_fields

        has_unfollowed_fields = False

        for field in fields:
            # logger.debug("%s %s %s", field, field.db_type, field.get_internal_type())
            try:
                field_name = field.name
            except (AttributeError, TypeError):
                # is a reverse FK field
                field_name = field.field_name

            if self.compare_fields and field_name not in self.compare_fields:
                continue
            if self.compare_exclude and field_name in self.compare_exclude:
                continue

            is_reversed = field in self.reverse_fields
            obj_compare = CompareObjects(field, field_name, obj, version1, version2, is_reversed)
            # obj_compare.debug()

            is_related = obj_compare.is_related
            follow = obj_compare.follow
            if is_related and not follow:
                has_unfollowed_fields = True

            if not obj_compare.changed():
                # Skip all fields that aren't changed
                continue

            html = self._get_compare(obj_compare)
            diff.append({
                "field": field,
                "is_related": is_related,
                "follow": follow,
                "diff": html
            })

        return diff, has_unfollowed_fields

    def revision_view(self, request, object_id, version_id, extra_context=None):
        """Override function from reversion/admin.py"""
        object_id = unquote(object_id)  # Underscores in primary key get quoted to "_5F"
        org_object_id = request.GET.get('org_object_id', object_id)
        version = get_object_or_404(VersionProxy, pk=version_id, object_id=org_object_id)
        context = {
            "title": _("Revert %(name)s") % {"name": version.object_repr},
            "revert": True
        }
        # replace object_id by master_id
        version.object_id = version.master_id
        context.update(extra_context or {})
        return super()._reversion_revisionform_view(
            request,
            version,
            super().revision_form_template or super()._reversion_get_template_list("revision_form.html"),
            context
        )


# extend the existing model of reversion.models.Version
class VersionProxy(reversion.models.Version):
    class Meta:
        app_label = "website"
        proxy = True

    @property
    def master_id(self):
        def _json_object_hook(d):
            return namedtuple('X', d.keys())(*d.values())

        def json2obj(data):
            return json.loads(data, object_hook=_json_object_hook)

        # decode JSON field and return master value
        data = json2obj(self.serialized_data)
        if hasattr(data[0].fields, 'master'):
            return "%s" % (data[0].fields.master)

        if hasattr(data[0].fields, 'article'):
            return "%s" % (data[0].fields.article)

        return self.object_id

    @property
    def is_translation_model(self):
        content_type_str = self.content_type.model
        if content_type_str.endswith('translation'):
            return True
        else:
            return False

    @property
    def is_lang_code_in_comment(self):
        comment = self.revision.comment
        lang_str = '(LANG_CODE: %s)' % self.object.language_code.upper()
        if lang_str in comment:
            return True
        else:
            return False


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = _("Export Selected")


class ExtendedActionsMixin:
    # https://gist.github.com/rafen/eff7adae38903eee76600cff40b8b659#file-admin-py-L1
    # actions that can be executed with no items selected on the admin change list.
    extended_actions = []

    def changelist_view(self, request, extra_context=None):
        # if a extended action is called and there's no checkbox selected, select one with
        # invalid id, to get an empty queryset
        if "action" in request.POST and request.POST["action"] in self.extended_actions:
            if not request.POST.getlist(admin.helpers.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                post.update({admin.helpers.ACTION_CHECKBOX_NAME: 0})
                request._set_post(post)  # pylint:disable=protected-access
        return super().changelist_view(request, extra_context)
