# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import re
import sys

from django.core.management import call_command
from django.test import TestCase
from six import StringIO


class DBBackupListingTests(TestCase):
    def setUp(self):
        super(DBBackupListingTests, self).setUp()

    def test_list_backups(self):
        out = StringIO()
        sys.stdout = out
        call_command('listbackups', stdout=out)
        assert re.match(r'^Name.*Datetime', out.getvalue())
