# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# Patches django.utils.translation

from django.conf import settings
from django.utils import translation


def get_language_info(lang_code):
    from django.conf.locale import LANG_INFO
    try:
        # append 'Swiss German'
        LANG_INFO['de-ch'] = {'bidi': False, 'code': 'de-ch', 'name': 'Swiss German', 'name_local': 'Deutsch (CH)'}

        lang_info = LANG_INFO[lang_code]
        if 'fallback' in lang_info and 'name' not in lang_info:
            info = translation.get_language_info(lang_info['fallback'][0])
        else:
            info = lang_info
    except KeyError:
        if '-' not in lang_code:
            raise KeyError("Unknown language code %s." % lang_code)
        generic_lang_code = lang_code.split('-')[0]
        try:
            info = LANG_INFO[generic_lang_code]
        except KeyError:
            raise KeyError("Unknown language code %s and %s." % (lang_code, generic_lang_code))

    if info:
        info['name_translated'] = translation.gettext_lazy(info['name'])
    return info


def get_language():
    """Return the currently selected language."""
    t = getattr(translation.trans_real._active, "value", None)
    if t is not None:
        try:
            return t.to_language()
        except AttributeError:
            pass
    # If we don't have a real translation object, assume it's the default language.
    try:
        if not hasattr(settings, 'PARLER_DEFAULT_LANGUAGE_CODE'):
            settings.PARLER_DEFAULT_LANGUAGE_CODE = settings.LANGUAGE_CODE
        return settings.PARLER_DEFAULT_LANGUAGE_CODE
    except LookupError:
        return settings.LANGUAGE_CODE


def get_language_from_request(request, check_path=False):
    """
    Analyze the request to find what language the user wants the system to
    show. Only languages listed in settings.LANGUAGES are taken into account.
    If the user requests a sublanguage where we have a main language, we send
    out the main language.

    If check_path is True, the URL path prefix will be checked for a language
    code, otherwise this is skipped for backwards compatibility.
    """
    if check_path:
        lang_code = translation.get_language_from_path(request.path_info)
        if lang_code is not None:
            return lang_code

    lang_code = request.COOKIES.get(settings.LANGUAGE_COOKIE_NAME)
    if lang_code is not None and lang_code in translation.trans_real.get_languages() and translation.check_for_language(lang_code):
        return lang_code

    try:
        return translation.get_supported_language_variant(lang_code)
    except LookupError:
        pass

    accept = request.META.get('HTTP_ACCEPT_LANGUAGE', '')
    for accept_lang, unused in translation.trans_real.parse_accept_lang_header(accept):
        if accept_lang == '*':
            break

        if not translation.trans_real.language_code_re.search(accept_lang):
            continue

        try:
            return translation.get_supported_language_variant(accept_lang)
        except LookupError:
            continue

    try:
        if not hasattr(settings, 'PARLER_DEFAULT_LANGUAGE_CODE'):
            settings.PARLER_DEFAULT_LANGUAGE_CODE = settings.LANGUAGE_CODE
        return translation.get_supported_language_variant(settings.PARLER_DEFAULT_LANGUAGE_CODE)
    except LookupError:
        return settings.LANGUAGE_CODE


translation.get_language_info = get_language_info
translation.trans_real.get_language = get_language
translation.trans_real.get_language_from_request = get_language_from_request
