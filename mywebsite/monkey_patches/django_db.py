# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# Patches django.db.backends.mysql.

from django.db.backends.mysql import base
from django.db.backends.mysql.schema import DatabaseSchemaEditor
from django.db.models.fields import CharField

old_get_new_connection = base.DatabaseWrapper.get_new_connection


def _create_index_sql(self, model, fields, suffix="", sql=None):
    """
    Return the SQL statement to create the index for one or several fields.
    `sql` can be specified if the syntax differs from the standard (GIS
    indexes, ...).
    """
    tablespace_sql = self._get_index_tablespace_sql(model, fields)
    idx_columns = []
    for field in fields:
        c = field.column
        if isinstance(field, CharField):
            if field.max_length > 255:
                idx_columns.append(self.quote_name(c) + '(255)')
            else:
                idx_columns.append(self.quote_name(c))
        else:
            idx_columns.append(self.quote_name(c))
    columns = [field.column for field in fields]
    sql_create_index = sql or self.sql_create_index
    return sql_create_index % {
        "table": self.quote_name(model._meta.db_table),
        "name": self.quote_name(self._create_index_name(model._meta.db_table, columns, suffix=suffix)),
        "using": "",
        "columns": ", ".join(column for column in idx_columns),
        "extra": tablespace_sql,
    }


def get_new_connection(self, conn_params):
    conn = old_get_new_connection(self, conn_params)
    conn.query("set names 'utf8mb4' collate 'utf8mb4_unicode_520_ci'")
    return conn


base.DatabaseWrapper.get_new_connection = get_new_connection
DatabaseSchemaEditor._create_index_sql = _create_index_sql
