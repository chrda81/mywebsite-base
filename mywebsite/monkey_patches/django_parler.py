# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# Patches Django-Parler utils.

from django.conf import settings

import parler
from parler import appsettings
from parler.models import TranslatableModel
from parler.utils import get_language_title, is_multilingual_project, normalize_language_code
from parler.utils.views import TabsList


def get_language_parameter(request, query_language_key='language', object=None, default=None):
    """
    Get the language parameter from the current request.
    """
    parler_permissions = list(filter(lambda n: 'parler.view_language_tab' in n,
                                     request.user.get_user_permissions()))

    # This is the same logic as the django-admin uses.
    # The only difference is the origin of the request parameter.
    if not is_multilingual_project():
        # By default, the objects are stored in a single static language.
        # This makes the transition to multilingual easier as well.
        # The default language can operate as fallback language too.
        return default or appsettings.PARLER_LANGUAGES.get_default_language()
    else:
        # In multilingual mode, take the provided language of the request.
        code = request.GET.get(query_language_key)

        if not code:
            # forms: show first tab by default
            code = default or appsettings.PARLER_LANGUAGES.get_first_language()

        # select first tab by permissions
        codename = 'parler.view_language_tab_{}'.format(code)
        if not request.user.is_superuser and len(parler_permissions) > 0 and codename not in parler_permissions:
            code = parler_permissions[0].replace('parler.view_language_tab_', '')

        return normalize_language_code(code)


def get_language_tabs(request, current_language, available_languages, css_class=None):
    """
    Determine the language tabs to show.
    """
    tabs = TabsList(css_class=css_class)
    get = request.GET.copy()  # QueryDict object
    tab_languages = []
    parler_permissions = list(filter(lambda n: 'parler.view_language_tab' in n,
                                     request.user.get_user_permissions()))

    site_id = getattr(settings, 'SITE_ID', None)
    for lang_dict in appsettings.PARLER_LANGUAGES.get(site_id, ()):
        code = lang_dict['code']

        # choose visible tabs by permissions
        codename = 'parler.view_language_tab_{}'.format(lang_dict['code'])
        if not request.user.is_superuser and len(parler_permissions) > 0 and codename not in parler_permissions:
            continue

        title = get_language_title(code)
        get['language'] = code
        url = '?{0}'.format(get.urlencode())

        if code == current_language:
            status = 'current'
        elif code in available_languages:
            status = 'available'
        else:
            status = 'empty'

        tabs.append((url, title, code, status))
        tab_languages.append(code)

    # Additional stale translations in the database?
    if appsettings.PARLER_SHOW_EXCLUDED_LANGUAGE_TABS:
        for code in available_languages:
            if code not in tab_languages:

                # choose visible tabs by permissions
                codename = 'parler.view_language_tab_{}'.format(lang_dict['code'])
                if not request.user.is_superuser and len(parler_permissions) > 0 and codename not in parler_permissions:
                    continue

                get['language'] = code
                url = '?{0}'.format(get.urlencode())

                if code == current_language:
                    status = 'current'
                else:
                    status = 'available'

                tabs.append((url, get_language_title(code), code, status))

    tabs.current_is_translated = current_language in available_languages
    tabs.allow_deletion = len(available_languages) > 1
    return tabs


# monkey-patch the classes from parler.utils
parler.utils.views.get_language_parameter = get_language_parameter
parler.utils.views.get_language_tabs = get_language_tabs
