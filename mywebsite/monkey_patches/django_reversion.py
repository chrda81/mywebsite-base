# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# Patches django-reversion.models.

from contextlib import contextmanager
from functools import wraps

from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.base import DeserializationError
from django.db import models, router, transaction
from django.db.models.query import QuerySet
from django.db.models.signals import m2m_changed, post_delete, post_save, pre_save
from django.utils.encoding import force_str
from django.utils.functional import cached_property
from django.utils.translation import gettext as _

import reversion
import six
from reversion.errors import RegistrationError, RevertError
from reversion.models import Version
from reversion.revisions import (
    _ContextWrapper, _current_frame, _dummy_context, _get_content_type, _get_options, _local, _m2m_changed_receiver,
    _pop_frame, _push_frame, _save_revision, add_to_revision, is_active, is_manage_manually, is_registered, set_comment
)
from reversion.views import _request_creates_revision, _set_user_from_request

from mywebsite_home.models import RevisionAudit


# Error since Django 2.2: Cannot use cached_property instance without calling __set_name__() on it. So we maybe don't
# need our workaround anymore and use the package method again
# @cached_property
# def _object_version(self):
#     version_options = _get_options(self._model)
#     data = self.serialized_data
#     data = force_str(data.encode("utf8"))
#     try:
#         result = list(serializers.deserialize(self.format, data, ignorenonexistent=True,
#                                               use_natural_foreign_keys=version_options.use_natural_foreign_keys))

#         if len(result) > 0:

#             return result[0]
#         else:
#             raise RevertError(_("Could not load %(object_repr)s version - no version data.") % {
#                 "object_repr": self.object_repr,
#             })
#     except DeserializationError:
#         raise RevertError(_("Could not load %(object_repr)s version - incompatible version data.") % {
#             "object_repr": self.object_repr,
#         })
#     except serializers.SerializerDoesNotExist:
#         raise RevertError(_("Could not load %(object_repr)s version - unknown serializer %(format)s.") % {
#             "object_repr": self.object_repr,
#             "format": self.format,
#         })


def _post_delete_receiver(sender, instance, using, **kwargs):
    if is_registered(sender) and is_active() and not is_manage_manually():
        # Exit early if the instance is not fully-formed.
        if instance.pk is None:
            return
        version_options = _get_options(instance.__class__)
        content_type = _get_content_type(instance.__class__, using)
        object_id = force_str(instance.pk)
        # Get the version data.
        version = Version.objects.filter(object_id=object_id, content_type=content_type)
        if version:
            version = version.latest('revision_id')
            object_repr = ", ".join(force_str(_version)
                                    for _version in version.revision.version_set.all())
        else:
            version = None
            object_repr = force_str(instance)
        # Add to table revision_audit
        revision_audit = RevisionAudit(
            object_id=object_id,
            object_repr=object_repr,
            content_type=content_type,
            user=_current_frame().user,
            action='Deleted',
            comment=_current_frame().comment,
            version=version
        )
        revision_audit.save()


def _pre_save_receiver(sender, instance, using, **kwargs):
    if is_registered(sender) and is_active() and not is_manage_manually():
        current_frame = _current_frame()
        previous_version = Version.objects.using(using).get_for_object(
            instance, model_db=router.db_for_write(instance.__class__, instance=instance)).first()
        update_fields = []
        if previous_version:
            # compare fields of previous version and current instance
            for field in previous_version._local_field_dict:
                if previous_version._local_field_dict[field] != getattr(instance, field):
                    update_fields.append(field)

        # add comment if necessary
        if not current_frame.comment and update_fields:
            set_comment('[{"changed": {"fields": %s}}]' % update_fields)


def _post_save_receiver(sender, instance, using, **kwargs):
    if is_registered(sender) and is_active() and not is_manage_manually():
        add_to_revision(instance, model_db=using)
        # add comment if necessary
        current_frame = _current_frame()
        if not current_frame.comment and kwargs.get('created', False):
            set_comment('[{"added": {}}]')


def _follow_relations(obj):
    version_options = _get_options(obj.__class__)
    for follow_name in version_options.follow:
        if hasattr(obj, follow_name):
            try:
                follow_obj = getattr(obj, follow_name)
            except ObjectDoesNotExist:
                continue
            if isinstance(follow_obj, models.Model):
                yield follow_obj
            elif isinstance(follow_obj, (models.Manager, QuerySet)):
                for follow_obj_instance in follow_obj.all():
                    yield follow_obj_instance
            elif follow_obj is not None:
                raise RegistrationError("{name}.{follow_name} should be a Model or QuerySet".format(
                    name=obj.__class__.__name__,
                    follow_name=follow_name,
                ))


def _get_senders_and_signals(model):
    # register post_save signals with method _post_save_receiver
    yield model, pre_save, _pre_save_receiver
    # register post_save signals with method _post_save_receiver
    yield model, post_save, _post_save_receiver
    # register post_delete signals with method _post_delete_receiver
    yield model, post_delete, _post_delete_receiver
    opts = model._meta.concrete_model._meta
    for field in opts.local_many_to_many:
        m2m_model = field.remote_field.through
        if isinstance(m2m_model, str):
            if "." not in m2m_model:
                m2m_model = "{app_label}.{m2m_model}".format(
                    app_label=opts.app_label,
                    m2m_model=m2m_model
                )
        # register m2m_changed signals with method _m2m_changed_receiver
        yield m2m_model, m2m_changed, _m2m_changed_receiver


@contextmanager
def _create_revision_context(manage_manually, using, atomic, comment):
    _push_frame(manage_manually, using)
    try:
        context = transaction.atomic(using=using) if atomic else _dummy_context()
        with context:
            yield
            # Only save for a db if that's the last stack frame for that db.
            if not any(using in frame.db_versions for frame in _local.stack[:-1]):
                current_frame = _current_frame()
                _save_revision(
                    versions=current_frame.db_versions[using].values(),
                    user=current_frame.user,
                    comment='{}{}'.format(comment, current_frame.comment),
                    meta=current_frame.meta,
                    date_created=current_frame.date_created,
                    using=using,
                )

                if '{"added":' in current_frame.comment:
                    action_str = 'Added'
                elif '{"changed":' in current_frame.comment:
                    action_str = 'Changed'
                elif 'Initial version' in current_frame.comment:
                    action_str = 'Initialized'
                elif 'Reverted to previous version' in current_frame.comment or \
                        'Zu vorheriger Version zurückgesetzt' in current_frame.comment:
                    action_str = 'Recovered'
                else:
                    action_str = ''

                if current_frame.db_versions[using]:
                    # Get the version data.
                    for version in current_frame.db_versions[using].values():
                        # Add to table revision_audit
                        revision_audit = RevisionAudit(
                            object_id=version.object_id,
                            object_repr=", ".join(force_str(_version)
                                                  for _version in version.revision.version_set.all()),
                            content_type=version.content_type,
                            user=current_frame.user,
                            action=action_str,
                            comment=current_frame.comment,
                            version=version
                        )
                        revision_audit.save()

    finally:
        _pop_frame()


def create_revision_base(manage_manually=False, using=None, atomic=True, comment=''):
    from reversion.models import Revision
    using = using or router.db_for_write(Revision)
    return _ContextWrapper(_create_revision_context, (manage_manually, using, atomic, comment))


def create_revision(manage_manually=False, using=None, atomic=True, request_creates_revision=None):
    """
    View decorator that wraps the request in a revision.

    The revision will have it's user set from the request automatically.
    """
    request_creates_revision = request_creates_revision or _request_creates_revision

    def decorator(func):
        @wraps(func)
        def do_revision_view(request, *args, **kwargs):
            if request_creates_revision(request):
                if request.resolver_match and request.resolver_match.view_name:
                    comment = _('Created by view: %s. ') % request.resolver_match.view_name
                elif request.path.startswith('/admin'):
                    comment = _('Created by ModelAdmin. ')
                else:
                    comment = _('Created by Middleware. ')
                with create_revision_base(manage_manually=manage_manually,
                                          using=using,
                                          atomic=atomic,
                                          comment=comment):
                    response = func(request, *args, **kwargs)
                    # Otherwise, we're good.
                    _set_user_from_request(request)
                    return response
            return func(request, *args, **kwargs)
        return do_revision_view
    return decorator


# monkey-patch the classes from reversion.revisions
# reversion.models.Version._object_version = _object_version
reversion.revisions._get_senders_and_signals = _get_senders_and_signals
reversion.revisions._create_revision_context = _create_revision_context
reversion.revisions._post_save_receiver = _post_save_receiver
reversion.revisions._follow_relations = _follow_relations
reversion.revisions.create_revision = create_revision_base

# monkey-patch the classes from reversion.views
reversion.views.create_revision = create_revision
