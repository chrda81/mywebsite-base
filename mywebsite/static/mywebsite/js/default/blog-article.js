/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';

import 'core_assets/js/default/ajax-setup';

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Article AJAX hit counter: Fire, if class="article-blog ajax" exists!
    if ($('.article-blog.ajax').length) {
        $.ajax({
            method: 'POST',
            url: $hitcount_url,
            data: {
                hitcountPK: $hitcount_pk,
            },
            success: function(data) {
                $('<i />')
                    .text(data.hit_counted)
                    .attr('id', 'hit-counted-value')
                    .appendTo('#hit-counted');
                $('#hit-response').text(data.hit_message);
            },
            error: function(xhr, status, error) {
                let json = JSON.parse(xhr.responseText);
                let error_message = json.message;
                console.log(error_message);
            },
        });
    }
});
