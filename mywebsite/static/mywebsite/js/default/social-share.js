/**
 * Copyright (C) 2018 - 2021 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';

import 'node_modules/jquery-floating-social-share/dist/jquery.floating-social-share.min.js';

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    const buttons = new Function(
        'return ' + $('meta[property="floatingSocialButtons"]').attr('content')
    )();
    $('body').floatingSocialShare({
        place: 'top-right', // alternatively content-left, content-right, top-right
        counter: false, // set to false for hiding the counters of buttons
        facebook_token: null, // To show Facebook share count, obtain a token, see: https://stackoverflow.com/questions/17197970/facebook-permanent-page-access-token/43570120#43570120
        buttons: buttons !== undefined ? buttons : [],
        title: document.title,
        url: window.location.href,
        text: {
            // the title of tags
            mail: 'share via Mail',
            default: 'share with ',
            twitter: 'tweet',
        },
        text_title_case: true, // if set true, then will convert share texts to title case like Share With G+
        description: $('meta[name="description"]').attr('content'),
        media: $('meta[property="og:image"]').attr('content'),
        target: true, // open share pages, such as Twitter and Facebook share pages, in a new tab
        popup: false, // open links in popup
        popup_width: 400, // the sharer popup width, default is 400px
        popup_height: 300, // the sharer popup height, default is 300px
    });
});
