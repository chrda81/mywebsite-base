/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';

import 'core_assets/js/default/ajax-setup';

function InitContactForm() {
    // Add refresh button after field (this can be done in the template as well)
    if ($('.captcha-refresh').length == 0) {
        $('img.captcha').after(
            $(
                '<a class="captcha-refresh" href="#void"><i class="fa fa-refresh"></i></a>'
            )
        );
    }

    // Bugfix for required field validation. In django-simple-captcha==0.5.6 it is not set!
    $('#id_captcha_1').prop('required', true);

    // Add crispy-forms css class to django-simple-captcha form field
    if ($('#error_1_id_captcha').length) {
        $('#id_captcha_1').prop(
            'class',
            'textinput textInput form-control is-invalid'
        );
    } else {
        $('#id_captcha_1').prop('class', 'textinput textInput form-control');
    }

    // Click-handler for the refresh-link
    // Prevent multiple event binding: https://www.gajotres.net/prevent-jquery-multiple-event-triggering/
    $('.captcha-refresh')
        .off('click')
        .on('click', function() {
            let $form = $(this).parents('form');
            let url =
                location.protocol +
                '//' +
                window.location.hostname +
                ':' +
                location.port +
                '/captcha/refresh/';

            console.log('Captcha refresh! Redirecting to ' + url);

            // Make the AJAX-call
            $.getJSON(url, {}, function(json) {
                // This should update your captcha image src and captcha hidden input
                $form.find('input[name="captcha_0"]').val(json.key);
                $form.find('img.captcha').attr('src', json.image_url);
            });

            return false;
        });

    // Prevent multiple event binding: https://www.gajotres.net/prevent-jquery-multiple-event-triggering/
    $('#ajax-form-contact')
        .off('submit')
        .on('submit', function(event) {
            event.preventDefault();

            let $formData = $(this).serialize();
            // console.log('FormData: ' + $formData);
            let $thisURL = $('#ajax-form-contact').attr('action');
            $.ajax({
                method: 'POST',
                url: $thisURL,
                data: $formData,
                success: function(data) {
                    if (!data['success']) {
                        $('.messages').replaceWith(
                            '<div class="messages"><ul> </ul></div>'
                        );
                        $('.messages ul').append(
                            '<li class="form_errors">' +
                                data['message'] +
                                '</li>'
                        );
                        // console.log(data)
                        $('#ajax-form-contact').replaceWith(data['html_form']);
                    } else {
                        $('.messages').replaceWith(
                            '<div class="messages"><ul> </ul></div>'
                        );
                        $('.messages ul').append(
                            '<li class="form_messages">' +
                                data['message'] +
                                '</li>'
                        );
                        // console.log(data)
                        // reset form data and remove field errors
                        $('#ajax-form-contact')
                            .find('input')
                            .filter('.form-control')
                            .val('');
                        $('#ajax-form-contact')
                            .find('input')
                            .removeClass('is-invalid');
                        $('#ajax-form-contact')
                            .find('textarea')
                            .filter('.form-control')
                            .val('');
                        $('#ajax-form-contact')
                            .find('textarea')
                            .removeClass('is-invalid');
                        $('#ajax-form-contact')
                            .find('.invalid-feedback')
                            .remove();
                    }
                },
                error: function(xhr, status, error) {
                    let json = JSON.parse(xhr.responseText);
                    let error_message = json.message;
                    console.log(error_message);
                },
            });
        });
}

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    InitContactForm();
});

// jQuery DOM manipulation after each AJAX call has completed
$(document).ajaxComplete(function() {
    InitContactForm();
});
