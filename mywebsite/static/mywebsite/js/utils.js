/*--------------------------------------------------------------
 *  Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 *  This Program may be used by anyone in accordance with the terms of the
 *  German Free Software License
 *
 *  The License may be obtained under http://www.d-fsl.org.
 *-------------------------------------------------------------*/

'use strict';

function readKeyValuesFromCookie(key) {
    try {
        var retval = document.cookie
            .split(';')
            .map(pair => pair.split('='))
            .filter(pair => pair.length == 2)
            .map(pair => [pair[0].trim(), pair[1].trim()])
            .filter(pair => pair[0] == key)
            .map(pair => pair[1])
            .toString();

        // remove enclosing "" chars
        if (retval.startsWith('"') && retval.endsWith('"')) {
            retval = retval.substr(1, retval.length - 2);
        }

        // remove enclosing '' chars
        if (retval.startsWith("'") && retval.endsWith("'")) {
            retval = retval.substr(1, retval.length - 2);
        }

        return retval;
    } catch (ex) {
        return '';
    }
}
