# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from jet.dashboard import modules
from jet.dashboard.dashboard import (AppIndexDashboard, Dashboard,
                                     get_admin_site_name)


class CustomIndexDashboard(Dashboard):
    columns = 3

    def init_with_context(self, context):
        self.available_children.append(modules.LinkList)
        self.available_children.append(modules.Feed)

        # self.available_children.append(google_analytics.GoogleAnalyticsVisitorsTotals)
        # self.available_children.append(google_analytics.GoogleAnalyticsVisitorsChart)
        # self.available_children.append(google_analytics.GoogleAnalyticsPeriodVisitors)

        site_name = get_admin_site_name(context)

        # append a feed module
        self.available_children.append(modules.Feed(
            _('Latest Django News'),
            feed_url='http://www.djangoproject.com/rss/weblog/',
            limit=5,
        ))

        # append an app list module for "Applications"
        self.available_children.append(modules.AppList(
            _('Applications'),
            exclude=('auth.*', 'django_otp.*'),
        ))

        # Column 1 / Row 1
        # append an app list module for "Editor Actions"
        self.children.append(modules.AppList(
            _('Editor Actions'),
            models=('mywebsite_home.News', 'mywebsite_blog.*', 'filebrowser.*'),
            column=0,
            order=0
        ))

        # Column 1 / Row 2
        # append an app list module for "Recent Actions"
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            8,
            column=0,
            order=1
        ))

        # Column 2 / Row 1
        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            _('Administration'),
            models=('mywebsite_home.*', 'reversion.*', 'hitcount.*', 'mywebsite_antispam.*'),
            exclude=('mywebsite_home.News'),
            column=1,
            order=0
        ))

        # Column 3 / Row 1
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            children=[
                [_('Return to site'), '/'],
                [_('Change password'), reverse('%s:password_change' % site_name)],
                [_('Log out'), reverse('%s:logout' % site_name)],
            ],
            column=2,
            order=0
        ))

        # Column 3 / Row 2
        # append an app list module for "Authentication"
        self.children.append(modules.AppList(
            _('Authentication'),
            models=('auth.*', 'django_otp.*'),
            column=2,
            order=1
        ))

        # Column 3 / Row 3
        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Support'),
            children=[
                {
                    'title': _('Admin documentation'),
                    'url': reverse('django-admindocs-docroot'),
                    'external': False,
                },
                {
                    'title': _('Django documentation'),
                    'url': 'http://docs.djangoproject.com/',
                    'external': True,
                },
            ],
            column=2,
            order=2
        ))


class CustomAppIndexDashboard(AppIndexDashboard):
    def init_with_context(self, context):
        self.available_children.append(modules.LinkList)

        self.children.append(modules.ModelList(
            title=_('Application models'),
            models=self.models(),
            column=0,
            order=0
        ))
        self.children.append(modules.RecentActions(
            include_list=self.get_app_content_types(),
            column=1,
            order=0
        ))
