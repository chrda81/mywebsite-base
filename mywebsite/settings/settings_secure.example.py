# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------

Django settings for website project.

This file contains settings, which should not be shared with others!

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os
import sys
from string import Template

from mywebsite.settings.base import BASE_DIR, DJANGO_PROJECT_NAME, STATIC_ROOT, STATICFILES_DIRS, WEBPACK_LOADER

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '<your secret key goes in here>'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
FORCE_SCRIPT_NAME = ''
ALLOWED_HOSTS = ['localhost', 'host.docker.internal']


# Settings for django-maintenance-mode
# https://github.com/fabiocaccamo/django-maintenance-mode

# if True the maintenance-mode will be activated
MAINTENANCE_MODE = None
# by default, a file named "maintenance_mode_state.txt" will be created in the maintenance_mode directory
# you can customize the state file path in case the default one is not writable
MAINTENANCE_MODE_STATE_FILE_PATH = os.path.join(STATIC_ROOT, 'maintenance_mode_state.txt')
# if True admin site will not be affected by the maintenance-mode page
MAINTENANCE_MODE_IGNORE_ADMIN_SITE = True
# if True anonymous users will not see the maintenance-mode page
MAINTENANCE_MODE_IGNORE_ANONYMOUS_USER = False
# if True authenticated users will not see the maintenance-mode page
MAINTENANCE_MODE_IGNORE_AUTHENTICATED_USER = False
# if True the staff will not see the maintenance-mode page
MAINTENANCE_MODE_IGNORE_STAFF = False
# if True the superuser will not see the maintenance-mode page
MAINTENANCE_MODE_IGNORE_SUPERUSER = False
# list of ip-addresses that will not be affected by the maintenance-mode
# ip-addresses will be used to compile regular expressions objects
MAINTENANCE_MODE_IGNORE_IP_ADDRESSES = ()
# the path of the function that will return the client IP address given the request object -> 'myapp.mymodule.myfunction'
# the default function ('maintenance_mode.utils.get_client_ip_address') returns request.META['REMOTE_ADDR']
# in some cases the default function returns None, to avoid this scenario just use 'django-ipware'
MAINTENANCE_MODE_GET_CLIENT_IP_ADDRESS = None
# list of urls that will not be affected by the maintenance-mode
# urls will be used to compile regular expressions objects
MAINTENANCE_MODE_IGNORE_URLS = ()
# if True the maintenance mode will not return 503 response while running tests
# useful for running tests while maintenance mode is on, before opening the site to public use
MAINTENANCE_MODE_IGNORE_TESTS = False
# the absolute url where users will be redirected to during maintenance-mode
MAINTENANCE_MODE_REDIRECT_URL = None
# the template that will be shown by the maintenance-mode page
MAINTENANCE_MODE_TEMPLATE = '503.html'
# the path of the function that will return the template context -> 'myapp.mymodule.myfunction'
MAINTENANCE_MODE_GET_TEMPLATE_CONTEXT = None
# the HTTP status code to send
MAINTENANCE_MODE_STATUS_CODE = 503
# the value in seconds of the Retry-After header during maintenance-mode
MAINTENANCE_MODE_RETRY_AFTER = 3600  # 1 hour


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': '<database>',
    #     'USER': '<username>',
    #     'PASSWORD': '<password>',
    #     'HOST': '<host>',
    #     'PORT': '3306',
    #     'OPTIONS': {
    #         'autocommit': True,
    #         'charset': 'utf8mb4',
    #         'init_command': 'set collation_connection=utf8mb4_unicode_520_ci',
    #         'sql_mode': 'STRICT_TRANS_TABLES',
    #     },
    # }
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
]


# Main FileBrowser Directory
# https://django-filebrowser.readthedocs.io/en/latest/settings.html#settings

FILEBROWSER_DIRECTORY = 'uploads/'


# Email backend
# https://hellowebbooks.com/news/tutorial-setting-up-a-contact-form-with-django/

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = 'postmaster@example.com'
DEFAULT_TO_EMAIL = 'testing@example.com'
DEFAULT_SUBJECT = 'New message from submission'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_PORT = 1025


# Social Media App Settings
# If array is empty, floating buttons are disabled
FLOATING_SOCIAL_BUTTONS = ['mail', 'twitter']


# Caching translations and other objects with Memcache
# https://realpython.com/python-memcache-efficient-caching/#installing-memcached
# https://django-parler.readthedocs.io/en/latest/performance.html

PARLER_ENABLE_CACHING = True
SITES_ENALBE_CACHING = True
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'KEY_PREFIX': 'mysite.production',  # Change this
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 24*3600  # cache data for 24 hours, then fetch them once again
    },
}


# Define custom filter attributes for extending the logging formatter
def add_custom_filter(record):
    record.project = DJANGO_PROJECT_NAME
    return True


# DBBackup Settings
# https://github.com/django-dbbackup/django-dbbackup
# use full path here, because cron cannot handle relative paths

LOGFILE = os.path.join(BASE_DIR, 'scheduled_django_jobs.log')
DBBACKUP_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'add_custom_filter': {
            '()': 'django.utils.log.CallbackFilter',
            'callback': add_custom_filter,
        }
    },
    'handlers': {
        'file': {
            'formatter': 'base',
            'class': 'logging.FileHandler',
            'filters': ['add_custom_filter'],
            'filename': LOGFILE,
        },
    },
    'formatters': {
        'base': {'format': '%(asctime)s.%(msecs)03d  %(project)s  %(message)s', 'datefmt': '%Y-%m-%d %H:%M:%S'},
    },
    'loggers': {
        'dbbackup': {
            'handlers': [
                'file'
            ],
            'level': 'INFO',
            'propagate': True,
        },
    }
}
# - For the dropbox settings usage, please use "pip install dropbox" before
# DBBACKUP_STORAGE = 'storages.backends.dropbox.DropBoxStorage'
# DBBACKUP_STORAGE_OPTIONS = {
#     'oauth2_access_token': '<your-secret-dropbox-auth-token>',
#     'root_path': '/Backups'
# }
DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': '/var/backups'}


# Settings for django-crontab
# extend PATH with location for 'mysqldump'

CRONTAB_COMMAND_PREFIX = 'PATH=$PATH:/usr/local/bin'
CRONTAB_COMMAND_SUFFIX = '2>&1'
CRONTAB_DJANGO_MANAGE_PATH = os.path.join(os.path.dirname(sys.argv[0]), 'manage.py')
CRONTAB_DJANGO_PROJECT_NAME = DJANGO_PROJECT_NAME
CRONJOBS = [
    ('*/5 * * * *', 'members.cron.set_product_expiration_job', '>> {}'.format(LOGFILE)),
    ('0 */4 * * *', 'django.core.management.call_command', ['dbbackup', '-c', '-z']),
    ('5 */4 * * *', 'django.core.management.call_command', ['mediabackup', '-c', '-z']),
]


# Source code repository server settings for 'members' app
# When members.product is expired, deactivate product.download_user and vice versa

SOURCE_CODE_API_USE = False
SOURCE_CODE_API_URL = "https://your-fance-source-control-server/api"
SOURCE_CODE_API_TEMPLATE = Template("""{
  "id": $id,
  "auth_token": "<your api auth token goes in here>",
  "method": "update_user",
  "args": {
    "userid": "$userid",
    "active": $active
  }
}""")  # API JSON template


if not DEBUG:
    WEBPACK_LOADER['DEFAULT'].update({
        'CACHE': True,
        # 'BUNDLE_DIR_NAME': 'dist/',
        # 'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats-prod.json')
    })


# Pagination for views

PAGINATION_NEWS = 5
PAGINATION_ARTICLES = 5
PAGINATION_CATEGORIES = 10
PAGINATION_SEARCH_RESULTS = 5


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

# Use './manage.py collectstatic' to collect all static files from STATICFILES_DIRS
# As we don't use static subfolders in our apps, to prevent some conflicts, we have to
# comment the STATIC_ROOT here. The STATIC_ROOT and STATICFILES_DIRS cannot be the same,
# but in development the STATICFILES_DIRS are only served.

# if DEBUG and len(sys.argv) > 1 and not sys.argv[1] in ['collectstatic', 'findstatic']:
#     STATICFILES_DIRS += [STATIC_ROOT]
#     STATIC_ROOT = ''
