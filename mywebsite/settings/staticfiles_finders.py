"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os

from django.contrib.staticfiles import utils
from django.contrib.staticfiles.finders import AppDirectoriesFinder as DjangoAppDirectoriesFinder


class AppDirectoriesFinder(DjangoAppDirectoriesFinder):
    ignore_patterns = []
    ignore_fullpath_patterns = [
        'jet/static/jet/js/build/bundle.min.js',
        'jet/static/jet/js/src/features/selects.js',
        'jet/static/jet/js/src/utils/translate.js',
        'jet/static/jet/css/themes/**/*',
        'jet/static/jet/css/_forms.scss',
        'jet/static/admin/js/SelectFilter2.js',
        'jet/static/admin/css/forms.css',
        'jet/static/admin/css/widgets.css'
    ]

    def get_ignored_patterns(self):
        return list(set(self.ignore_patterns))

    def list(self, ignore_patterns):
        """
        List all files in all app storages.
        """
        if ignore_patterns:
            ignore_patterns = ignore_patterns + self.get_ignored_patterns()

        for app, storage in self.storages.items():
            if storage.exists(''):  # check if storage location exists
                for path in utils.get_files(storage, ignore_patterns):
                    if utils.matches_patterns(os.path.join(app, self.source_dir, path), self.ignore_fullpath_patterns):
                        continue
                    yield path, storage
