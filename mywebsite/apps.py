# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig


class MyWebsiteConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite``."""
    name = 'mywebsite'

    def ready(self):
        """ Apply monkey patches """
        import mywebsite.monkey_patches.django_translations  # monkey-patch the clases from django.utils.translation
        import mywebsite.monkey_patches.django_db  # monkey-patch the classes from django.db
        import mywebsite.monkey_patches.django_hitcount  # monkey-patch the classes from django_hitcount
        import mywebsite.monkey_patches.django_jet  # monkey-patch the classes from jet
        import mywebsite.monkey_patches.django_parler  # monkey-patch the classes from parler
        import mywebsite.monkey_patches.django_reversion  # monkey-patch the classes from django_reversion
