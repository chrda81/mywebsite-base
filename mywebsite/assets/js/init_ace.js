/*--------------------------------------------------------------
 *  Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 *  This Program may be used by anyone in accordance with the terms of the
 *  German Free Software License
 *
 *  The License may be obtained under http://www.d-fsl.org.
 *
 *  Credit for contents of this file goes to the project https://github.com/vahaah/django-ace-editor
 *-------------------------------------------------------------*/

import 'node_modules/ace-builds';
// import 'node_modules/select2';

// ACE webpack resolve helper
// import 'node_modules/ace-builds/webpack-resolver';
ace.config.setModuleUrl(
    'ace/ext/beautify',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-beautify.js')
);
ace.config.setModuleUrl(
    'ace/ext/elastic_tabstops_lite',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-elastic_tabstops_lite.js')
);
ace.config.setModuleUrl(
    'ace/ext/emmet',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-emmet.js')
);
ace.config.setModuleUrl(
    'ace/ext/error_marker',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-error_marker.js')
);
ace.config.setModuleUrl(
    'ace/ext/keyboard_menu',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-keybinding_menu.js')
);
ace.config.setModuleUrl(
    'ace/ext/language_tools',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-language_tools.js')
);
ace.config.setModuleUrl(
    'ace/ext/linking',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-linking.js')
);
ace.config.setModuleUrl(
    'ace/ext/modelist',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-modelist.js')
);
ace.config.setModuleUrl(
    'ace/ext/options',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-options.js')
);
ace.config.setModuleUrl(
    'ace/ext/rtl',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-rtl.js')
);
ace.config.setModuleUrl(
    'ace/ext/searchbox',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-searchbox.js')
);
ace.config.setModuleUrl(
    'ace/ext/settings_menu',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-settings_menu.js')
);
ace.config.setModuleUrl(
    'ace/ext/spellcheck',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-spellcheck.js')
);
ace.config.setModuleUrl(
    'ace/ext/split',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-split.js')
);
ace.config.setModuleUrl(
    'ace/ext/static_highlight',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-static_highlight.js')
);
ace.config.setModuleUrl(
    'ace/ext/statusbar',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-statusbar.js')
);
ace.config.setModuleUrl(
    'ace/ext/textarea',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-textarea.js')
);
ace.config.setModuleUrl(
    'ace/ext/themelist',
    require('file-loader!node_modules/ace-builds/src-noconflict/ext-themelist.js')
);
ace.config.setModuleUrl(
    'ace/mode/css',
    require('file-loader!node_modules/ace-builds/src-noconflict/mode-css.js')
);
ace.config.setModuleUrl(
    'ace/mode/javascript',
    require('file-loader!node_modules/ace-builds/src-noconflict/mode-javascript.js')
);
ace.config.setModuleUrl(
    'ace/mode/sass',
    require('file-loader!node_modules/ace-builds/src-noconflict/mode-sass.js')
);
ace.config.setModuleUrl(
    'ace/theme/tomorrow_night_eighties',
    require('file-loader!node_modules/ace-builds/src-noconflict/theme-tomorrow_night_eighties.js')
);
ace.config.setModuleUrl(
    'ace/snippets/css',
    require('file-loader!node_modules/ace-builds/src-noconflict/snippets/css.js')
);
ace.config.setModuleUrl(
    'ace/snippets/javascript',
    require('file-loader!node_modules/ace-builds/src-noconflict/snippets/javascript.js')
);
ace.config.setModuleUrl(
    'ace/snippets/sass',
    require('file-loader!node_modules/ace-builds/src-noconflict/snippets/sass.js')
);

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    function getDocHeight() {
        var D = document;
        return Math.max(
            Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
            Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
            Math.max(D.body.clientHeight, D.documentElement.clientHeight)
        );
    }

    function getDocWidth() {
        var D = document;
        return Math.max(
            Math.max(D.body.scrollWidth, D.documentElement.scrollWidth),
            Math.max(D.body.offsetWidth, D.documentElement.offsetWidth),
            Math.max(D.body.clientWidth, D.documentElement.clientWidth)
        );
    }

    function next(elem) {
        // Credit to John Resig for this function
        // taken from Pro JavaScript techniques
        do {
            elem = elem.nextSibling;
        } while (elem && elem.nodeType != 1);
        return elem;
    }

    function prev(elem) {
        // Credit to John Resig for this function
        // taken from Pro JavaScript techniques
        do {
            elem = elem.previousSibling;
        } while (elem && elem.nodeType != 1);
        return elem;
    }

    function redraw(element) {
        element = $(element);
        var n = document.createTextNode(' ');
        element.appendChild(n);
        (function() {
            n.parentNode.removeChild(n);
        }.defer());
        return element;
    }

    function minimizeMaximize(widget, main_block, editor) {
        if (window.fullscreen == true) {
            main_block.className = 'django-ace-editor';

            widget.style.width = window.ace_widget.width + 'px';
            widget.style.height = window.ace_widget.height + 'px';
            widget.style.zIndex = 1;
            window.fullscreen = false;
        } else {
            window.ace_widget = {
                width: widget.offsetWidth,
                height: widget.offsetHeight,
            };

            main_block.className = 'django-ace-editor-fullscreen';

            widget.style.height = getDocHeight() + 'px';
            widget.style.width = getDocWidth() + 'px';
            widget.style.zIndex = 999;

            window.scrollTo(0, 0);
            window.fullscreen = true;
            editor.resize();
        }
    }

    function apply_widget(widget) {
        var div = widget.firstChild,
            textarea = next(widget),
            editor = ace.edit(div),
            mode = widget.getAttribute('data-mode'),
            theme = widget.getAttribute('data-theme'),
            wordwrap = widget.getAttribute('data-wordwrap'),
            minlines = widget.getAttribute('data-minlines'),
            maxlines = widget.getAttribute('data-maxlines'),
            showprintmargin = widget.getAttribute('data-showprintmargin'),
            showinvisibles = widget.getAttribute('data-showinvisibles'),
            tabsize = widget.getAttribute('data-tabsize'),
            usesofttabs = widget.getAttribute('data-usesofttabs'),
            toolbar = prev(widget),
            main_block = toolbar.parentNode;

        // Toolbar maximize/minimize button
        var min_max = toolbar.getElementsByClassName('django-ace-max_min');
        min_max[0].onclick = function() {
            minimizeMaximize(widget, main_block, editor);
            return false;
        };

        editor.getSession().setValue(textarea.value);
        editor.$blockScrolling = Infinity;

        // the editor is initially absolute positioned
        textarea.style.display = 'none';

        // options
        if (mode) {
            editor.getSession().setMode('ace/mode/' + mode);
        }
        if (theme) {
            editor.setTheme('ace/theme/' + theme);
        }
        if (wordwrap == 'true') {
            editor.getSession().setUseWrapMode(true);
        }
        if (!!minlines) {
            editor.setOption('minLines', minlines);
        }
        if (!!maxlines) {
            editor.setOption(
                'maxLines',
                maxlines == '-1' ? Infinity : maxlines
            );
        }
        if (showprintmargin == 'false') {
            editor.setShowPrintMargin(false);
        }
        if (showinvisibles == 'true') {
            editor.setShowInvisibles(true);
        }
        if (!!tabsize) {
            editor.setOption('tabSize', tabsize);
        }
        if (usesofttabs == 'false') {
            editor.getSession().setUseSoftTabs(false);
        }

        editor.getSession().on('change', function() {
            textarea.value = editor.getSession().getValue();
        });

        editor.commands.addCommand({
            name: 'Full screen',
            bindKey: { win: 'Ctrl-F11', mac: 'Command-F11' },
            exec: function(editor) {
                minimizeMaximize(widget, main_block, editor);
            },
            readOnly: true, // false if this command should not apply in readOnly mode
        });
    }

    // attach ace to widget
    var widgets = document.getElementsByClassName('django-ace-widget');
    for (var i = 0; i < widgets.length; i++) {
        var widget = widgets[i];
        widget.className = 'django-ace-widget'; // remove `loading` class

        apply_widget(widget);
    }

    // $('.django-ace-toolbar').find('.select2').on('change', function(e) {
    //     var data = e.params.data;
    //     console.log(data);
    // });
});
