/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';
// import 'node_modules/jquery.nicescroll';
import 'node_modules/animatedmodal';

import ScrollReveal from 'core_assets/js/default/scrollReveal';

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    'use strict';
    $(window).on('load', function() {
        var $preloader = $('.ct-preloader');
        var $content = $('.ct-preloader-content');

        var $timeout = setTimeout(function() {
            $($preloader)
                .addClass('animated')
                .addClass('fadeOut');
            $($content)
                .addClass('animated')
                .addClass('fadeOut');
        }, 0);
        var $timeout2 = setTimeout(function() {
            $($preloader)
                .css('display', 'none')
                .css('z-index', '-9999');
        }, 1200);
    });

    if ($('.menuButton').length > 0) {
        $('#openMenu').animatedModal({
            modalTarget: 'menuModal',
            animatedIn: 'bounceInLeft',
            animatedOut: 'bounceOutUp',
            color: '#3498db',
        });
    }
    if ($('.mainMenu').length > 0) {
        var subStatus = true;
        $('.hasChildItem > a').click(function(e) {
            e.preventDefault();
            if (
                $(this)
                    .parent('li')
                    .hasClass('active')
            ) {
                $(this)
                    .parent('li')
                    .removeClass('active');
                $(this)
                    .next('ul.subMenu')
                    .slideUp('slow');
            } else {
                $('.mainMenu ul li.hasChildItem.active ul.subMenu').slideUp(
                    'slow'
                );
                $('.mainMenu ul li.hasChildItem.active').removeClass('active');
                $(this)
                    .parent()
                    .toggleClass('active');
                $(this)
                    .next('ul.subMenu')
                    .slideToggle('slow');
            }
        });
    }

    // enables scrollReveal on base.html
    window.scrollReveal = new ScrollReveal();

    // back-to-top function
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn();
        } else {
            $('.back-to-top').fadeOut();
        }
    });

    $('.back-to-top').click(function() {
        $('html, body').animate(
            {
                scrollTop: 0,
            },
            600
        );
        return false;
    });

    //========================
    // Fixed Header
    //========================
    // if ($("#header").length > 0) {
    //     $(window).on('scroll', function () {
    //         if ($(window).scrollTop() > 5) {
    //             $("#header").addClass('fixedHeader');
    //         }
    //         else {
    //             $("#header").removeClass('fixedHeader');
    //         }
    //     });
    // }

    //========================
    // Nice Scroll
    //========================
    // $('html').niceScroll({
    //     cursorcolor: '#161616',
    //     cursorwidth: 8,
    //     cursorborder: false,
    //     zindex: 9999,
    //     cursorborderradius: 0,
    //     scrollspeed: 60,
    //     mousescrollstep: 60,
    // });

    //========================
    // PopUps
    //========================
    //open search popup
    $('#search .cd-popup-trigger').on('click', function(event) {
        event.preventDefault();
        $('#search .cd-popup').addClass('is-visible');
    });

    //close search popup
    $('#search .cd-popup').on('click', function(event) {
        if (
            $(event.target).is('#search .cd-popup-close') ||
            $(event.target).is('#search .cd-popup')
        ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close search popup when clicking the esc keyboard button
    $(document).keyup(function(event) {
        if (event.which == '27') {
            $('#search .cd-popup').removeClass('is-visible');
        }
    });

    //close CookielawBanner popup
    $('#CookielawBanner .cd-popup').on('click', function(event) {
        if (
            $(event.target).is('#CookielawBanner .cd-popup-close') ||
            $(event.target).is('#CookielawBanner .cd-popup')
        ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close CookielawBanner popup when clicking the esc keyboard button
    $(document).keyup(function(event) {
        if (event.which == '27') {
            $('#CookielawBanner .cd-popup').removeClass('is-visible');
        }
    });
});
