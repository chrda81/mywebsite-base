/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';

import 'core_assets/js/default/ajax-setup';

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Intercepting the forms submit function, to inject the searchword into the url => /search?searchword=<value>
    // Finally redirect to url
    $('#search-form').on('submit', function(e) {
        e.preventDefault();
        let searchword = $('#popup-search-searchword').val();

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: {
                searchword: searchword,
            },
            success: function(data) {
                console.log('Redirecting to ' + this.url);
                $(window).attr('location', this.url);
            },
            error: function(request, status, error) {
                console.log(request.responseText);
            },
        });
    });
});
