/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';
import 'node_modules/popper.js';
import 'node_modules/bootstrap';
import 'node_modules/jquery-parallax.js';

import 'core_assets/js/default/caption';
import 'core_assets/js/default/main';
import 'core_assets/js/default/blog-article';
import 'core_assets/js/default/search-form';
import 'core_assets/js/default/contact-form';

// const path = require('path');

// //file loader for imgages assets folder
// require('fs').readdirSync('../img/').forEach(function(file) {
//     if (file.match(/\.(jpe?g|png|svg|gif|ico)$/) && file !== 'index.js') {
//         let img = new Image();
//         img.src = require('../img/' + file);
//     }
// });

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Enable floating social share
    if ($('meta[property="floatingSocialButtons"]').attr('content')) {
        import('core_assets/js/default/social-share');
    }
});

// Hot module replacement for webpack
if (module.hot) {
    module.hot.dispose(() => {
        // some code for disposal goes here
    });
    module.hot.accept();
}
