# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.conf import settings
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand

from parler.models import TranslatableModel


class Command(BaseCommand):
    help = 'Create permissions for viewing specific language tabs in admin.'

    def handle(self, *args, **options):
        ct = ContentType.objects.get_for_model(TranslatableModel)

        site_id = getattr(settings, 'SITE_ID', None)
        for lang_dict in settings.PARLER_LANGUAGES.get(site_id, ()):
            codename = 'view_language_tab_{}'.format(lang_dict['code'])
            name = 'Can view language tab {}'.format(lang_dict['code'])
            view_tab = Permission.objects.get_or_create(name=name, codename=codename, content_type=ct)
