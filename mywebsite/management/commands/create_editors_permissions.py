# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand

from mywebsite_blog.models import Article, Category
from mywebsite_home.models import News


class Command(BaseCommand):
    help = 'Create an auth group for editors with all necessary permissions.'

    def handle(self, *args, **options):
        new_group, created = Group.objects.get_or_create(name='editors')

        # Add permissions to group 'editors' on home.model 'News'
        ct = ContentType.objects.get_for_model(News)
        can_add = Permission.objects.get(name__iexact='can add news', content_type=ct)
        can_change = Permission.objects.get(name__iexact='can change news', content_type=ct)
        can_delete = Permission.objects.get(name__iexact='can delete news', content_type=ct)
        new_group.permissions.add(can_add)
        new_group.permissions.add(can_change)
        new_group.permissions.add(can_delete)

        # Add permissions to group 'editors' on blog.model 'Category'
        ct = ContentType.objects.get_for_model(Category)
        can_add = Permission.objects.get(name__iexact='can add category', content_type=ct)
        can_change = Permission.objects.get(name__iexact='can change category', content_type=ct)
        can_delete = Permission.objects.get(name__iexact='can delete category', content_type=ct)
        new_group.permissions.add(can_add)
        new_group.permissions.add(can_change)
        new_group.permissions.add(can_delete)

        # Add permissions to group 'editors' on blog.model 'Article'
        ct = ContentType.objects.get_for_model(Article)
        can_add = Permission.objects.get(name__iexact='can add article', content_type=ct)
        can_change = Permission.objects.get(name__iexact='can change article', content_type=ct)
        can_delete = Permission.objects.get(name__iexact='can delete article', content_type=ct)
        new_group.permissions.add(can_add)
        new_group.permissions.add(can_change)
        new_group.permissions.add(can_delete)
