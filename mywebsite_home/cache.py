# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------

Use caching to avoid fetching model data when it doesn't have to
"""

from django.conf import settings
from django.core.cache import cache

DEFAULT_TIMEOUT = cache.default_timeout
CACHE_MAP = []


def get_item_cache_key(settingstype, name):
    """
    The low-level function to get the cache key for an item
    """
    # Always cache the entire object, as this already produces
    # a lot of queries. Don't go for caching individual fields.
    return 'websettings.{0}.{1}'.format(settingstype, name).replace(' ', '_')


def add_to_cache_map_parent_key(parent, child):
    """
    Build initial cache map
    """
    global CACHE_MAP
    is_dirty = True
    if any(CACHE_MAP):
        for (key, val) in CACHE_MAP:
            if key == child:
                is_dirty = False

    if is_dirty:
        CACHE_MAP.append((child, parent))


def get_cache_list_parent_key(child):
    """
    Get previous stored parent key of instance
    """
    global CACHE_MAP
    if any(CACHE_MAP):
        for (key, val) in CACHE_MAP:
            if key == child:
                return val

    return child


def add_to_cache(item, settingstype, name, version=1, timeout=DEFAULT_TIMEOUT):
    """
    Store a new item in the cache
    """
    if not settings.SITES_ENALBE_CACHING:
        return

    if item is None:
        raise ValueError("Can't cache this item!")

    key = get_item_cache_key(settingstype, name)
    cache.set(key, item, version=version, timeout=timeout)


def get_cached_values(settingstype, name, version=1, use_fallback=False):
    """
    Fetch a cached field
    """
    if not settings.SITES_ENALBE_CACHING or not settingstype or not name:
        return None

    key = get_item_cache_key(settingstype, name)
    values = cache.get(key, version=version)
    if not values:
        return None

    return values


def clear_older_cached_values(settingstype, name, version=1):
    """
    Delete a cached field
    """
    if not settings.SITES_ENALBE_CACHING or not settingstype or not name:
        return None

    for i in range(1, version + 1):
        key = get_item_cache_key(settingstype, name)
        cache.delete(key, i)
