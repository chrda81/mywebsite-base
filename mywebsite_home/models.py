# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from jsonfield import JSONField
from parler.models import TranslatableModel, TranslatedFields
from reversion.models import Version
from tinymce.models import HTMLField


# Abstract model
class TimeStampedModelMixin(models.Model):
    """
    Abstract Mixin model to add timestamp
    """
    # Timestamp
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('created_field')
    )
    updated = models.DateTimeField(
        auto_now=True,
        db_index=True,
        verbose_name=_('updated_field')
    )

    class Meta:
        abstract = True


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
class News(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(
            max_length=191,
            verbose_name=_('title_field')
        ),
        body=HTMLField(
            default='',
            verbose_name=_('body_field')
        )
    )
    date = models.DateField(
        default=now,
        verbose_name=_('date_field')
    )
    author = models.CharField(
        max_length=80,
        blank=True,
        verbose_name=_('author_field')
    )
    is_body_title_hidden = models.BooleanField(
        default=False,
        verbose_name=_('is_body_title_hidden_field')
    )
    is_published = models.BooleanField(
        default=False,
        verbose_name=_('is_published_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'home_news'
        ordering = ['-date']
        verbose_name = _('News')
        verbose_name_plural = _('Newss')

    def __unicode__(self):
        if self.language_code:
            return self.title
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def __str__(self):
        if self.language_code:
            return self.title
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)


# Define choices for settingstype
SETTINGSTYPE_CHOICES = (
    (1, _("Slider Item")),
    (2, _("Menu Link")),
    (3, _("Meta Tag")),
    (4, _("Advertising Item")),
    (5, _("Script Reference")),
    (6, _("Template Item"))
)


# Define choices for settingstype
VALUETYPE_CHOICES = (
    (1, _("String")),
    (2, _("List")),
    (3, _("Dictionary")),
    (4, _("Translation")),
    (5, _("StaticURL")),
    (6, _("MediaURL")),
    (7, _("URLResolver")),
    (8, _("Script"))
)


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
# Model for translations of WebsiteSettings
class WebsiteSettingsLocals(TranslatableModel):
    msgid = models.CharField(
        max_length=191,
        verbose_name=_('msgid_field')
    )
    translations = TranslatedFields(
        msgstr=models.TextField(
            blank=True,
            null=True,
            verbose_name=_('msgstr_field')
        ),
    )

    class Meta:
        db_table = 'home_websitesettingslocals'
        verbose_name = _('Website settings locale')
        verbose_name_plural = _('Website settings locales')

    def __unicode__(self):
        return self.msgid

    def __str__(self):
        return self.msgid


# Model for scripts of WebsiteSettings
class WebsiteSettingsScripts(models.Model):
    name = models.CharField(
        max_length=191,
        unique=True,
        verbose_name=_('name_field')
    )
    content_type = models.CharField(
        max_length=191,
        verbose_name=_('content_type_field')
    )
    body = models.TextField(
        verbose_name=_('body_field')
    )
    description = models.CharField(
        max_length=191,
        blank=True,
        null=True,
        verbose_name=_('description_field')
    )

    class Meta:
        db_table = 'home_websitesettingsscripts'
        verbose_name = _('Website settings scripts')
        verbose_name_plural = _('Website settings scriptss')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# Model for Settings
class Settings(TimeStampedModelMixin):
    name = models.CharField(
        max_length=30,
        verbose_name=_('name_field')
    )
    json = JSONField(
        verbose_name=_('json_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'home_appsettings'
        verbose_name = _('Settings')
        verbose_name_plural = _('Settingss')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


# Model for audit logging of registered models by reversion
class RevisionAudit(TimeStampedModelMixin):
    object_id = models.CharField(
        max_length=191,
        help_text="Primary key of the model under version control.",
    )
    object_repr = models.TextField(
        help_text="Pseudo name of the model under version control.",
    )
    content_type = models.ForeignKey(
        ContentType,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text="Content type of the model under version control.",
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("User"),
        help_text="The user who created this revision.",
    )
    action = models.CharField(
        max_length=20,
        help_text="The database action for this revision.",
    )
    comment = models.TextField(
        blank=True,
        verbose_name=_("Comment"),
        help_text="A text comment on this revision.",
    )
    version = models.ForeignKey(
        Version,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text="The object that contains this version.",
    )

    class Meta:
        db_table = 'revision_audit'
        ordering = ['-created']
        verbose_name = _('Revision Audit')
        verbose_name_plural = _('Revision Audits')

    def __str__(self):
        return '{} (ID: {}, Version: {})'.format(
            self.content_type,
            self.object_id,
            self.version.pk if self.version else ''
        )

    def __unicode__(self):
        return '{} (ID: {}, Version: {})'.format(
            self.content_type,
            self.object_id,
            self.version.pk if self.version else ''
        )
