# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver

from .cache import clear_older_cached_values, get_cache_list_parent_key
from .models import (SETTINGSTYPE_CHOICES, Settings,
                     WebsiteSettingsLocals, WebsiteSettingsScripts)
from .templatetags.core_settings_tags import getCoreSettingItemsFor
from .templatetags.websettings_tags import (getScriptFromWebSettings,
                                            transFromWebSettings)


# subscribe to event post_save of table 'Settings' to update the cache
@receiver(post_save, sender=Settings)
def post_save_core_settings_receiver(sender, instance, *args, **kwargs):
    if not transaction.get_connection().in_atomic_block:
        on_core_settings_post_save_impl(instance)
    else:
        transaction.on_commit(lambda: on_core_settings_post_save_impl(instance))


# Is triggered after commit transaction was fired!
# This is necessary, because getCoreSettingItemsFor() is using database calls on the saved object
def on_core_settings_post_save_impl(instance):
    for (pk, value) in SETTINGSTYPE_CHOICES:
        if hasattr(instance.json, 'settings_type') and pk == instance.json['settings_type']:
            # get previous stored parent key of instance
            name = get_cache_list_parent_key(instance.name)
            # clear cache item, so that it can be stored through same function than before
            clear_older_cached_values(instance.json['settings_type'], name)
            getCoreSettingItemsFor(value, name)


# subscribe to event post_save of table 'WebsiteSettingsScripts' to update the cache
@receiver(post_save, sender=WebsiteSettingsScripts)
def post_save_websettingsscripts_receiver(sender, instance, *args, **kwargs):
    if not transaction.get_connection().in_atomic_block:
        on_websettingsscripts_post_save_impl(instance)
    else:
        transaction.on_commit(lambda: on_websettingsscripts_post_save_impl(instance))


# Is triggered after commit transaction was fired!
# This is necessary, because transFromWebSettings() is using database calls on the saved object
def on_websettingsscripts_post_save_impl(instance):
    # clear cache item, so that it can be stored through same function than before
    settingstype = 8
    clear_older_cached_values(settingstype, instance.name)
    getScriptFromWebSettings(instance.name)


# subscribe to event post_save of table 'WebsiteSettingsLocals' to update the cache
@receiver(post_save, sender=WebsiteSettingsLocals)
def post_save_websettingslocale_receiver(sender, instance, *args, **kwargs):
    if not transaction.get_connection().in_atomic_block:
        on_websettingslocale_post_save_impl(instance)
    else:
        transaction.on_commit(lambda: on_websettingslocale_post_save_impl(instance))


# Is triggered after commit transaction was fired!
# This is necessary, because transFromWebSettings() is using database calls on the saved object
def on_websettingslocale_post_save_impl(instance):
    if instance.language_code:
        # clear cache item, so that it can be stored through same function than before
        settingstype = 'locale.%s' % instance.language_code
        clear_older_cached_values(settingstype, instance.msgid)
        transFromWebSettings(instance.msgid)
