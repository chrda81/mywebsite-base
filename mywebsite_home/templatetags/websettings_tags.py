# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os

from django import template
from django.conf import settings
from django.urls import reverse
from django.utils import translation
from django.utils.safestring import mark_safe

from mywebsite_home.cache import (add_to_cache, add_to_cache_map_parent_key,
                                  clear_older_cached_values, get_cached_values)
from mywebsite_home.models import (SETTINGSTYPE_CHOICES, WebsiteSettingsLocals,
                                   WebsiteSettingsScripts)

# Django template custom math filters
register = template.Library()


# returns tranlation for msgid from WebsiteSettingsLocals
@register.simple_tag
@mark_safe
def transFromWebSettings(msgid):
    """
    Translates a given parameter

    ``msgid`` of type string (mandatory)

    to a string, which is stored in the database. The translations can be done in runtime, without compiling
    the typical used django.po localized files. Returning string is 'safe', to avoid escaping HTML special characters.
    """
    settingstype = 'locale.%s' % translation.get_language()
    # TODO: set version for cache operations
    result = get_cached_values(settingstype, msgid)
    if result is None:
        result = ''
        queryset = WebsiteSettingsLocals.objects.active_translations().distinct().filter(msgid=msgid)
        if queryset:
            # Get first object from list. Btw., we should always have only one object!
            obj = queryset[0]
            result = obj.msgstr
            # store items to memcache
            add_to_cache(result, settingstype, msgid)  # add_to_cache(result, settingstype, msgid, version_incr)
            # clear older items
            # clear_older_cached_values(settingstype, msgid, version_incr)
    return result


# return iterateable list of object attributes
@register.filter
def getWebSettingsAttribItems(obj):
    """
    Returns an iteratable list of attributes from given parameter

    ``obj`` of type object (mandatory)

    which was collected with template tag function ``getCoreSettingItemsFor``.

    Sample usage::

        {% getCoreSettingItemsFor _('Meta Tag') 'HTML Tags' as object_list %}
        {% for object in object_list %}
            {% for obj in object|getWebSettingsAttribItems %}
                <meta name="{{ obj|getAttribItemKey }}" content="{{ obj|getAttribItemValue|handleWebSettingsType }}">
            {% endfor %}
        {% endfor %}

    Each attribute object in that list is a type of ``dictionary`` and contains a key and a value. The filter tags
    ``getAttribItemKey``, ``getAttribItemValue`` and ``handleWebSettingsType`` are used normally in addition with
    that, to extract the attributes values.
    """
    if type(obj) is dict:
        result_list = []
        for key, value in obj.items():
            # the only tuples in object should be attributes

            if type(value) is tuple:
                # if obj is attribute tuple
                result_list.append({'key': key, 'value': value})

    return result_list


# return the named attribute's value from a list of object attributes
@register.filter
def getWebSettingsNamedAttrib(obj, name):
    """
    Returns the named attribute's value from given parameter

    ``obj`` of type object (mandatory)

    which was collected with template tag function ``getCoreSettingItemsFor``.

    Sample usage::

        {% getCoreSettingItemsFor _('Meta Tag') 'Facebook Tags' as object_list %}
        {% with object_list|getWebSettingsNamedAttrib:'og:image'|getAttribItemValue|handleWebSettingsType as og_image%}
            <meta property="og:image" content="{{ og_image }}">
        {% endwith %}

    If the named attribute was found, the result contains a key and a value, othwise it is None. The filter tags
    ``getAttribItemValue`` and ``handleWebSettingsType`` are used normally in addition with
    that, to extract the attribute's value.
    """
    if type(obj) is list:
        for item in obj:
            if type(item) is dict:
                for key, value in item.items():
                    if key == name:
                        return {'key': key, 'value': value}

    return None


# return objects key
@register.filter(is_safe=True)
def getAttribItemKey(obj):
    """
    Returns the key name of the given parameter

    ``obj`` of type dictionary (mandatory)

    Returning string is 'safe', to avoid escaping HTML special characters.
    """
    if type(obj) is dict:
        return obj['key']

    return None


# return objects value
@register.filter(is_safe=True)
def getAttribItemValue(obj):
    """
    Returns the value name of the given parameter

    ``obj`` of type dictionary (mandatory)

    Returning string is 'safe', to avoid escaping HTML special characters.
    """
    if type(obj) is dict:
        return obj['value']

    return None


# return value of the specific attribute's valuetype
@register.filter(is_safe=True)
def handleWebSettingsType(attribute):
    """
    Returns the converted value of the given parameter

    ``attribute`` of type tuple (mandatory)

    The following types can be handled:

    String, List, Dictionary, Translation, StaticURL, MediaURL

    Returning string is 'safe', to avoid escaping HTML special characters.
    """
    if type(attribute) is tuple:
        valuetype, value = attribute
        if valuetype in (1, 2, 3):  # type: String, List, Dictionary
            return value

        if valuetype == 4:  # type: Translation
            return transFromWebSettings(value)

        if valuetype == 5:  # type: Static URL
            return os.path.join(settings.STATIC_URL, value)

        if valuetype == 6:  # type: Media URL
            return os.path.join(settings.MEDIA_URL, value)

        if valuetype == 7:  # type: URL Resolver
            return reverse(value)

        if valuetype == 8:  # type: Script
            return getScriptFromWebSettings(value)

    # if attribute is no tuple, don't treat it special
    return attribute


def getScriptFromWebSettings(name):
    """
    Fetches script by

    ``name`` of type string (mandatory)

    which is stored in the database.
    """
    # TODO: set version for cache operations
    settingstype = 8
    result = get_cached_values(settingstype, name)
    if result is None:
        try:
            script = WebsiteSettingsScripts.objects.get(name=name)
            result = script.body
            # store items to memcache
            add_to_cache(result, settingstype, name)  # add_to_cache(result_list, settingstype, name, version_incr)
            # clear older items
            # clear_older_cached_values(settingstype, name, version_incr)
            return result
        except WebsiteSettingsScripts.DoesNotExist:
            return "Cannot load script '{}'!".format(name)

    else:
        return result
