# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import template
from django.urls import reverse_lazy
from django.utils import translation

from menu import Menu

register = template.Library()
lang_code = None


class MenuNode(template.Node):
    def __init__(self):
        pass

    def render(self, context):
        global lang_code
        # if a 500 error happens we get an empty context, in which case
        # we should just return to let the 500.html template render
        if 'request' not in context:
            return '<!-- menu failed to render due to missing request in the context -->'

        request = context['request']

        if translation.LANGUAGE_SESSION_KEY in request.session:
            if Menu.loaded and lang_code is not request.session[translation.LANGUAGE_SESSION_KEY]:
                # memorize current language code
                lang_code = request.session[translation.LANGUAGE_SESSION_KEY]
                # reverse stored url to append language prefix
                for name in Menu.items:
                    for item in Menu.items[name]:
                        if 'reverse_url' in item.kwargs:
                            if 'kwargs' in item.kwargs:
                                item.url = reverse_lazy(item.kwargs['reverse_url'],
                                                        kwargs=item.kwargs['kwargs'])
                            else:
                                item.url = reverse_lazy(item.kwargs['reverse_url'])

                        if item.children:
                            for submenu in item.children:
                                if 'reverse_url' in submenu.kwargs:
                                    if 'kwargs' in submenu.kwargs:
                                        submenu.url = reverse_lazy(submenu.kwargs['reverse_url'],
                                                                   kwargs=submenu.kwargs['kwargs'])
                                    else:
                                        submenu.url = reverse_lazy(submenu.kwargs['reverse_url'])

            if not Menu.loaded:
                # memorize current language code
                lang_code = request.session[translation.LANGUAGE_SESSION_KEY]

        menus = Menu.process(request)

        # recursively find the selected item
        def find_selected(menu):
            process = []
            _url = '{}:{}'.format(request.resolver_match.namespace, request.resolver_match.url_name)
            for item in menu:
                if 'reverse_url' in item.kwargs and item.kwargs['reverse_url'] == _url:
                    return item
                if len(item.children) > 0:
                    process.append(item)
            for item in process:
                r = find_selected(item.children)
                if r is not None:
                    return r
            return None

        selected_menu = None
        for name in menus:
            found_menu = find_selected(menus[name])
            if found_menu:
                if selected_menu is None or len(selected_menu.url) < len(found_menu.url):
                    # since our call to Menu.process above allows for menus
                    # in multiple menus we reset the selected attr to False
                    # if we find a more specific match
                    if selected_menu is not None:
                        selected_menu.selected = False

                    selected_menu = found_menu

            else:
                # an url was called which is not configured in the menu, so we select the parent menu item by namespace
                _ns = request.resolver_match.namespace
                for item in menus[name]:
                    if 'reverse_url' in item.kwargs and _ns in item.kwargs['reverse_url']:
                        selected_menu = item

        # now for the submenu
        submenu = []
        has_submenu = False
        selected_menu_parent = None
        if selected_menu is not None:
            if selected_menu.parent is not None:
                submenu = selected_menu.parent.children
                has_submenu = True
            elif len(selected_menu.children) > 0:
                submenu = selected_menu.children
                has_submenu = True

            # set parent of selected_menu
            if selected_menu.parent is None:
                selected_menu_parent = selected_menu
            else:
                selected_menu_parent = selected_menu.parent

        # set the items in our context
        context['menus'] = menus
        context['selected_menu'] = selected_menu
        context['selected_menu_parent'] = selected_menu_parent
        context['submenu'] = submenu
        context['has_submenu'] = has_submenu

        return ''


@register.tag
def generate_simple_menu(parser, token):
    """
    django-simple-menu==1.2.1 cannot handle localization. As work-around we use our own
    templatetags function `generate_simple_menu` to reverse() the urls. In addition
    we need a kwargs 'reverse_url' = 'namespace:url' for that.
    """
    return MenuNode()
