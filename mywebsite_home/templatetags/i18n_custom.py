# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import template
from django.conf import settings
from django.utils import translation

from mywebsite.utils.tools import get_default_language

# Django template custom math filters
register = template.Library()


# return iterateable list of object attributes
@register.filter(name='translate_url')
def do_translate_url(url):
    lang_code = translation.get_language()
    if lang_code != get_default_language():
        url = "/%s%s" % (lang_code, url)
    return url
