# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json
import random

from django import template
from django.conf import settings as settings_file
from django.db import models
from django.db.utils import OperationalError, ProgrammingError
from django.utils.translation import gettext as _

from mywebsite.utils.tools import template_from_string
from mywebsite_home.cache import add_to_cache, add_to_cache_map_parent_key, clear_older_cached_values, get_cached_values
from mywebsite_home.models import SETTINGSTYPE_CHOICES, Settings
from mywebsite_home.templatetags import websettings_tags

# Django template custom math filters
register = template.Library()


@register.simple_tag
def site_vars():
    site_vars_data = {}
    site_vars_data['FLOATING_SOCIAL_BUTTONS'] = settings_file.FLOATING_SOCIAL_BUTTONS
    return site_vars_data


@register.simple_tag
def getCoreSettingsByName(name):
    settings = {}
    try:
        # get last active dataset
        settings = Settings.objects.filter(name=name, is_archived=False).latest('updated')
    except (models.ObjectDoesNotExist, OperationalError, ProgrammingError):
        pass
    return settings


# return iterateable list of objects
@register.simple_tag
def getCoreSettingItemsFor(settingstype, name, shuffel=None):
    """
    Returns an iteratable list of objects for given parameters

    ``settingstype`` of type integer (mandatory)
    ``name`` of type string (mandatory)
    ``shuffel`` of type integer (optional), which limits result list to given amount of randomized objects

    Sample usage::

        {% getCoreSettingItemsFor _('Meta Tag') 'HTML Tags' as object_list %}
        {% for object in object_list %}
            {% for obj in object|getWebSettingsAttribItems %}
                <meta name="{{ obj|getAttribItemKey }}" content="{{ obj|getAttribItemValue|handleWebSettingsType }}">
            {% endfor %}
        {% endfor %}

    In this example, the search arguments ``settingstype`` = 'Meta Tag' and ``name`` = 'HTML Tags'
    are using the icontains lookup operator to fetch the items.
    """
    obj = {}
    # TODO: set version for cache operations
    # get all matching settingstypes by its name
    for (pk, value) in SETTINGSTYPE_CHOICES:
        if value == settingstype:
            result_list = get_cached_values(pk, name)

            if result_list is None:
                result_list = []
                queryset = getCoreSettingsByName(name)
                if queryset:
                    # get items from json
                    for (key, config_list) in queryset.json['settings_items'].items():
                        # get all attributes and their value, type
                        for (attr, attr_list) in config_list.items():
                            if obj:
                                if key == obj['name']:
                                    obj[attr] = (attr_list['type'], attr_list['value'])
                                else:
                                    result_list.append(obj)
                                    obj = {'name': key, attr: (attr_list['type'], attr_list['value']),
                                           'settingstype': queryset.json['settings_type']}
                                    add_to_cache_map_parent_key(name, key)

                            else:
                                obj = {'name': key, attr: (attr_list['type'], attr_list['value']),
                                       'settingstype': queryset.json['settings_type']}

                    # if query is the last item of the queryset list, append obj to result_list
                    if obj:
                        result_list.append(obj)
                        add_to_cache_map_parent_key(name, key)
            else:
                # Rebuild cache map from memcache
                for obj in result_list:
                    child_name = obj['name']
                    if type(child_name) is str:
                        add_to_cache_map_parent_key(name, child_name)

    # get random items of result_list, if shuffel was set as param
    if shuffel is not None:
        item_count = len(result_list)
        if item_count < shuffel:
            shuffel = item_count
        result_list = random.sample(list(result_list), k=shuffel)

    return result_list


def getTemplateByName(name):
    # get possible template with attributes from database
    template_str = ""
    subject_str = ""
    attachments_str = ""
    attachments = []
    for template_items in getCoreSettingItemsFor(_('Template Item'), 'App Templates'):
        if template_items and template_items['name'] == name:
            for template_attr in websettings_tags.getWebSettingsAttribItems(template_items):
                if 'content' == websettings_tags.getAttribItemKey(template_attr):
                    template_str = websettings_tags.handleWebSettingsType(
                        websettings_tags.getAttribItemValue(template_attr))

                if 'subject' == websettings_tags.getAttribItemKey(template_attr):
                    subject_str = websettings_tags.handleWebSettingsType(
                        websettings_tags.getAttribItemValue(template_attr))

                if 'attachments' == websettings_tags.getAttribItemKey(template_attr):
                    attachments_str = websettings_tags.handleWebSettingsType(
                        websettings_tags.getAttribItemValue(template_attr))

                    for elem in attachments_str.strip('[]').split(','):
                        if str(elem):
                            file_name = elem.strip("'/")
                            attachments.append(file_name)

    return {
        'template': template_from_string(template_str),
        'subject': template_from_string(subject_str),
        'attachments': attachments
    }


@register.tag('include_maybe')
def do_include_maybe(parser, token):
    "Source: http://stackoverflow.com/a/18951166/15690"
    bits = token.split_contents()
    if len(bits) < 2:
        raise template.TemplateSyntaxError(
            "%r tag takes at least one argument: "
            "the name of the template to be included." % bits[0])

    try:
        silent_node = template.loader_tags.do_include(parser, token)
    except template.TemplateDoesNotExist:
        # Django < 1.7
        return template.defaulttags.CommentNode()

    _orig_render = silent_node.render

    def wrapped_render(*args, **kwargs):
        try:
            return _orig_render(*args, **kwargs)
        except template.TemplateDoesNotExist:
            return template.defaulttags.CommentNode()
    silent_node.render = wrapped_render
    return silent_node


@register.filter
@template.defaultfilters.stringfilter
def template_exists(value):
    try:
        template.loader.get_template(value)
        return True
    except template.TemplateDoesNotExist:
        return False
