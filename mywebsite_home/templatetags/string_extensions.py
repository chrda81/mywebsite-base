# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import template

register = template.Library()
lang_code = None


@register.simple_tag
def replace(value, arg1, arg2):
    return value.replace(arg1, arg2)


@register.filter(name='split')
def split(value, arg):
    return value.split(arg)


@register.filter(name='icontains')
def icontains(value, arg):
    if type(value) is list:
        for obj in value:
            if arg.lower() in obj.strip().lower():
                return True
    else:
        if arg.lower() in value.strip().lower():
            return True

    return False


@register.filter(name='contains')
def contains(value, arg):
    if type(value) is list:
        for obj in value:
            if arg in obj.strip():
                return True
    else:
        if arg in value.strip():
            return True

    return False


@register.filter(name='iexact')
def iexact(value, arg):
    if type(value) is list:
        for obj in value:
            if arg.lower() == obj.strip().lower():
                return True
    else:
        if arg.lower() == value.strip().lower():
            return True

    return False


@register.filter(name='exact')
def exact(value, arg):
    if type(value) is list:
        for obj in value:
            if arg == obj.strip():
                return True
    else:
        if arg == value.strip():
            return True

    return False
