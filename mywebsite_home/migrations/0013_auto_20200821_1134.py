# Generated by Django 2.0.10 on 2020-08-21 11:34

from django.db import migrations
import django.db.models.deletion
import parler.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mywebsite_home', '0012_auto_20191218_2217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newstranslation',
            name='master',
            field=parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='mywebsite_home.News'),
        ),
        migrations.AlterField(
            model_name='websitesettingslocalstranslation',
            name='master',
            field=parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='mywebsite_home.WebsiteSettingsLocals'),
        ),
    ]
