# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from captcha.fields import CaptchaField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.shortcuts import reverse
from django.utils.translation import gettext_lazy as _
from parler.forms import TranslatableModelForm
from reversion.models import Version

from mywebsite.utils.widgets import AceWidget, JSONEditorWidget, TinyMCEWidget
from mywebsite_antispam.honeypot.forms import HoneypotField
from mywebsite_antispam.spamdetector.forms import SpamDetectorField

from . import models


class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True)
    contact_email = forms.EmailField(required=True)
    url = HoneypotField()
    message = SpamDetectorField(required=True, widget=forms.Textarea, max_length=2048)
    captcha = CaptchaField()  # required=True must be set via jQuery in template, because of CaptchaField

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'ajax-form-contact'
        self.helper.form_class = 'form-validate well'
        self.helper.form_method = 'POST'
        self.helper.form_action = reverse('home:contact')
        self.helper.attrs.update({'novalidate': ''})
        # translation for fields
        self.fields['contact_name'].label = _('contact_name')
        self.fields['contact_email'].label = _('contact_email')
        self.fields['message'].label = _('contact_message')
        self.fields['captcha'].label = _('contact_captcha')
        # add submit button
        self.helper.add_input(Submit('submit', _("CONTACT_SUBMIT"), css_class="btn btn-dark text-white"))


class NewsAdminForm(TranslatableModelForm):
    body = forms.CharField(widget=TinyMCEWidget(
        attrs={'cols': 30, 'rows': 10}
    ))

    class Meta:
        model = models.News
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(NewsAdminForm, self).__init__(*args, **kwargs)
        self.fields['body'].label = _('body_field')


class ScriptsEditorAdminForm(forms.ModelForm):
    class Meta:
        model = models.WebsiteSettingsScripts
        widgets = {
            'body': AceWidget(
                mode="css",
                theme="tomorrow_night_eighties",
                width="100%",
                height="500px",
                showprintmargin=True,
            ),
        }
        exclude = ()


class VersionAdminForm(forms.ModelForm):
    class Meta:
        model = Version
        widgets = {
            'serialized_data': JSONEditorWidget
        }
        exclude = ()
