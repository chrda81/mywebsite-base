# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from datetime import datetime

from django.db.models import Q

from django_filters import FilterSet
from parler.utils import get_active_language_choices

from mywebsite_blog import models as blogmodels


class ArticleFilter(FilterSet):
    class Meta:
        model = blogmodels.Article
        exclude = ()

    @property
    def qs(self):
        # The request url looks like /search?searchword=<value> and we build a multiple or lookup_expr
        # with that.
        searchword = self.data.get('searchword', '')
        parent = super(ArticleFilter, self).qs
        filter_list = parent.active_translations().distinct().filter(Q(date__lte=datetime.now())
                                                                     & (Q(is_published=True, is_archived=False)
                                                                        | Q(is_internal=True, is_archived=False)))
        filter_list = filter_list.filter(Q(translations__title__icontains=searchword)
                                         | Q(translations__body__icontains=searchword)
                                         | Q(translations__teaser__icontains=searchword)
                                         | Q(slug__icontains=searchword)).order_by('-date')
        return filter_list
