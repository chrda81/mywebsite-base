# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os
from pathlib import Path

from django.conf import settings
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.admin.utils import quote
from django.forms.utils import flatatt
from django.template.defaultfilters import truncatechars
from django.urls import reverse
from django.utils.html import format_html
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

import reversion
from jet.filters import DateRangeFilter
from jsonfield import JSONField
from parler.admin import TranslatableAdmin
from related_admin import RelatedFieldAdmin
from reversion_compare.admin import CompareVersionAdmin

from mywebsite.utils.mixins import ExtendedActionsMixin, TranslateableReversionAdminMixin
from mywebsite.utils.widgets import JSONEditorWidget

from .forms import NewsAdminForm, ScriptsEditorAdminForm, VersionAdminForm
from .models import (
    SETTINGSTYPE_CHOICES, VALUETYPE_CHOICES, News, RevisionAudit, Settings, WebsiteSettingsLocals,
    WebsiteSettingsScripts
)


@admin.register(News)
class NewsAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    form = NewsAdminForm
    fieldsets = (
        (None, {
            'fields': ('title', 'author', 'date', ('is_published', 'is_archived', 'is_body_title_hidden'), 'body'),
        }),
    )
    list_display = ('title', 'date', 'author', 'is_published', 'is_archived')
    list_editable = ('is_published', 'is_archived')
    search_fields = ['translations__title', 'translations__body']

    def save_model(self, request, obj, form, change):
        if not obj.author:
            username = request.user.username
            # remap administrator name to 'admin' as small security optimazation, because it is a risk to show
            # real admin username at articles and news sites)
            if 'admin' in username.lower():
                username = 'admin'
            obj.author = username
        super().save_model(request, obj, form, change)


@admin.register(WebsiteSettingsLocals)
class WebsiteSettingsLocalsAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    list_display = ('msgid', 'msgstr')
    search_fields = ['msgid', 'translations__msgstr']
    ordering = ('msgid',)


@admin.register(WebsiteSettingsScripts)
class WebsiteSettingsScriptsAdmin(CompareVersionAdmin):
    form = ScriptsEditorAdminForm
    fieldsets = (
        (None, {
            'fields': ('name', 'content_type', 'description', 'body'),
        }),
    )
    list_display = ('name', 'content_type', 'description')
    search_fields = ['name', 'body', 'description']
    list_filter = ('content_type',)
    ordering = ('name', 'content_type')


@admin.register(Settings)
class SettingsAdmin(ExtendedActionsMixin, CompareVersionAdmin):
    # concaternate settings types
    settingstypes_str = ''
    for (key, value) in SETTINGSTYPE_CHOICES:
        settingstypes_str = '%s; %s' % (settingstypes_str, '{}: {}'.format(key, value))

    # concaternate value types
    valuetypes_str = ''
    for (key, value) in VALUETYPE_CHOICES:
        valuetypes_str = '%s; %s' % (valuetypes_str, '{}: {}'.format(key, value))

    fieldsets = (
        (None, {
            'fields': ('name', 'json', 'is_archived'),
            'description': _("For the attribute <b>settings_type</b> use the ID of one of the following "
                             "choices: %(settingstype)s.<br />For all other <b>_type</b> attributes use the ID of one "
                             "of the following choices: %(valuestype)s.") %
            {'settingstype': settingstypes_str.strip('; '), 'valuestype': valuetypes_str.strip('; ')}
        }),
    )
    list_display = ('name', 'json', 'created', 'updated', 'is_archived')
    list_editable = ('is_archived',)
    search_fields = ['name', 'json']
    list_filter = ('is_archived', 'name')
    formfield_overrides = {
        JSONField: {'widget': JSONEditorWidget},
    }
    actions = ['reload_config', ]
    extended_actions = ['reload_config', ]
    ordering = ('name', 'is_archived')

    def reload_config(self, request, queryset):
        # touch uwsgi_file to restart the uwsgi emperor service and reload the configuration
        uwsgi_file = os.path.join(settings.BASE_DIR, 'sites_uwsgi.ini')
        Path(uwsgi_file).touch()
    reload_config.short_description = _('Reload Configuration')


# re-add VersionAdmin with custom form
if hasattr(settings, 'ADD_REVERSION_ADMIN') and settings.ADD_REVERSION_ADMIN:
    class VersionAdmin(RelatedFieldAdmin, admin.ModelAdmin):
        form = VersionAdminForm
        list_display = ('revision__date_created', 'short_revision', 'short_object_repr',
                        'object_id', 'revision__comment', 'revision__user', 'content_type')
        list_display_links = ('short_revision', 'object_id')
        list_filter = ('content_type', 'format', ('revision__date_created', DateRangeFilter))
        search_fields = ('object_repr', 'serialized_data')

        def short_revision(self, obj):
            return truncatechars(obj.revision, 50)

        def short_object_repr(self, obj):
            return truncatechars(obj.object_repr, 50)

        # Set the column name in the change list
        short_revision.short_description = 'revision'
        short_object_repr.short_description = 'object_repr'

        # Set the field to use when ordering using this column
        short_revision.admin_order_field = 'revision'
        short_object_repr.admin_order_field = 'object_repr'

    admin.site.unregister(reversion.models.Version)
    admin.site.register(reversion.models.Version, VersionAdmin)


class ActionFilter(SimpleListFilter):
    title = _('Action')
    parameter_name = 'action'

    def lookups(self, request, model_admin):
        return [
            ('Added', _('Added')),
            ('Changed', _('Changed')),
            ('Deleted', _('Deleted')),
            ('Initialized', _('Initialized')),
            ('Recovered', _('Recovered'))]

    def queryset(self, request, queryset):
        if self.value() == 'Added':
            return queryset.filter(action__iexact='Added')
        elif self.value() == 'Changed':
            return queryset.filter(action__iexact='Changed')
        elif self.value() == 'Deleted':
            return queryset.filter(action__iexact='Deleted')
        elif self.value() == 'Initialized':
            return queryset.filter(action__iexact='Initialized')
        elif self.value() == 'Recovered':
            return queryset.filter(action__iexact='Recovered')

        return queryset.all()


@admin.register(RevisionAudit)
class RevisionAuditAdmin(RelatedFieldAdmin, admin.ModelAdmin):
    list_display = ('created', 'version_link', 'short_object_repr',
                    'object_id', 'content_type', 'user', 'action_translated', 'comment')
    list_filter = (ActionFilter, 'content_type', ('created', DateRangeFilter))
    search_fields = ['object_id', 'comment', 'object_repr']
    ordering = ('-created', 'content_type')

    def action_translated(self, obj):
        if obj.action == 'Added':
            return _('Added')
        elif obj.action == 'Changed':
            return _('Changed')
        elif obj.action == 'Initialized':
            return _('Initialized')
        elif obj.action == 'Recovered':
            return _('Recovered')
        elif obj.action == 'Deleted':
            return _('Deleted')
        else:
            return obj.action

    def short_object_repr(self, obj):
        return truncatechars(obj.object_repr, 50)

    def version_link(self, obj):
        version = obj.version
        if version:
            opts = version._meta
            route = '{}:{}_{}_change'.format(self.admin_site.name, opts.app_label, opts.model_name)
            version_edit_url = reverse(route, args=[version.pk])
            return format_html(
                '<a{}>{}</a>', flatatt({'href': version_edit_url}), truncatechars(version, 50))

    # Set the column name in the change list
    action_translated.short_description = _('Action')
    version_link.short_description = _('Version')
    short_object_repr.short_description = 'object_repr'

    # Set the field to use when ordering using this column
    action_translated.admin_order_field = 'action'
    version_link.admin_order_field = 'version'
    short_object_repr.admin_order_field = 'object_repr'
