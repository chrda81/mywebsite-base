# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.test import TestCase
from django.urls import resolve, reverse
from django.utils.translation import gettext as _

from mywebsite_blog.models import Article, Category


class URLTests(TestCase):
    def setUp(self):
        super(URLTests, self).setUp()

    def assert_link_response(self, named_url, expected_url, args=None):
        if args:
            reverse_url = reverse(named_url, kwargs=args)
        else:
            reverse_url = reverse(named_url)
        # reverse_url matches expected_url string
        self.assertEqual(reverse_url, expected_url)
        # resolved view name matches
        self.assertEqual(resolve(expected_url).view_name, named_url)
        # Check that the response is 200 OK.
        response = self.client.get(reverse_url)
        self.assertEqual(response.status_code, 200)

    def test_index(self):
        self.assert_link_response("home:index", "/")

    def test_search(self):
        self.assert_link_response("home:search", "/search")

    def test_about_me(self):
        self.assert_link_response("home:about-me", "/about-me")

    def test_crowdfunding(self):
        self.assert_link_response("home:crowdfunding", "/crowdfunding")

    def test_disclosures(self):
        self.assert_link_response("home:disclosures", "/disclosures")

    def test_data_privacy(self):
        self.assert_link_response("home:data-privacy", "/data-privacy")

    def test_contact(self):
        # for captcha tests, see https://stackoverflow.com/questions/3159284/how-to-unit-test-a-form-with-a-captcha-field-in-django
        self.assert_link_response("home:contact", "/contact")

    def test_terms_and_conditions(self):
        self.assert_link_response("home:terms-and-conditions", "/terms-and-conditions")


class ViewTests(TestCase):
    def setUp(self):
        self.category = Category(
            name="Test category",
            slug="test-category"
        )
        self.category.save()
        self.article = Article(
            title="Test article",
            slug="test-article",
            category=self.category,
            teaser="This is the teaser of the test article.",
            body="",
            is_published=False,
            is_archived=False,
            is_internal=False,
        )
        self.article.save()
        super(ViewTests, self).setUp()

    def test_search_view_no_filter(self):
        response = self.client.get('/search?searchword=OMEGA13')
        self.assertContains(response, _('SEARCH_NORESULTS'))

    def test_search_view_condition1(self):
        # Hardcoded filter condition for article match:
        #   Q(is_published=True, is_archived=False) or Q(is_internal=True, is_archived=False)
        self.article.is_published = True
        self.article.is_archived = False
        self.article.is_internal = False
        self.article.save()
        response = self.client.get('/search?searchword=test%20article')
        self.assertContains(response, self.article.teaser)

    def test_search_view_not_condition1(self):
        # Hardcoded filter condition for article match:
        #   Q(is_published=True, is_archived=False) or Q(is_internal=True, is_archived=False)
        self.article.is_published = False
        self.article.is_archived = True
        self.article.is_internal = False
        self.article.save()
        response = self.client.get('/search?searchword=test%20article')
        self.assertContains(response, _('SEARCH_NORESULTS'))

    def test_search_view_condition2(self):
        # Hardcoded filter condition for article match:
        #   Q(is_published=True, is_archived=False) or Q(is_internal=True, is_archived=False)
        self.article.is_published = False
        self.article.is_archived = False
        self.article.is_internal = True
        self.article.save()
        response = self.client.get('/search?searchword=test%20article')
        self.assertContains(response, self.article.teaser)

    def test_search_view_not_condition2(self):
        # Hardcoded filter condition for article match:
        #   Q(is_published=True, is_archived=False) or Q(is_internal=True, is_archived=False)
        self.article.is_published = True
        self.article.is_archived = True
        self.article.is_internal = False
        self.article.save()
        response = self.client.get('/search?searchword=test%20article')
        self.assertContains(response, _('SEARCH_NORESULTS'))
