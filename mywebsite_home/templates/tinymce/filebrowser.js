/*--------------------------------------------------------------
 *  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.
 *
 *  This Program may be used by anyone in accordance with the terms of the
 *  German Free Software License
 *
 *  The License may be obtained under http://www.d-fsl.org.
 *-------------------------------------------------------------*/

function djangoFileBrowser(callback, value, meta) {
    var url = '{{ fb_url }}?pop=5&type=' + meta.filetype;

    tinyMCE.activeEditor.windowManager.openUrl({
        title: 'Django Filebrowser',
        url: url,
        width: window.innerWidth * 0.8,
        height: window.innerHeight * 0.9,
        resizable: 'yes',
        scrollbars: 'yes',
        inline: 'no',
        close_previous: 'no',
        popup_css: false, // Disable TinyMCE's default popup CSS
        onMessage: function(dialogApi, details) {
            callback(details.content);
            dialogApi.close();
        },
    });
    return false;
}
