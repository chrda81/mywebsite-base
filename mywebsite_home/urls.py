# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.urls import path

from mywebsite_blog import views as blogviews

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.NewsListView.as_view(), name='index'),
    path('set_language', views.set_language, name='set_language'),
    path('search', views.SearchListView.as_view(), name='search'),
    path('contact', views.ContactView.as_view(), name='contact')
]

# add AJAX based blog articles dynamically
for item in blogviews.getSlugByMenuItemName(None):
    # extract url and name
    url = item['slug']
    name = item['name'].replace('Item ', '')

    # add path to urlpatterns
    urlpatterns.append(path(url, blogviews.AjaxArticleDetailView.as_view(link_name=name), name=name))
