# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os
from datetime import datetime
from itertools import chain
from operator import attrgetter

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.template.context_processors import csrf
from django.template.loader import get_template
from django.urls import translate_url
from django.utils import translation
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.views import generic
from django.views.decorators.csrf import requires_csrf_token

from crispy_forms.utils import render_crispy_form
from django_filters.views import FilterView

from mywebsite.utils.tools import LazyEncoder, get_default_language
from mywebsite_antispam import models as antispammodels
from mywebsite_blog import models as blogmodels
from mywebsite_blog import views as blogviews
from mywebsite_home.templatetags.core_settings_tags import getTemplateByName

from .filters import ArticleFilter
from .forms import ContactForm
from .models import News


class NewsListView(blogviews.ArticleCommonDataMixin, generic.ListView):
    model = News
    context_object_name = 'news_list'
    template_name = 'home/index.html'
    paginate_by = settings.PAGINATION_NEWS
    paginator_class = blogviews.CustomPaginator

    def render_to_response(self, context, **response_kwargs):
        response = super(NewsListView, self).render_to_response(context, **response_kwargs)
        # set cookie for FORCE_SCRIPT_NAME setting, because this is used for TinyMCE CustomFileBrowser's callback
        response.set_cookie("force_script_name", settings.FORCE_SCRIPT_NAME)
        return response

    def get_queryset(self):
        queryset = super(NewsListView, self).get_queryset()
        # standard filter for News
        news_queryset = queryset.active_translations().distinct().filter(
            is_published=True, is_archived=False, date__lte=datetime.now())
        # addidtional filter for Articles, which should be shown in news list
        article_queryset = blogmodels.Article.objects.active_translations().distinct().filter(is_onnewsfeed=True,
                                                                                              is_internal=False,
                                                                                              is_published=True,
                                                                                              is_archived=False,
                                                                                              date__lte=datetime.now())
        # combine both querysets and sort with date desc
        queryset = sorted(chain(news_queryset, article_queryset), key=attrgetter('date'), reverse=True)
        return queryset


def set_language(request):
    """
    Redirect to a given URL while setting the chosen language in the session or
    cookie. The URL and the language code need to be specified in the request
    parameters.

    Since this view changes how the user will see the rest of the site, it must
    only be accessed as a POST request. If called as a GET request, it will
    redirect to the page in the request (the 'next' parameter) without changing
    any state.
    """
    next = request.POST.get('next', request.GET.get('next'))
    if not next:
        next = request.META.get('HTTP_REFERER', None)
    if not next:
        next = '/'
    response = HttpResponseRedirect(next) if next else HttpResponse(status=204)
    if request.method == 'POST':
        lang_code = request.POST.get('language', get_default_language())
        if lang_code:
            if next:
                next_trans = translate_url(next, lang_code)
                if next_trans != next:
                    response = HttpResponseRedirect(next_trans)
            if hasattr(request, 'session'):
                request.session[translation.LANGUAGE_SESSION_KEY] = lang_code
            # Always set cookie
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, lang_code,
                max_age=settings.LANGUAGE_COOKIE_AGE,
                path=settings.LANGUAGE_COOKIE_PATH,
                domain=settings.LANGUAGE_COOKIE_DOMAIN,
            )
            # Switch language for current thread
            translation.activate(lang_code)
    return response


# added custom view for manual
# see http://www.beardygeek.com/2010/03/adding-views-to-the-django-admin/
@staff_member_required
def manual(request):
    return render(request, 'admin_doc/admin_manual.html', {'title': _('Manual')})


@method_decorator(requires_csrf_token, name='dispatch')
class ContactView(blogviews.ArticleCommonDataMixin, generic.edit.FormView):
    template_name = 'home/contact.html'
    form_class = ContactForm
    success_url = '/contact'

    def form_invalid(self, form):
        response = super().form_invalid(form)

        if self.request.is_ajax():
            if form.has_error('url', code='spam-protection') or form.has_error('message', code='spam-protection'):
                contact_name = self.request.POST.get('contact_name', '')
                contact_email = self.request.POST.get('contact_email', '')
                form_message = self.request.POST.get('message', '')

                # Add sender to AntiSpamTrainingData
                last_spam = antispammodels.AntiSpamTrainingData.objects.filter(
                    text=form_message).last()
                spam_dataset = antispammodels.AntiSpamTrainingData.objects.get(pk=last_spam.pk)
                if spam_dataset:
                    spam_dataset.sender = '{} ({})'.format(contact_name, contact_email)
                    spam_dataset.save()
                # Fake successful sending
                data = {'message': _('Message sent successfully!'), 'success': True}
                return JsonResponse(data, encoder=LazyEncoder)
            else:
                # ensure CSRF token is placed in, before using render_crispy_form
                ctx = {}
                ctx.update(csrf(self.request))
                # render AJAX response with django-crispy-forms
                html_form = render_crispy_form(form, context=ctx)
                data = {'message': _('Please correct the error below.'), 'html_form': html_form, 'success': False}
                return JsonResponse(data, encoder=LazyEncoder)
        else:
            return response

    def form_valid(self, form):
        response = super().form_valid(form)

        if self.request.is_ajax():
            form = ContactForm(self.request.POST)

            # Validate the form: the captcha field will automatically
            # check the input
            human = True
            contact_name = self.request.POST.get('contact_name', '')
            contact_email = self.request.POST.get('contact_email', '')
            form_message = self.request.POST.get('message', '')

            # Add sender to AntiSpamTrainingData
            last_spam = antispammodels.AntiSpamTrainingData.objects.filter(
                text=self.request.POST['message']).last()
            spam_dataset = antispammodels.AntiSpamTrainingData.objects.get(pk=last_spam.pk)
            if spam_dataset:
                spam_dataset.sender = '{} ({})'.format(contact_name, contact_email)
                spam_dataset.save()

            # 1) Send Email to Admin
            # Build Email structure with the contact information
            template_dict = getTemplateByName('Template contact')
            template = template_dict['template']
            if not template:
                template = get_template('home/contact-template.txt')
            mail_subject = template_dict['subject'].render()
            if not mail_subject:
                mail_subject = settings.DEFAULT_SUBJECT
            context = {
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_message': form_message,
            }
            content = template.render(context)
            email = EmailMessage(
                subject=mail_subject,
                body=content,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[settings.DEFAULT_TO_EMAIL]
            )
            email.send()

            # 2) Send Email response to sender
            # Build Email structure with the contact information
            template_dict = getTemplateByName('Template contact-response')
            template = template_dict['template']
            if not template:
                template = get_template('home/contact-response-template.txt')
            mail_subject = template_dict['subject'].render()
            if not mail_subject:
                mail_subject = settings.DEFAULT_SUBJECT
            context = {
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_message': form_message,
            }
            content = template.render(context)
            email = EmailMessage(
                subject=mail_subject,
                body=content,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[contact_email]
            )
            email.send()

            data = {'message': _('Message sent successfully!'), 'success': True}
            return JsonResponse(data, encoder=LazyEncoder)

        else:
            return response


@method_decorator(requires_csrf_token, name='dispatch')
class SearchListView(blogviews.ArticleCommonDataMixin, FilterView):
    filterset_class = ArticleFilter
    context_object_name = 'article_list'
    template_name = 'home/search-results.html'
    paginate_by = settings.PAGINATION_SEARCH_RESULTS
    paginator_class = blogviews.CustomPaginator

    def get_context_data(self, **kwargs):
        context = super(SearchListView, self).get_context_data(**kwargs)
        # add extra context
        searchword = self.request.GET.get('searchword', None),
        context["searchword"] = searchword[0]
        return context
