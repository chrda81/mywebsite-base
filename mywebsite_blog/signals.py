# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from mywebsite.utils.tools import unique_slug_generator
from mywebsite_blog.templatetags.blog_tags import escape_raw

from .models import Article, ArticleTag, Category


# subscribe to event pre_save of table 'Article' and 'Category', to create unique slug
@receiver(pre_save, sender=Article)
@receiver(pre_save, sender=Category)
def pre_save_project_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


# subscibe to event pre_save of table 'Article'
@receiver(pre_save, sender=Article)
def escape_article_tags(sender, instance=None, **kwargs):
    if not kwargs.get('raw', False) and instance.language_code and instance.tags:
        tags = json.loads(instance.tags)
        for idx, tagify_data in enumerate(tags):
            # escape value
            tags[idx]['value'] = escape_raw(tagify_data['value'])

        instance.tags = json.dumps(tags)


# subscibe to event pre_save of table 'Article'
@receiver(pre_save, sender=Article)
def delete_article_tags(sender, instance=None, created=False, **kwargs):
    if not kwargs.get('raw', False) and instance.language_code and instance:
        try:
            language_code = instance.get_current_language()
            old_tag_list = ArticleTag.objects.filter(
                article=instance, lang_code=language_code).values_list('name', flat=True)
            new_tag_list = instance.get_tag_list()
            for tag in old_tag_list:
                if str(tag) and tag not in new_tag_list.split(','):
                    tag_to_delete = ArticleTag.objects.filter(
                        article=instance,
                        name=tag,
                        lang_code=language_code)
                    if tag_to_delete:
                        tag_to_delete.delete()
        except Article.DoesNotExist:
            # No tag stuff to do here
            pass


# subscribe to event post_save of table 'Article'
@receiver(post_save, sender=Article, dispatch_uid="update_article_tags")
def create_article_tags(sender, instance=None, created=False, **kwargs):
    if not kwargs.get('raw', False) and instance.language_code and instance:
        language_code = instance.get_current_language()
        # First mode:
        # Store Article tags to DB
        # Split tagify's tags by delimiter set to ','
        tag_list = instance.get_tag_list()
        for tag in tag_list.split(','):
            # tag with given name of Article related ProspectiveCompany is not existent..
            if str(tag) and not ArticleTag.objects.filter(
                    article=instance,
                    name=tag,
                    lang_code=language_code):
                new_tag = ArticleTag()
                new_tag.name = tag
                new_tag.article = instance
                new_tag.tagify_data = instance.get_tag_data(tag)
                new_tag.lang_code = language_code
                new_tag.save()
