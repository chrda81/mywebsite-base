# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""
import json

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from hitcount.models import HitCount, HitCountMixin
from parler.models import TranslatableModel, TranslatedFields
from tinymce.models import HTMLField


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
class Category(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(
            max_length=140,
            verbose_name=_('name_field')
        )
    )
    slug = models.SlugField(
        max_length=191,
        editable=True,
        unique=True,
        db_index=True,
        default=None,
        blank=True,
        verbose_name=_('slug_field')
    )
    date = models.DateField(
        default=now,
        verbose_name=_('date_field')
    )
    is_internal = models.BooleanField(
        default=False,
        verbose_name=_('is_internal_field')
    )
    is_published = models.BooleanField(
        default=False,
        verbose_name=_('is_published_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'blog_category'
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __unicode__(self):
        if self.language_code:
            return self.name
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def __str__(self):
        if self.language_code:
            return self.name
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
class Article(TranslatableModel, HitCountMixin):
    translations = TranslatedFields(
        title=models.CharField(
            max_length=191,
            verbose_name=_('title_field')
        ),
        body=HTMLField(
            blank=True,
            verbose_name=_('body_field')
        ),
        teaser=HTMLField(
            default='',
            verbose_name=_('teaser_field')
        ),
        meta_keywords=models.CharField(
            max_length=191,
            blank=True,
            verbose_name=_('meta_keywords_field')
        ),
        tags=models.CharField(
            max_length=191,
            blank=True,
            verbose_name=_('tags_field')
        )
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name="articles",
        verbose_name=_('category_field')
    )
    slug = models.SlugField(
        max_length=191,
        editable=True,
        unique=True,
        db_index=True,
        default=None,
        blank=True,
        verbose_name=_('slug_field')
    )
    date = models.DateField(
        default=now,
        verbose_name=_('date_field')
    )
    author = models.CharField(
        max_length=80,
        blank=True,
        verbose_name=_('author_field')
    )
    hits = GenericRelation(
        HitCount,
        object_id_field='object_pk',
        related_query_name='hit_count_generic_relation',
        verbose_name=_('hits_field')
    )
    is_body_title_hidden = models.BooleanField(
        default=False,
        verbose_name=_('is_body_title_hidden_field')
    )
    is_internal = models.BooleanField(
        default=False,
        verbose_name=_('is_internal_field')
    )
    is_published = models.BooleanField(
        default=False,
        verbose_name=_('is_published_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )
    is_onnewsfeed = models.BooleanField(
        default=False,
        verbose_name=_('is_onnewsfeed_field')
    )

    class Meta:
        db_table = 'blog_article'
        ordering = ['-date']
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    def get_tag_list(self):
        # collect string from tagify data and remove possible commas, whitespaces
        # at the begin or end to jsonify the clean data
        if self.language_code:
            dirty_tags = self.tags.strip(' ,')
            cleaned_tags = ''
            if str(dirty_tags):
                cleaned_tags_json = json.loads(dirty_tags)
                for tagify_data in cleaned_tags_json:
                    value = tagify_data.get('value', None)
                    # append tagify value to string list
                    cleaned_tags = '{},{}'.format(cleaned_tags, value)
                return cleaned_tags.strip(' ,')
        # nothing found
        return ''

    def get_tag_data(self, content):
        # collect string from tagify data and remove possible commas, whitespaces
        # at the begin or end to jsonify the clean data
        if self.language_code:
            dirty_tags = self.tags.strip(' ,')
            if str(dirty_tags):
                cleaned_tags_json = json.loads(dirty_tags)
                for tagify_data in cleaned_tags_json:
                    # find content in json values
                    if tagify_data['value'] == content:
                        # build json like string without encoding to utf-8, which tagify understands
                        json_str = str(tagify_data).replace("'", '"')
                        json_str = "[{}]".format(json_str)
                        return json_str
        # nothing found
        return ''

    def __unicode__(self):
        if self.language_code:
            return self.title
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def __str__(self):
        if self.language_code:
            return self.title
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def get_absolute_url(self):
        return reverse('article', kwargs={'slug': self.slug, 'pk': self.pk})


class ArticleTag(models.Model):
    name = models.CharField(
        max_length=191,
        verbose_name=_('name_field')
    )
    article = models.ForeignKey(
        Article,
        related_name='tags_rel',
        on_delete=models.CASCADE,
        verbose_name=_('article_field')
    )
    tagify_data = models.CharField(
        max_length=191,
        verbose_name=_('tagify_data_field'),
    )
    lang_code = models.CharField(
        max_length=12,
        verbose_name=_('lang_code_field')
    )

    class Meta:
        db_table = 'blog_article_tag'
        verbose_name = _('Article Tag')
        verbose_name_plural = _('Article Tags')
        ordering = ['name']
        unique_together = ('name', 'article', 'lang_code')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
