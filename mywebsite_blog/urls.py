# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.urls import include, path

from . import views

app_name = 'blog'

urlpatterns = [
    # Categories
    path('category', views.CategoryListView.as_view(), name='category'),
    path('category/<slug:category>', views.ArticleListView.as_view(), name='article-category-list'),

    # Tags
    path('tags/get-list-flat', views.get_tags_list_flat_jx, name='get-tags-list-flat'),
    path('tags/<tagname>', views.ArticleListView.as_view(), name='article-tag-list'),

    # Articles
    path('', views.ArticleListView.as_view(), name='blog'),
    path('<slug:slug>', views.ArticleDetailView.as_view(), name='article-detail'),
    path('<slug:slug>/compare', views.ArticleHistoryCompareDetailView.as_view(), name='compare-article'),
]
