# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import forms
from django.utils.translation import gettext_lazy as _
from parler.forms import TranslatableModelForm

from mywebsite.utils.widgets import TinyMCEWidget

from . import models
from .templatetags.blog_tags import unescape_raw


class ArticleAdminForm(TranslatableModelForm):
    body = forms.CharField(widget=TinyMCEWidget(
        attrs={'cols': 30, 'rows': 10}
    ),
        required=False
    )
    teaser = forms.CharField(widget=TinyMCEWidget(
        attrs={'cols': 30, 'rows': 10}
    ))
    tags = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Add tags separated by comma')}
    ),
        required=False
    )

    class Meta:
        model = models.Article
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(ArticleAdminForm, self).__init__(*args, **kwargs)
        self.fields['body'].label = _('body_field')
        self.fields['teaser'].label = _('teaser_field')
        self.fields['tags'].label = _('tags_field')
        # unescape 'tags'
        if self.instance.id and self.instance.tags:
            self.initial['tags'] = unescape_raw(self.instance.tags)
