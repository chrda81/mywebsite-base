# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json
import math

from django import forms, template
from django.utils.http import urlquote_plus, urlunquote_plus

# Django template custom math filters
register = template.Library()


@register.simple_tag
def query(qs, **kwargs):
    """ template tag which allows queryset filtering. Usage:
          {% query books author=author as mybooks %}
          {% for book in mybooks %}
            ...
          {% endfor %}
    """
    return qs.filter(**kwargs)


# sum two integers
@register.simple_tag
def add(a, b):
    return a + b


# calculating minutes of the article reading time
@register.filter
def minutes(value):
    return math.floor(value / 200)


# calculating seconds of the article reading time
@register.filter
def seconds(value):
    return math.floor(value % 200 / (200 / 60))


@register.filter
def escape_raw(value):
    return urlquote_plus(value)


@register.filter
def unescape_raw(value):
    if type(value) is forms.boundfield.BoundField:
        if value.initial:
            labels = json.loads(value.initial)
            for idx, tagify_data in enumerate(labels):
                # unescape value
                labels[idx]['value'] = unescape_raw(tagify_data['value'])

            value.initial = json.dumps(labels)
        return value
    # elif '{"value":' in value:
    #     labels = json.loads(value)
    #     for idx, tagify_data in enumerate(labels):
    #         # unescape value
    #         labels[idx]['value'] = unescape_raw(tagify_data['value'])

    #     return json.dumps(labels)
    elif type(value) is str:
        return urlunquote_plus(value)
    else:
        return value
