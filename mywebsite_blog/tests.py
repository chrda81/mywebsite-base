# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.test import TestCase
from django.urls import resolve, reverse

from mywebsite_blog.models import Article, Category


class URLTests(TestCase):
    def setUp(self):
        self.category = Category(
            name="Test category",
            slug="test-category"
        )
        self.category.save()
        self.article = Article(
            title="Test article",
            slug="test-article",
            category=self.category,
            teaser="This is the teaser of the test article.",
            body="",
        )
        self.article.save()
        super(URLTests, self).setUp()

    def assert_link_response(self, named_url, expected_url, args=None):
        if args:
            reverse_url = reverse(named_url, kwargs=args)
        else:
            reverse_url = reverse(named_url)
        # reverse_url matches expected_url string
        self.assertEqual(reverse_url, expected_url)
        # resolved view name matches
        self.assertEqual(resolve(expected_url).view_name, named_url)
        # Check that the response is 200 OK.
        response = self.client.get(reverse_url)
        self.assertEqual(response.status_code, 200)

    def test_blog(self):
        self.assert_link_response("blog:blog", "/blog/")

    def test_category(self):
        self.assert_link_response("blog:category", "/blog/category")

    def test_article_detail(self):
        self.assert_link_response("blog:article-detail", "/blog/{}".format(self.article.slug),
                                  {'slug': self.article.slug})

    def test_article_category_list(self):
        self.assert_link_response("blog:article-category-list", "/blog/category/{}".format(self.category.slug),
                                  {'category': self.category.slug})

    def test_compare_article(self):
        self.assert_link_response("blog:compare-article", "/blog/{}/compare".format(self.article.slug),
                                  {'slug': self.article.slug})
