# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib import admin

from parler.admin import TranslatableAdmin
from reversion_compare.admin import CompareVersionAdmin
from mywebsite.utils.mixins import TranslateableReversionAdminMixin

from .forms import ArticleAdminForm
from .models import Article, ArticleTag, Category


@admin.register(Category)
class CategoryAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'date', ('is_internal', 'is_published', 'is_archived')),
        }),
    )
    list_display = ('name', 'date', 'is_internal', 'is_published', 'is_archived')
    list_editable = ('is_internal', 'is_published', 'is_archived')


@admin.register(Article)
class ArticleAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    form = ArticleAdminForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'author', 'category', 'tags', 'meta_keywords', 'date',
                       ('is_internal', 'is_published', 'is_archived', 'is_onnewsfeed', 'is_body_title_hidden'),
                       'teaser', 'body'),
        }),
    )
    list_display = ('title', 'category', 'date', 'author', 'is_internal',
                    'is_onnewsfeed', 'is_published', 'is_archived')
    list_editable = ('is_internal', 'is_onnewsfeed', 'is_published', 'is_archived')
    search_fields = ['translations__title', 'translations__teaser', 'translations__body', 'tags_rel__name']
    list_filter = ('is_onnewsfeed', 'category')

    # we can change the CSS attributes here, but we use /static/mywebsite/css/admin-extra.css instead
    # def get_form(self, request, obj=None, **kwargs):
    #     form = super(ArticleAdmin, self).get_form(request, obj, **kwargs)
    #     form.base_fields['meta_keywords'].widget.attrs['style'] = 'width: 60%;'
    #     return form

    def save_model(self, request, obj, form, change):
        if not obj.author:
            username = request.user.username
            # remap administrator name to 'admin' as small security optimazation, because it is a risk to show
            # real admin username at articles and news sites)
            if 'admin' in username.lower():
                username = 'admin'
            obj.author = username
        super().save_model(request, obj, form, change)


@admin.register(ArticleTag)
class ArticleTagAdmin(CompareVersionAdmin):
    list_display = ('name', 'article', 'lang_code')
