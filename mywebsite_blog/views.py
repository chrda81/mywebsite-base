# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import operator
from datetime import datetime

from django.conf import settings
from django.core.paginator import EmptyPage, Paginator
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import http, translation
from django.utils.translation import get_language
from django.utils.translation import gettext as _
from django.views import generic

from hitcount.views import HitCountDetailView
from reversion_compare.views import HistoryCompareDetailView

from mywebsite.utils.tools import get_default_language, tinymce_word_count
from mywebsite_blog.templatetags.blog_tags import unescape_raw
from mywebsite_home.templatetags.core_settings_tags import getCoreSettingItemsFor
from mywebsite_home.templatetags.websettings_tags import handleWebSettingsType

from .models import Article, ArticleTag, Category


# Commond data & querys used to view/create/edit articles
def common_article_data(extra_context=None):
    # Static data
    lang_code = translation.get_language()
    # Querys
    article_tags = ArticleTag.objects.filter(lang_code=lang_code,
                                             article__is_published=True,
                                             article__is_archived=False).order_by(
        'name').distinct().values_list('name', flat=True)

    # set context
    context = {'lang_code': lang_code, 'article_tags': article_tags}

    # Extra context - merge context dict and extra_context dict to context dict
    if extra_context:
        context = {**extra_context, **context}

    return context


def getSlugByMenuItemName(link_name=None):
    # Collect WebSettings for this menu link
    result_list = getCoreSettingItemsFor(settingstype=_('Menu Link'), name='Menu Items')

    if link_name:
        # Get exact menu item by containing string
        slug_itmes = [item['slug'] for item in result_list if link_name in item['name']]
    else:
        # Get all menu items
        slug_itmes = [{'name': item['name'], 'slug': handleWebSettingsType(item['slug'])} for item in result_list]

    data = []
    if slug_itmes:
        data = slug_itmes

    return data


def getArticleByMenuItemName(link_name):
    # Get exact slug by containing string
    slug_items = getSlugByMenuItemName(link_name)

    data = None
    if slug_items:
        # Get first object from list. Btw., we should always have only one object when using link_name!
        slug = slug_items[0]

        # We are using the blog app instead of a fixed template
        # data = Article.objects.active_translations().distinct().get(
        #     slug__iexact=handleWebSettingsType(slug), is_internal=True, is_archived=False)
        # TODO: django-parler has a bug here! We have manually retrieve the fallback language not to run into error 500
        current_lang = get_language()
        fallback_lang = get_default_language()
        try:
            data = Article.objects.translated(current_lang).get(
                slug__iexact=handleWebSettingsType(slug), is_internal=True, is_archived=False)
        except Article.DoesNotExist:
            data = Article.objects.translated(fallback_lang).get(
                slug__iexact=handleWebSettingsType(slug), is_internal=True, is_archived=False)
    return data


class CustomPaginator(Paginator):
    def validate_number(self, number):
        try:
            return super().validate_number(number)
        except EmptyPage:
            if int(number) > 1:
                # return the last page
                return self.num_pages
            elif int(number) < 1:
                # return the first page
                return 1
            else:
                raise


class ArticleCommonDataMixin(object):
    def get_context_data(self, **kwargs):
        context = super(ArticleCommonDataMixin, self).get_context_data(**kwargs)
        # add common_article_data to context
        for key, value in common_article_data().items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context


class ArticleListView(ArticleCommonDataMixin, generic.ListView):
    model = Article
    context_object_name = 'article_list'
    template_name = 'blog/article_list.html'
    paginate_by = settings.PAGINATION_ARTICLES
    paginator_class = CustomPaginator

    def render_to_response(self, context, **response_kwargs):
        response = super(ArticleListView, self).render_to_response(context, **response_kwargs)
        # set cookie for FORCE_SCRIPT_NAME setting, because this is used for TinyMCE CustomFileBrowser's callback
        response.set_cookie("force_script_name", settings.FORCE_SCRIPT_NAME)
        return response

    def get_queryset(self):
        queryset = super(ArticleListView, self).get_queryset()
        # standard filter
        queryset = queryset.active_translations().distinct().filter(is_published=True,
                                                                    is_archived=False,
                                                                    date__lte=datetime.now()).order_by('-date')
        # additional filter through url request
        if self.kwargs and 'category' in self.kwargs:
            # request was 'article-category-list'
            category_filter = self.kwargs['category']
            if category_filter:
                queryset = queryset.filter(category__slug=category_filter)

        if self.kwargs and 'tagname' in self.kwargs:
            # request was 'article-tag-list'
            tag_filter = self.kwargs['tagname']
            if tag_filter:
                queryset = queryset.filter(tags_rel__name=tag_filter)
        return queryset


class ArticleDetailView(ArticleCommonDataMixin, HitCountDetailView):
    context_object_name = 'article'
    template_name = 'blog/article.html'
    model = Article
    count_hit = True

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)

        # calculate article read time
        if self.object.body:
            context['body_word_count'] = tinymce_word_count(self.object.body)
        else:
            context['body_word_count'] = 0
        if self.object.teaser:
            context['teaser_word_count'] = tinymce_word_count(self.object.teaser)
        else:
            context['teaser_word_count'] = 0
        return context


class CategoryListView(ArticleCommonDataMixin, generic.ListView):
    model = Category
    context_object_name = 'category_list'
    template_name = 'blog/category_list.html'
    paginate_by = settings.PAGINATION_CATEGORIES
    paginator_class = CustomPaginator

    def get_queryset(self):
        queryset = super(CategoryListView, self).get_queryset()
        queryset = queryset.active_translations().distinct().filter(is_internal=False, is_published=True,
                                                                    is_archived=False, date__lte=datetime.now())
        # .order_by('translations__name') doesn't work, because it multiplies entries by using .active_translations()
        queryset = sorted(queryset, key=operator.attrgetter('name'))
        return queryset


class ArticleHistoryCompareDetailView(ArticleCommonDataMixin, HistoryCompareDetailView):
    model = Article
    template_name = 'blog/article_compare.html'


class AjaxArticleDetailView(ArticleCommonDataMixin, generic.TemplateView):
    link_name = None
    template_name = 'blog/article_ajax.html'

    def get_context_data(self, **kwargs):
        context = super(AjaxArticleDetailView, self).get_context_data(**kwargs)
        context = {**{'article': getArticleByMenuItemName(self.link_name)}, **context}
        return context


def get_tags_list_flat_jx(request):
    lang_code = request.POST.get('language', get_default_language())
    data = list()
    for item in ArticleTag.objects.filter(lang_code=lang_code).order_by('name').distinct().values_list('name', flat=True):
        data.append(unescape_raw(item))

    return JsonResponse(data, safe=False)
