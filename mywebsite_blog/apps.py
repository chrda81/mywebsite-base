# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class BlogConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite_blog``."""
    name = 'mywebsite_blog'
    verbose_name = _("Website Blog")

    def ready(self):
        """Wire up the signals """
        import mywebsite_blog.signals
