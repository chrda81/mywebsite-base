# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import forms
from django.utils.safestring import mark_safe


class HoneypotInput(forms.TextInput):
    """
    Default honeypot field widget.
    Display text input in hidden div.
    """

    @property
    def is_hidden(self):
        return True

    def render(self, *args, **kwargs):
        """
        Returns this widget rendered as HTML.
        """
        return mark_safe(
            '<div style="display: none;">%s</div>' % str(
                super(HoneypotInput, self).render(*args, **kwargs))
        )
