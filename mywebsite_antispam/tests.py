# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from datetime import datetime

from django.conf import settings
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils.translation import gettext_lazy as _

from mywebsite.utils.tools import make_tz_aware
from mywebsite_antispam.honeypot.forms import HoneypotField
from mywebsite_antispam.models import AntiSpamTrainingData
from mywebsite_antispam.spamdetector.forms import SpamDetectorField


class SpamDetectorFieldTests(TestCase):
    def setUp(self):
        self.spam_dataset = AntiSpamTrainingData(
            timestamp=make_tz_aware(dt=datetime.now()),
            sender='me@self.com',
            text="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor \
            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et \
            justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem \
            ipsum dolor sit amet.",
            is_spam=True
        )
        self.spam_dataset.save()
        super(SpamDetectorFieldTests, self).setUp()

    def test_spam_validation_true(self):
        if not settings.SPAM_FILTER_ACTIVE:
            return

        self.field = SpamDetectorField(required=True, max_length=2048)
        self.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor \
            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et \
            justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem \
            ipsum dolor sit amet."
        try:
            self.field.validate(self.text)
            # our test should raise the execption, so we never should ran into this
            self.assertTrue(False)
        except ValidationError as ex:
            self.assertEqual(ex.message, _('Invalid value for spam dectector field.'))
            self.assertEqual(ex.code, 'spam-protection')

    def test_spam_validation_false(self):
        if not settings.SPAM_FILTER_ACTIVE:
            return

        self.field = SpamDetectorField(required=True, max_length=2048)
        # mark text as no spam
        self.spam_dataset.is_spam = False
        self.spam_dataset.save()
        self.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor \
            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et \
            justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem \
            ipsum dolor sit amet."
        try:
            self.field.validate(self.text)
            self.assertTrue(True)
        except ValidationError as ex:
            # our test shouldn't raise the execption, so we never should ran into this
            self.assertNotEqual(ex.message, _('Invalid value for spam dectector field.'))
            self.assertNotEqual(ex.code, 'spam-protection')

    def test_honeypot_validation_true(self):
        self.field = HoneypotField()
        try:
            self.field.validate('Some input data was given.')
            # our test should raise the execption, so we never should ran into this
            self.assertTrue(False)
        except ValidationError as ex:
            self.assertEqual(ex.message, _('Invalid value for honey pot field.'))
            self.assertEqual(ex.code, 'spam-protection')

    def test_honeypot_validation_false(self):
        self.field = HoneypotField()
        try:
            self.field.validate('')
            self.assertTrue(True)
        except ValidationError as ex:
            # our test should raise the execption, so we never should ran into this
            self.assertEqual(ex.message, _('Invalid value for honey pot field.'))
            self.assertEqual(ex.code, 'spam-protection')
