# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib import admin

from mywebsite.utils.mixins import ExportCsvMixin
from reversion_compare.admin import CompareVersionAdmin

from .models import AntiSpamTrainingData


@admin.register(AntiSpamTrainingData)
class AntiSpamTrainingDataAdmin(CompareVersionAdmin, ExportCsvMixin):
    list_display = ('timestamp', 'sender', 'text', 'is_spam')
    search_fields = ['text']
    list_editable = ('is_spam',)
    list_filter = ('is_spam',)
    actions = ["export_as_csv"]
