# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.conf import settings
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC

global Classifier
global Vectorizer
global DataFrame
global SGDClassesCount


class AntiSpamTrainingData(models.Model):
    timestamp = models.DateTimeField(
        default=now,
        verbose_name=_('Date (with time)')
    )
    sender = models.TextField(
        blank=True,
        null=True,
        verbose_name=_('sender_field')
    )
    text = models.TextField(
        verbose_name=_('text_field')
    )
    is_spam = models.BooleanField(
        default=False,
        verbose_name=_('is_spam_field')
    )

    class Meta:
        db_table = 'antispam_antispamtrainingdata'
        verbose_name = _('AntiSpam training data')
        verbose_name_plural = _('AntiSpam training datas')

    def __unicode__(self):
        return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def __str__(self):
        return '{} (ID: {})'.format(self.__class__.__name__, self.pk)


def extract_features(text):
    """
    Extract features from a given text.

    :param text: Text to extract features for.
    :return:     A vector where:
                    - The 0th element is 1 if there is a "!" inside the text (0 otherwise).
                    - The 1th element is 1 if there is a "$" inside the text (0 otherwise).
                    - The 2nd element is the ratio of uppercase characters with respect to the sum of all uppercase and
                      lowercase characters.
    """
    features = np.zeros((3,))
    if "!" in text:
        features[0] = 1
    if "$" in text:
        features[1] = 1
    # A list consisting of lowercase characters
    lowercase = list('abcdefghijklmnopqrstuvwxyz')
    # A list consisting of uppercase characters
    uppercase = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    # Set the counts of lowercase and uppercase characters to 0
    num_lowercase = 0
    num_uppercase = 0
    # And count the lowercase and uppercase characters
    for char in text:
        if char in lowercase:
            num_lowercase += 1
        elif char in uppercase:
            num_uppercase += 1
    # Define the third feature as the ratio of uppercase characters
    features[2] = num_uppercase / (num_lowercase + num_uppercase)
    return features


def train_model():
    global Classifier
    global Vectorizer
    global DataFrame
    global SGDClassesCount

    qs = AntiSpamTrainingData.objects.all()
    q = qs.values('text', 'is_spam')
    DataFrame = pd.DataFrame.from_records(q)
    DataFrame.drop_duplicates(inplace=True)

    xtrain = []
    xtest = []
    ytrain = []
    ytest = []

    if settings.SPAM_CLASSIFIER == 'SGD':
        # Train model with SGD classifier
        training_data = [query for query in DataFrame['text'].values]
        training_features = np.vstack([extract_features(training_data[i]) for i in range(len(training_data))])
        SGDClassesCount = np.unique(DataFrame['is_spam'].values)
        if len(SGDClassesCount) > 1:
            # Make an Stochastic Gradient Descent classifier
            Classifier = SGDClassifier(max_iter=100, tol=1e-3)
            # Split training set data in 80% / 20%
            xtrain, xtest, ytrain, ytest = train_test_split(
                training_features, DataFrame['is_spam'], test_size=0.20, random_state=0)

            # And fit classifier to the training set
            Classifier.fit(xtrain, ytrain)

            # evaluate the model
            predict = Classifier.predict(xtrain)
            score = accuracy_score(ytrain, predict)
        else:
            score = 0
    else:
        # Train model with OneVsRest classifier
        Classifier = OneVsRestClassifier(SVC(kernel='linear', probability=True))
        Vectorizer = TfidfVectorizer()
        vectorized_text = Vectorizer.fit_transform(DataFrame['text'])
        # Split training set data in 80% / 20%
        xtrain, xtest, ytrain, ytest = train_test_split(
            vectorized_text, DataFrame['is_spam'], test_size=0.20, random_state=0)

        # And fit classifier to the training set
        Classifier.fit(xtrain, ytrain)

        # evaluate the model
        predict = Classifier.predict(xtrain)
        score = accuracy_score(ytrain, predict)

    return score


def spam_prediction(text):
    global Classifier
    global Vectorizer
    global DataFrame
    global SGDClassesCount

    predict = False

    if settings.SPAM_CLASSIFIER == 'SGD':
        if len(SGDClassesCount) > 1:
            # Predict the labels of the text
            features = extract_features(text)
            predict = Classifier.predict([features])[0]
        else:
            # If there is only one training class which is marked as spam, then try simple method,
            # because math algorithm isn't applicable
            if len(DataFrame['is_spam'].values) > 0 and DataFrame['is_spam'].values.item(0) is True:
                for data in DataFrame['text'].values:
                    if (data in text) or (text in data):
                        predict = True
                        break
    else:
        vectorize_message = Vectorizer.transform([text])
        predict = Classifier.predict(vectorize_message)[0]

    return predict
