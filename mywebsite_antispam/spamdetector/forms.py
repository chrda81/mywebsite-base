# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from datetime import datetime

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.utils.translation import gettext_lazy as _

from mywebsite.utils.tools import make_tz_aware

from ..models import AntiSpamTrainingData, spam_prediction, train_model


class SpamDetectorField(forms.CharField):
    """
    Spam detector form field.
    """
    default_error_messages = {
        'invalid': _('Enter some data.'),
        'spamdetector': _('Invalid value for spam dectector field.'),
    }

    def __init__(self, **kwargs):
        kwargs.setdefault('required', False)
        kwargs.setdefault('max_length', 255)

        super(SpamDetectorField, self).__init__(**kwargs)

    def validate(self, value):
        """
        Validates form field value entered by user.
        :param value: user-input
        :raise: ValidationError with code="spam-protection"
                if spam detector check failed.
        """
        super(SpamDetectorField, self).validate(value)

        if value and settings.SPAM_FILTER_ACTIVE:
            # Make an array of features where the ith row corresponds to the ith documents and the columns correspond to
            # the features
            train_model()

            # Predicting if value is spam or not
            spamflag = spam_prediction(value)

            # Add as training dataset to database
            spam_dataset = AntiSpamTrainingData()
            spam_dataset.timestamp = make_tz_aware(dt=datetime.now()) if settings.USE_TZ else datetime.now()
            spam_dataset.text = value
            spam_dataset.is_spam = spamflag
            spam_dataset.save()

            if spamflag:
                raise ValidationError(self.error_messages['spamdetector'],
                                      code='spam-protection')

        return True
