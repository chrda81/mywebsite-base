# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import OperationalError, ProgrammingError
from django.utils.translation import gettext_lazy as _


class AntispamConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite_antispam``."""
    name = 'mywebsite_antispam'
    verbose_name = _("Antispam")

    def ready(self):
        """Import module settings"""
        try:
            import mywebsite_antispam.settings
        except (ObjectDoesNotExist, OperationalError, ProgrammingError):
            pass
