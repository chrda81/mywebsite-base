# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json

from django.conf import settings as settings_file

from mywebsite.utils.tools import get_object_or_none
from mywebsite_home.models import Settings

if not hasattr(settings_file, 'SPAM_FILTER_ACTIVE'):
    app_settings = get_object_or_none(Settings, name='Base Settings', is_archived=False)

    # set default
    settings_file.SPAM_FILTER_ACTIVE = True

    if app_settings and app_settings.json:
        json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

        if 'SPAM Filter active' in json_dict:
            settings_file.SPAM_FILTER_ACTIVE = json_dict['SPAM Filter active']

if not hasattr(settings_file, 'SPAM_CLASSIFIER'):
    app_settings = get_object_or_none(Settings, name='Base Settings', is_archived=False)

    # set default
    settings_file.SPAM_CLASSIFIER = 'SGD'

    if app_settings and app_settings.json:
        json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

        if 'SPAM Classifier' in json_dict:
            settings_file.SPAM_CLASSIFIER = json_dict['SPAM Classifier']
