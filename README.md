# MyWebsite-base

This is the base package of the modular WEB application for my business It is written in Python/Django.

---

License agreement:

Copyright (C) 2018 dsoft-app-dev.de and friends.

This Program may be used by anyone in accordance with the terms of the
German Free Software License

The License may be obtained under http://www.d-fsl.org.

---

If you like my work, I would appreciate a donation. You can find several options on my website www.dsoft-app-dev.de at section crowdfunding. Thank you!

This application depends on some Python modules, which should be installed into the virtual environment the WEB application is running from within. For the development system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/development.txt

For the production system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/production.txt

Create your own copy of settings_secure.py and tweak the settings:

    $ cp mywebsite/settings/settings_secure.example.py mywebsite/settings/settings_secure.py

To generate a new SECRET_KEY use:

    $ function django_secret() { python -c "import random,string;print(''.join([random.SystemRandom().choice(\"{}{}{}\".format(string.ascii_letters, string.digits, string.punctuation)) for i in range(63)]).replace('\\'','\\'\"\\'\"\\''))"; }
    $ echo "DJANGO_SECRET_KEY='$(django_secret)'"

To create the database structure and group permissions use:

    $ ./manage.py migrate
    $ ./manage.py create_editors_permissions

Now collect all static files. But before running the command, you have to collect all node_modules from third party vendors:

    $ yarn

For the production system use:

    $ yarn --production

Then you can run the command:

    $ ./manage.py collectstatic

To import sample data, use:

    $ ./manage.py loaddata fixtures/sample_data.json

To dump the whole database use:

    $ ./manage.py dumpdata --indent 2 > fixtures/dump.json

To dump a single database table use:

    $ ./manage.py dumpdata --indent 2 mywebsite_home.websitesettings > fixtures/dump_websettings.json

It is possible to exclude unneccessary tables during the database dump, e.g.:

    $ ./manage.py dumpdata --natural-foreign --natural-primary --indent 2 -e filebrowser -e reversion -e hitcount -e sessions -e admin -e captcha -e contenttypes -e auth -e dashboard  > fixtures/sample_data.json

The app 'extendcmds' allows to create a superuser with a given password:

    $ ./manage.py createsuperuser --username <account name> --email <email> --password <secret password> --preserve --noinput

## Settings for mywebsite_home app

The mywebsite_home app represents the website with its index page. In order to get the correct pages for menu entries 'Crowdfunding', 'About', 'Disclosures' and 'Data privacy', you have to create 4 article pages in the mywebsite_blog app and name the slug field with the names 'crowdfunding', 'about-me', 'disclosures' and 'data-privacy'.

## Unit tests

Unit tests for Javascript is done by using QUnit, see https://qunitjs.com
To execute tests, you have 2 possibilities:

1. Run "python -m http.server" instead of "./manage.py runserver" from command line and browse http://localhost:8000/test/tests.html

or

2. Install QUnit (npm install -g qunit) and simply run it from command line "qunit"

All other unit tests can be run with

    $ tox

To test the cache scenario, you have to install "memcached" and run it with:

    $ memcached -vv

## More MyWebsite modules

-   [mywebsite_celery](https://bitbucket.org/chrda81/mywebsite-celery)
-   [mywebsite_members](https://bitbucket.org/chrda81/mywebsite-members)
-   [mywebsite_project](https://bitbucket.org/chrda81/mywebsite-project)

To be continued ...

## Build package

To build package via setup.py use:

    $ python setup.py sdist --formats=zip --dist-dir .tox/dist

To install this built package locally in your Python virtual environment, use e.g.:

    $ pip install --exists-action w .tox/dist/mywebsite_base_django-1.0.9.zip

## GIT pre-commit example

To change the build no. automatically, create a file '.git/pre-commit' with the following content:

    #!/bin/bash

    # extract version and write it to VERSION file before commit
    VERSION_NO=`cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print(obj['version']);"`

    echo $VERSION_NO > ./VERSION
    cp ./mywebsite/__init__.py ./mywebsite/__init__.py.bak
    sed "/__version__/s/.*/__version__ = '$VERSION_NO'/" ./mywebsite/__init__.py.bak > ./mywebsite/__init__.py
    rm ./mywebsite/__init__.py.bak

## VSCode custom configuration

Custum configuration files are stored in the .vscode folder under the project path. The file _settings.json_ contains
full path settings to programs, e.g. python, pep8 and so forth. It is recommended to exclude this file in git, using
_.gitignore_. Here is a template for _settings.json_:

    {
        "python.pythonPath": "<path to bin/python>",
        "python.linting.pep8Path": "<path to bin/pep8>",
        "python.linting.pep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pep8Enabled": true,
        "python.formatting.autopep8Path": "<path to bin/autopep8>",
        "python.formatting.autopep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pylintPath": "<path bin/pylint>",
        "python.linting.pylintArgs": [
            "--errors-only",
            "--load-plugins",
            "pylint_django"
        ],
        "python.linting.pylintEnabled": true,
        "html.format.enable": false,
        "eslint.enable": false,
        "editor.formatOnSave": true,
        "editor.rulers": [
            119,
            140
        ],
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true,
            ".vscode": false,
            ".idea": true,
            "**/*,pyc": true,
            "*/__pycache__": true
        },
        "workbench.editor.enablePreview": false,
        "editor.find.globalFindClipboard": true,
        "search.globalFindClipboard": true,
        "editor.minimap.side": "left",
        "git.promptToSaveFilesBeforeCommit": true,
        "markdown-pdf.type": [
            "html",
        ],
        "markdown-pdf.convertOnSave": true,
        "markdown-pdf.outputDirectory": "mywebsite_home/templates/admin_doc",
        "markdown-pdf.convertOnSaveExclude": [
            "^(?!MANUAL.*\\.md$)",
        ],
        "markdown-pdf.includeDefaultStyles": false,
        "markdown-pdf.styles": [
            "mywebsite/static/mywebsite/assets_bundles/appStyle.css",
        ],
    }

## Commit message convention

We follow the [conventional commits specification](https://www.conventionalcommits.org/en) for our commit messages:

- `style`: style changes, e.g. add css code to a modules stylesheet section.
- `fix`: bug fixes, e.g. fix crash due to deprecated method.
- `feat`: new features, e.g. add new method to the module.
- `refactor`: code refactor, e.g. migrate from class components to hooks.
- `docs`: changes to documentation, e.g. add usage example for the module.
- `test`: adding or updating tests, eg add integration tests using detox.
- `chore`: tooling changes, e.g. change CI config.

You can also add a scope to your message, e.g.:

- `chore(deps)`: Update package @react-navigation/drawer to version 6.3.0
- `fix(navigation)`: Rework linking configuration
- `style(global)`: Rework responsive styles

# Change history

    1.3.0:

-   chore(deps): Update webpack dependencies
-   refactor(webpack): Compress static files and auto inject build version
-   refactor(test): Switch to docker environment with gunicorn

    1.2.3:

-   chore(deps): Update filebrowser to 4.0.1 and tinymce to 5.10.1
-   fix(lang): Add parler tab for sub language de_CH
-   fix(lang): Use PARLER_DEFAULT_LANGUAGE_CODE as default for get_language_from_request(), get_language()
-   feat(lang): Created get_default_language()
-   feat(admin): Add admin option "Reload Configuration"

    1.2.2:

-   feat(admin): Add permissions for viewing specific language tabs in admin
-   fix(settings): Allow local urls embedded in iframes
-   fix(styles): Bugfix for permission select boxes

    1.2.1:

-   feat(deps): Upgrade to django version 3.0.14
-   fix(deps): Handle RemovedInDjango40Warning and replace ugettext_lazy and ugettext
-   fix(deps): Replace django.utils.six with six
-   fix(deps): Replace admin_static with static in templates
-   feat(tools): Add class OrderedStack
-   fix(views): Add CustomPaginator to get correct page by request param

    1.2.0:

-   feat(deps): Upgrade django to version 2.2.24
-   feat(deps): Upgrade all other python packages to latest possible version
-   fix(): Make modifications to handle upgraded deps

    1.1.23:

-   feat(deps): Add package jquery-floating-social-share
-   feat(): Replace facebook-api.html with floating social buttons
-   feat(views): Add date filter for future news and articles
-   fix(views): Add future time filter on CategoryListView

    1.1.22:

-   feat(js): Upgrade node packages ace-builds and jsoneditor
-   feat(js): Add readonly_fields to JSONEditorWidget

    1.1.21:

-   fix(hitcount): Add monkey patch for ip based hitcount
-   fix(): Bugfix for filebrowser filelisting.html

    1.1.20:

-   fix(debug): Remove nothreading option from debug launch config
-   fix(): Bugfix for inlines in TranslateableReversionAdminMixin

    1.1.19:

-   fix(admin): Reworked handling of user permissions for django-jet
-   fix(): Upgrade package django-good-otp for using correct user permissions

    1.1.18:

-   Upgraded package django-jet to version 1.0.8.
-   Added bugfix for Django-Jet to suppress Select2 on ManyToMany fields, if filter_horizontal or filter_vertical is set in ModelAdmin.
-   Bugfix for initial missing Settings table in mywebsite_antispam.settings.
-   Added SPAM_FILTER_ACTIVE to mywebsite_antispam.settings.

    1.1.17:

-   Extend function utils.tools.unique_slug_generator()
-   Upgraded package django-parler to version 2.1.

    1.1.16:

-   Bugfix for SpamDetector field.
-   Rebuild spam detection algorhythm.
-   Export to CSV for Admin AntiSpam TrainingsData.
-   Added function make_tz_aware() to tools.py.
-   Fixed unit tests.

    1.1.15:

-   Updated packages django-storages and django-dbbackup.
-   Removed monkey patch for django-storages.
-   Handle attachments in core_settings_tags.getTemplateByName()


    1.1.14:

-   Removed template for member_login. This is now part of mywebsite-members-django.
-   Added custom monkey-patch function for \_object_version for django_reversion.


    1.1.13:

-   Truncate string of column version_link for RevisionAuditAdmin.
-   Changed type of model field 'object_repr' to TextField.

    1.1.12:

-   Added new home model RevisionAudit.
-   Changed reversion patch to use RevisionAudit.

    1.1.11:

-   Added package django-admin-rangefilter.
-   Updated packages django-reversion and django-reversion-compare and added JSONEditorWidget to VersionAdmin.
-   Fixed behavior of django-reversion for ArticleTags.
-   Added new column 'Action' to VersionAdmin.

    1.1.10:

-   Replaced package django-jsonfield with jsonfield.
-   Modified template article.html, to hide read times for internal articles.
-   Upgraded node packages.

    1.1.9:

-   Added drop_all.sql.
-   Fixed working directory for cronjobs.

    1.1.8:

-   Added new template tags 'do_include_maybe' and 'template_exists'.

    1.1.7:

-   Fixed bugs for migrations and installing fixtures after implementing features in v1.1.6.
-   Added maintenance mode.
-   Added simple_tag 'query' to blog_tags template tags.
-   Added package django-mysql-utf8mb4 and monkey patch for mysql connection to use charset utf8mb4_unicode_520_ci.
-   Fixed char length for utf8mb4 and added monkey patch for creating index, see https://code.djangoproject.com/ticket/18392

    1.1.6:

-   Added ArticleTagAdmin to admin view and functions escape_raw() and unescape_raw() to blog_tags templatetags.

    1.1.5:

-   Created functions for dynamically adding AJAX articles to url patterns. Easy to configure with settings, but still needs restart of django service to recognize for menu.

    1.1.4:

-   Added new settings type for 'Template Item' and created function to load templates from database.
-   Bugfix for widgets and 'App Stylesheets'.
-   Re-create sample_data.json file.

    1.1.3:

-   Added new field 'is_body_title_hidden' for Article and News model. Changed templates to hide the body's title.
-   Bugfix for boolean field for Django-Jet.

    1.1.2:

-   Replaced function based view for contact with class based view, to be able to extend this view. Added new contact template: contact-response-template.txt

    1.1.1:

-   Replaced template tag 'getWebSettingItemsFor' with 'getCoreSettingItemsFor'. This takes advantage of the new Settings model in JSON format.
-   Bugfix for simple_menu to mark selected menu entry as active, or select the parent if selected menu is a sub menu entry.

    1.1.0:

-   Removed djace_editor and integrated node packages for ace-builds and jsoneditor.
-   Added model for Settings, based on JSONField. This is a future replacement for some WebSettings models.
-   Webpack optimizations

    1.0.9:

-   Renamed template-tags _custom_tags_ to _blog_tags_. Make sure, to update your template tags for articles!
-   Fixed javascript error on tagify input field in article and news. This field shows now an autocomplete list of all existing tags.
-   Added template-tags for _string_extenstions_.
-   Fixed cache handling for WebSettings
-   Added pagination settings for NEWS, ARTICLES, etc. See settings_secure.example.py.

    1.0.8:

-   Replaced bundle handling by _django-pipeline_ with webpack.
-   Renamed menu item _imprint_ to _disclosures_. Make sure, to update your article slug and websettings link object!
-   Styles and javascript files can now be stored and loaded from websettings database models. See database upgrade files _/.development/mysql/update_to_v108.sql_ and _/fixtures/update_fixtures_v108.json_

    1.0.7:

-   Rebuild contact form to use AJAX handling.
-   Added _ajax-setup.js_ for further includes of modules.
-   Added field for sender to AntiSpamTrainingData model.

    1.0.6:

-   Added package _django-pipeline_ to bundle javascript and style files for project.

    1.0.5:

-   Added _package.json_ to manage javascript files of third party vendors.
