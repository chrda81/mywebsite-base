## MyWebsite

This is the WEB application for my business. It is written in Python/Django. There are many Content Management Systems (CMS) out there, but
I wanted a small, scaleable and fast platform developed by me. I'm trying to use less external modules and Javascripts to have less security
concerns.

---

License agreement:

Copyright (C) 2018 dsoft-app-dev.de and friends.

This Program may be used by anyone in accordance with the terms of the
German Free Software License

The License may be obtained under [http://www.d-fsl.org](http://www.d-fsl.org).

---

If you like my continuous work, I would appreciate a donation. You can find several options on my website
[https://dsoft-app-dev.de](https://dsoft-app-dev.de) at section _crowdfunding_. Thank you!

### Table of contents

[Configuration](#configuration)<br />
[Administration Area](#administration)<br />
[Website Settings and Locals](#website_settings)<br />
[Creating Menu Items for Navigation](#menu_settings)<br />
[Editing News and Articles](#editing)<br />
[Editing Projects](#editing_projects)<br />
[Version History for Modified Items](#versionhistory)<br />
[SEO - Search Engine Optimization](#seo)

### <a name="configuration"></a>Configuration

The main settings can be done in the file **/website/settings/base.py**, which includes another configuration file for storing secure
settings, e.g. mail server configuration, database connection, the applications _SECRET_KEY_ and so forth. The file
**/website/settings/settings_secure.py** isn't part of the source code version control system, like the rest. So you have initially create your own
from the template file **/website/settings/settings_secure.example.py**.

This application can handle multiple languages, which can be configured in the base file:

```python
LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
)

PARLER_LANGUAGES = {
    None: (
        {'code': 'en', },
        {'code': 'de', },
    ),
    'default': {
        # defaults to PARLER_DEFAULT_LANGUAGE_CODE
        'fallback': 'de',
        # the default; let .active_translations() return fallbacks too,
        'hide_untranslated': False,
    }
}
```

There is shown a small flag symbol in the menu of the website for every configured language, where you can switch the current display language.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/menu-flags.png"/>
</div>

Depending on which additional language is configured, a separate tab for the language is displayed in the modules for creating news and
articles.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/language-tabs.png"/>
</div>

If you provide the application on your web server, please make sure, to list all possible domains you have configured for this application in
your servers vhost configuration, e.g.:

```python
ALLOWED_HOSTS = [your-fancy-domain.name, www.your-fancy-domain.name]
```

The application supports caching database objects to minimize the database load using the memcached service. To see more details, you can
find an instruction for
[Memcached Installation and Configuration with PHP on Debian server](https://www.pontikis.net/blog/install-memcached-php-debian).
The part for setting memcached up for PHP can be ignored, because we are using Python instead. All necessary packages are provided by the
file **/requirements.prod**. The options can be set in the file **/website/settings/settings_secure.py**:

```python
PARLER_ENABLE_CACHING = True
SITES_ENALBE_CACHING = True
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'KEY_PREFIX': 'mysite.production',  # Change this
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 24*3600   # cache data for 24 hours, then fetch them once again
    },
}
```

The option _PARLER_ENABLE_CACHING_ enables the cache for all translation objects of the **News**, **Articles**, **Categories** and
**Project** models. Enabeling the option _SITES_ENALBE_CACHING_ will cache all **Website settings** objects.

::: hljs
**Hint:**
As you can see, the cache has a timeout. Only after the timeout has expired the queries will be sent once again to the database
to refresh the cache. Please remember this behavior when changing settings in the **Website settings**. Do you want to re-new the cache
manually, you may have to restart the memcached service or temporarily disable the _SITES_ENALBE_CACHING_ option and your webserver.
:::

### <a name="administration"></a>Administration Area

Normally, the admin interface can be opened by entering the address of your website, followed by /admin, e.g.
[https://your-fancy-domain.name/admin](https://your-fancy-domain.name/admin). You will be prompted for an username, password and if you have
registered a second factor authorization device (2FA), you also have to enter the one time password (OTP). If you have no 2FA registration,
then just leave this field blank.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3" src="/static/mywebsite/img/documentation/admin-login.png"/>
</div>

After you've logged in, you can see the main panel with some modules, containing information about, e.g. the users last login, the last
recent actions and so forth. There is also a sidebar which could be pinned and unpinnd on the left, showing our main functions.

<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-top.png"/>
  </div>
  <div class="col-md-7 text-justify">

Provides standard links for

-   showing module page
-   leaving the administration area and visiting the website
-   show the documentation about this web application
-   change the language, if several have been configured (see section [Configuration](#configuration))
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <img src="/static/mywebsite/img/documentation/admin-sidebar-auth.png"/>
      </div>
      <div class="col-md-7 text-justify">

The _Authentication and Authorization_ section is providing the user management functions. Here you can create, delete and modify the user
accounts for this application and delegate the necessary rights. If you want to activate a 2FA code for dedicated users, this can be done in
_Django OTP_.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-filebrowser.png" />
  </div>
  <div class="col-md-7 text-justify">

The _Filebrowser_ can be used to upload content to your website. You can create folders and subfolders which are relative to path **/media**.
The filebrowser is also available in the TinyMCE widget, when editing articles and news.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-hitcount.png" />
  </div>
  <div class="col-md-7 text-justify">

Everytime a page of your website is loaded, some information about the user (e.g. IP address, the browser's user agent, ..) is collected.
You can define some threshold values for counting the same IP/User Agent within a specific period and how long the data is stored within the
database. The settings can be found in the file **/website/settings/base.py**:

```python
HITCOUNT_KEEP_HIT_ACTIVE = {'minutes': 60}
HITCOUNT_HITS_PER_IP_LIMIT = 0  # unlimited
HITCOUNT_EXCLUDE_USER_GROUP = ()  # not used
HITCOUNT_KEEP_HIT_IN_DATABASE = {'seconds': 10}
```

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-revision.png" />
  </div>
  <div class="col-md-7 text-justify">

Saving changes to articles and news will create a backup copy of the previous version. Doing so, changes can be historically tracked,
reviewed and rolled back, if necessary.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-home.png" />
  </div>
  <div class="col-md-7 text-justify">

This is the main module of the website. News are 'hot notes' on the index page, but cannot be shared via social media plugins and contain
only one message section. Within _Website settings_ you are able to define some dynamic items within the website, e.g. Sidebar items,
Banner items, Meta tags and so fort. The website can also handle dynamic text items, which can be created, deleted or modified within
_Website settings locale_.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-blog.png" />
  </div>
  <div class="col-md-7 text-justify">

This module is used to define article categories and for writing articles, which can be shared via social media plugins. Articles have a
teaser and a body section, which both are combined in the full view. The teaser is only shown as intro on list pages.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-project.png" />
  </div>
  <div class="col-md-7 text-justify">

This module can be used to list and describe your own projects. You can add some milestones and workitems and show the progress of your work
to people who are interested in.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-bottom.png" />
  </div>
  <div class="col-md-7 text-justify">

Here you can add your own shortcuts to websites.

  </div>
</div>

### <a name="website_settings"></a>Website Settings and Locals

[Example 1 - Creating Meta Tags](#websettings_example1)<br />
[Example 2 - Creating Slider Items](#websettings_example2)

**Website Settings** can be searched by its _Title_, and _Value_ contents. The list view can also be filtered by the column entries
_SettingsType_, _ValueType_ and _Attribute_. If the column _Active_ is checked, the setting is used in the HTML templates for your website.
These settings can be used in a creative way, so think outside of the box, when adding them to your HTML templates.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-view.png"/>
</div>

There are several _SettingsTypes_ which can be configured:

1. Advertising Item
2. Menu Link
3. Meta Tag
4. Slider Item

For every _SettingsType_ you can chose a _Name_, _Attribute_ and a _ValueType_.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-detail-view.png"/>
</div>

#### <a name="websettings_example1"></a>Example 1 - Creating Meta Tags

In the screenshot above, we've defined a single meta tag element for our website called &laquo;Twitter Tags&raquo;, which has an _Attribute_
called &laquo;twitter:description&raquo;. The attribute's _Value_ is equal &laquo;META*DESCRIPTION&raquo;, which is of \_ValueType*
&laquo;Translation&raquo;. Values of this type are fetched from _Website Settings Locals_, where _Value_ is used as lookup key for _MSGID_.
**Website Settings Locals** can be searched by its _MSGID_ and _MSGSTR_.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-locals-view.png"/>
</div>

You can set the same _Name_ for **Website Settings** items more than once. This means, that you can define multiple meta tag items
using the name &laquo;Twitter Tags&raquo;, which then are collected by the template tag function
[_getCoreSettingItemsFor_](/admin/doc/tags/#websettings_tags-getCoreSettingItemsFor) and grouped by the _Name_ in the HTML template
_/templates/toods/home/includes/meta-tags.html_. As result you get a list of &laquo;Twitter Tags&raquo;, which could be processed in
sequence. The single list objects have to be splitted into its single _Attributes_ with
[_getWebSettingsAttribItems_](/admin/doc/filters/#websettings_tags-getWebSettingsAttribItems).

The _Attribute_ field's content could be returned with the template tag filter
[_getAttribItemKey_](/admin/doc/filters/#websettings_tags-getAttribItemKey) and the _Value_ field content could
be returned with the template tag filter [_getAttribItemValue_](/admin/doc/filters/#websettings_tags-getAttribItemValue). In addition we have
to handle the correct returned value's data type, using [_handleWebSettingsType_](/admin/doc/filters/#websettings_tags-handleWebSettingsType).
Here is a full example:

{% verbatim %}

```html
<!-- Twitter Meta Tags -->
{% getCoreSettingItemsFor _('Meta Tag') 'Twitter Tags' as object_list %} {% for
object in object_list %} {% for obj in object|getWebSettingsAttribItems %}
<meta
    name="{{ obj|getAttribItemKey }}"
    content="{{ obj|getAttribItemValue|handleWebSettingsType }}"
/>
{% endfor %} {% endfor %}}
```

{% endverbatim %}

Depending on your database data, this snippet could be rendered like this HTML code:

```html
<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://your-fancy-domain.name" />
<meta name="twitter:title" content="Your fancy website title" />
<meta name="twitter:description" content="Your fancy website description" />
<meta
    name="twitter:image"
    content="https://your-fancy-domain.name/media/images/logo.png"
/>
```

#### <a name="websettings_example2"></a>Example 2 - Creating Slider Items

In the first screenshot of [Website Settings and Locals](#website_settings) we can see some items called &laquo;Parallax-Slider Item&raquo;. In the
HTML template _/templates/toods/home/includes/parallax-slider.html_ we are collecting and grouping this items by using the template tag
function [_getCoreSettingItemsFor_](/admin/doc/tags/#websettings_tags-getCoreSettingItemsFor). Here is a full sample:

{% verbatim %}

```html
{% getCoreSettingItemsFor _('Slider Item') 'Parallax-Slider Item' as object_list
%}
<ol class="carousel-indicators">
    {% for object in object_list %}
    <li
        data-target="#myCarouselIndicators"
        data-slide-to="{{ forloop.counter0 }}"
        class="{% if forloop.first %}active{% endif %}"
    ></li>
    {% endfor %}
</ol>
<div class="carousel-inner">
    {% for object in object_list %}
    <div
        class="carousel-item client-carousel-item {% if forloop.first %}active{% endif %}"
    >
        {% if object.text %}
        <figure>
            <i class="{{ object.class|handleWebSettingsType }}"></i>
        </figure>
        <h3>{{ object.header|handleWebSettingsType }}</h3>
        <p>{{ object.text|handleWebSettingsType }}</p>
        {% elif object.src %}
        <img
            class="{{ object.class|handleWebSettingsType }}"
            src="{{ object.src|handleWebSettingsType }}"
            alt=".."
        />
        {% endif %}
    </div>
    {% endfor %}
</div>
```

{% endverbatim %}

The HTML template from above can render 2 different types of slider items:

1. _Text slider_, which needs HTML attributes 'class', 'header' and 'text'
2. _Image slider_, which needs HTML attributes 'class' and 'src'

The needed _Attributes_ are part of the object*list items and can be extracted with the template filter
[\_handleWebSettingsType*](/admin/doc/filters/#websettings_tags-handleWebSettingsType).

### <a name="menu_settings"></a>Creating Menu Items for Navigation

The menu items can be configured in the file **/home/menus.py**, but there are a few more dependencies. Here is a small example:

```python
...
# Define children for the ABOUT menu
disclosures_children = (
    MenuItem(_("MENU_DATA_PRIVACY"),
             reverse('home:data-privacy'),
             weight=10,
             kwargs={'reverse_url': 'home:data-privacy'}),
...
# Add items to our main menu
Menu.add_item("main", MenuItem(_("MENU_HOME"),
                               reverse('home:index'),
                               weight=10,
                               kwargs={'reverse_url': 'home:index'}))

...
```

The menu entries are generated using the Django package _django-simple-menu_. Please have a detailed look into the
[Documentation](https://django-simple-menu.readthedocs.io/en/latest/). Important for the configuration is that you define the URL in the
format 'namespace: url-name' once in the line _reverse()_ and another time in the line _kwargs_, as value for 'reverse_url'.

Behind each URL name is a view, which is configured in the file **/home/views.py**. As an example, let's take a look at the view for
the menu entry 'home:data-privacy':

```python
...
def data_privacy(request):
    # Collect WebSettings for this menu link
    obj = getCoreSettingItemsFor(settingstype=_('Menu Link'), name='data_privacy')

    data = None
    if obj:
        # Get first object from list. Btw., we should always have only one object!
        obj = obj[0]

        if 'slug' in obj.keys():
            # We are using the blog app instead of a fixed template
            data = blogmodels.Article.objects.active_translations().get(
                slug__iexact=handleWebSettingsType(obj['slug']))

    # To return the correct article hits, we use the AJAX method of hitcount
    return render(request, 'blog/article_ajax.html', {'article': data})
...
```

Each view must be known as an URL in Django. The mapping between URL and View takes place in the file **/home/urls.py**. Here is one
snippet:

```python
...
app_name = 'home'

urlpatterns = [
    path('', views.NewsListView.as_view(), name='index'),
    ...
    path('data-privacy', views.data_privacy, name='data-privacy'),
    path('contact', views.contact, name='contact'),
    ...
]
```

For some menu items **Articles** are defined, as in the example above with the menu entry 'home:data-privacy'. Which menu entries are
available, can be found in the file **/home/views.py**, or if you have a look into the **Website settings** by using the filter
_SettingsTypes_ = 'Menu Link'. To create a mapping between the menu item and an existing **Article**, you have to configure the 'Menu Link'
item like that:

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-menu-item.png"/>
</div>

The menu item 'home:data-privacy' thus refers to an **Article** with the slug attribute _data-privacy_.

::: hljs
**Note:**
Please do not forget to create the translations either statically with the help of
[LC_MESSAGES](https://djangobook.com/localization-create-language-files/) or with the **Website Settings Locals** if you have configured
several languages. As _Msgid_ you have to use an unique name in the file **/home/menus.py**, e.g. \_('NEW_MENU') for LC_MESSAGES or
transFromWebSettings('NEW_MENU') for **Website Settings Locals**. After the changes to the above files, you need to restart your web server
for the changes to take effect.
:::

### <a name="editing"></a>Editing News and Articles

You can add, delete and modify news and articles by clicking **News** or **Article** on the sidebar. Depending of the type you want to
modify, the view is slightly different.

**News** can be searched by its _Title_ and _Body_ contents. If the column _Published_ is checked, the news item is shown on the index page.
If the column _Archived_ is checked, the news item is <ins>not</ins> shown on the index page, even if _Published_ is checked!

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/news-view.png"/>
</div>

**Articles** can be searched by its _Title_, _Teaser_ and _Body_ contents. The list view can also be filtered by the column entries
_Category_ and _OnNewsFeed_. If the column _Internal_ is checked, the article will neither show in the articles nor categories lists, but it
will be found by the global website search, if the search criteria matches. In most cases, the internal articles are used in the menu.
If the column _OnNewsFeed_ is checked, the article is also shown on the index page. The columns _Publish_ and _Archived_ work the same as
for the news items.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/article-view.png"/>
</div>

In section [Configuration](#configuration) of this manual, you can see that there are tabs for each configured language. So it is possible
to write the news and articles in different languages. Affected are the fields _Title_, _Teaser_ for **News**, _Title_, _Teaser_, _Body_ and
_Keywords_ for **Articles** and _Name_ for **Categories**.

::: hljs
**Hint:** When editing news and articles you can also edit the field _Author_. If you leave it blank, the current username is inserted
automatically. The real username for the admin account is always set to &laquo;admin&raquo; for security purpose if it contains the string
'admin', because the author field is shown on the website pages. If someone knows the URL for the administration area, a brute force attack
could be started with the real admin's name. So it is recommended, to change the administrators name to something different, e.g.
&laquo;SuperAdmin&raquo; or &laquo;AdminFromHell&raquo;.
:::

### <a name="editing_projects"></a>Editing Projects

You can add, delete and modify **Projects** and relating **Milestones** including depending **WorkItems**.

**Projects** can be searched by _Ttitle_ and _Description_.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/project-view.png"/>
</div>

**Milestones** can only be searched by its _Title_. They can also be filteres by its _State_.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/milestone-view.png"/>
</div>

**WorkItems** can be searched by _Name_ and _Description_. It is also possible to filter **WorkItems** by _Milestone_ and _State_.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/workitem-view.png"/>
</div>

In section [Configuration](#configuration) of this manual, you can see that there are tabs for each configured language. So it is possible
to translate the projects in different languages. Affected are the fields _Title_, _Description_ for **Projects**, _Title_ for **Milestones**
and _Name_, _Description_ for **WorkItems**.

The data is shown on the website as a top-down timeline, where you can drill down into details.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/project-timeline.png"/>
</div>

### <a name="versionhistory"></a>Version History for Modified Items

Items like **News**, **Articles**, **Categories**, **Projects**, **Milestones** and **WorkItems** have the capability to track their
changes in a version history. To open this _History_, you have to select the preferred item for editing. Now you can see the _History_
button in the upper right corner.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-button.png"/>
</div>

The history view shows a list of the last changes for the selected item from above. You will find the language of the changed object in the
column _Object_. The column _Comment_ will give you a little hint, what information has changed. You can now select two lines and compare
them against each other. It makes more sense to choose two columns of the same language!

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-view.png"/>
</div>

Normally you can now see what text lines have been changed. In the example below, the character 3 at the end of the line was replaced by a
dot.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-details.png"/>
</div>

You can also select a whole revision from the history view by clicking on the link in the _Date/Time_ column. After that, you can hit the
save button to revert to this version.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-revert.png"/>
</div>

### <a name="seo"></a>SEO - Search Engine Optimization

It is very important to configure the correct _Meta Tags_, otherwise a search engine like Google won't find your website. The global
_Meta Tags_ for your website can be configured in **Website Settings** like shown in [Example 1 - Creating Meta Tags](#websettings_example1).

The _Meta Tags_ for your **Articles** are dynamically created. Here is an overview of all available tags:

{% verbatim %}

```html
{% block head %}
<meta name="keywords" content="{{ article.meta_keywords|safe }}" />
<meta name="description" content="{{ article.teaser|striptags|safe }}" />
<meta name="author" content="{{ article.author|safe }}" />

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="{{ article.title|safe }}" />
<meta itemprop="description" content="{{ article.teaser|striptags|safe }}" />
{% getCoreSettingItemsFor _('Meta Tag') 'Google Tags' as object_list %} {% with
object_list|getWebSettingsNamedAttrib:'image'|getAttribItemValue|handleWebSettingsType
as google_image %}
<meta itemprop="image" content="{{ google_image }}" />
{% endwith %}

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ request.build_absolute_uri }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ article.title|safe }}" />
<meta property="og:description" content="{{ article.teaser|striptags|safe }}" />
<meta
    property="og:image"
    content="https://your-fancy-domain.name/media/images/logo.png"
/>
<meta property="og:locale" content="de_DE" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="{{ request.build_absolute_uri }}" />
<meta name="twitter:title" content="{{ article.title|safe }}" />
<meta
    name="twitter:description"
    content="{{ article.teaser|striptags|safe }}"
/>
<meta
    name="twitter:image"
    content="https://your-fancy-domain.name/media/images/logo.png"
/>
{% endblock %}
```

{% endverbatim %}

As you can see, there are the _Meta Tags_:

-   **title** is not listed here, because it is always taken from the **Website Settings Locals** entry called _WEBSITE_TITLE_
-   **keywords** are created from the field _Meta Keywords_
-   **description** is created from the field _Teaser_. Attention: The content is truncated after 160 characters!
-   **name** is created from the field _Title_
-   **author** is created from the field _Author_
-   **image** can be set via **Website Settings** template filter [_getWebSettingsNamedAttrib_](/admin/doc/filters/#websettings_tags-getWebSettingsNamedAttrib) (see the Google tags example above), or with static URL
