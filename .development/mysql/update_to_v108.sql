use djangodb;
update home_websitesettings set name = 'Menu Item disclosures', value = 'disclosures' where name = 'Menu Item imprint';
insert into home_websitesettings ('date', 'user', 'name', 'attribute', 'settingstype', 'value', 'valuetype', 'is_active')
    values ('2019-02-28', 'admin', 'Stylesheet 01 (Google Fonts)', 'href', 5, 'https://fonts.googleapis.com/css?family=PT+Sans|Roboto', 1, 1);
insert into home_websitesettings ('date', 'user', 'name', 'attribute', 'settingstype', 'value', 'valuetype', 'is_active')
    values ('2019-02-28', 'admin', 'Stylesheet 02 (Custom Style)', 'content', 5, 'Custom Style', 8, 1);
