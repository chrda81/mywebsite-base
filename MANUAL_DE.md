## MyWebsite

Dies ist die WEB-Anwendung für mein Unternehmen. Sie ist in Python/Django geschrieben. Es gibt viele Content Management Systeme (CMS),
diese Applikation nutzt aber eine kleine, skalierbare und schnelle Plattform, die von mir entwickelt wurde. Es wird Wert auf wenige externe
Module und Javascripts gelegt, um weniger Sicherheitslücken zu ermöglichen.

---

Lizenzvereinbarung:

Copyright (C) 2018 dsoft-app-dev.de and friends.

Dieses Programm kann von jedem gemäß den Bestimmungen der
Deutsche Freie Software Lizenz genutzt werden.

Die Lizenz kann unter [http://www.d-fsl.org](http://www.d-fsl.org) erhalten werden.

---

Wenn Dir meine kontinuierliche Arbeit an der Anwendung gefällt, wäre ich dankbar für eine Spende. Du kannst mehrere Optionen auf meiner
Website [https://dsoft-app-dev.de](https://dsoft-app-dev.de) im Bereich _Crowdfunding_ finden. Vielen Dank!

### Inhaltsverzeichnis

[Konfiguration](#configuration)<br />
[Adminbereich](#administration)<br />
[Website Settings und Übersetzungen](#website_settings)<br />
[Menüeinträge für Navigation erstellen](#menu_settings)<br />
[News und Artikel bearbeiten](#editing)<br />
[Projekte bearbeiten](#editing_projects)<br />
[Versionshistorie für geänderte Elemente](#versionhistory)<br />
[SEO - Search Engine Optimization](#seo)

### <a name="configuration"></a>Konfiguration

Die Haupteinstellungen können in der Datei **/website/settings/base.py** vorgenommen werden, die eine weitere Konfigurationsdatei zum
Speichern von vertraulichen Einstellungen verwendet, z.B. Mail-Server-Konfiguration, Datenbankverbindung, den _SECRET_KEY_ der Anwendung und
so weiter. Die Datei **/website/settings/settings_secure.py** ist nicht Teil der Source-Code Versionsverwaltung, wie der Rest. Du musst also
zunächst eine eigene aus der Vorlagendatei **/website/settings/settings_secure.example.py** erstellen.

Diese Anwendung kann mehrere Sprachen bereitstellen, die in den Haupteinstellungen konfiguriert werden können:

```python
LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
)

PARLER_LANGUAGES = {
    None: (
        {'code': 'en', },
        {'code': 'de', },
    ),
    'default': {
        # defaults to PARLER_DEFAULT_LANGUAGE_CODE
        'fallback': 'de',
        # the default; let .active_translations() return fallbacks too,
        'hide_untranslated': False,
    }
}
```

Im Menü der Website wird für jede konfigurierte Sprache ein kleines Flaggensymbol angezeigt, mit dem Du die aktuelle Anzeigesprache wechseln
kannst.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/menu-flags.png"/>
</div>

Abhängig davon, welche zusätzliche Sprache konfiguriert ist, wird in den Modulen für das Erstellen von Nachrichten und Artikeln, eine
separate Registerkarte für die Sprache angezeigt.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/language-tabs.png"/>
</div>

Wenn Du die Anwendung auf Deinem Webserver bereitstellst, stelle bitte sicher, dass Du alle möglichen Domäns angibst, die Du für diese
Anwendung in deinen vHost Einstellungen des Server konfiguriert hast, z.B .:

```python
ALLOWED_HOSTS = [your-fancy-domain.name, www.your-fancy-domain.name]
```

Die Anwendung unterstützt das Zwischenspeichern von Datenbankobjekten, um die Datenbank-Abfragen mit Hilfe des memcached-Dienst zu minimieren.
Um Näheres zu erfahren, findest Du hier eine Anleitung dafür [Memcached Installation and Configuration with PHP on Debian server](https://www.pontikis.net/blog/install-memcached-php-debian). Der Teil zum Einrichten von memcached für PHP kann ignoriert werden, da wir
stattdessen Python verwenden. Alle notwendigen Pakete werden von der Datei **/requirements.prod** bereitgestellt. Die Optionen können in der
Datei **/website/settings/settings_secure.py** eingestellt werden:

```python
PARLER_ENABLE_CACHING = True
SITES_ENALBE_CACHING = True
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'KEY_PREFIX': 'mysite.production',  # Change this
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 24*3600   # cache data for 24 hours, then fetch them once again
    },
}
```

Die Option _PARLER_ENABLE_CACHING_ aktiviert den Cache für alle Übersetzungsobjekte der **News**, **Article**, **Categories** und
**Project** Modelle. Wenn Du die Option _SITES_ENALBE_CACHING_ aktivierst, werden alle **Website settings** Objekte zwischengespeichert.

::: hljs
**Hinweis:**
Wie Du siehst, ist für den Cache ein Timeout eingestellt. Erst nach Ablauf des Timeouts werden die Abfragen einmalig erneut an die Datenbank
gestellt, um den Cache zu erneuern. Bitte beachte dieses Verhalten bei Änderungen an Einstellungen in den **Website settings**. Willst du den
Cache eher erneuern, musst du evtl. den memcached-Dienst neustarten oder die Option _SITES_ENALBE_CACHING_ temporär deaktivieren und deinen
Webserver neustarten.
:::

### <a name="administration"></a>Adminbereich

Normalerweise kann der Adminbereich durch Eingabe der Adresse Deiner Website, gefolgt von /admin, z.B.
[https://your-fancy-domain.name/admin](https://your-fancy-domain.name/admin) aufgerufen werden. Du wirst aufgefordert, einen Benutzernamen,
ein Passwort einzugeben und wenn Du ein Zwei-Faktor-Autorisierungs-Gerät (2FA) registriert hast, musst Du auch das Einmal-Passwort (OTP)
ablesen und angeben. Wenn Du keine 2FA-Registrierung aktiviert hast, dann lasse dieses Feld einfach leer.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3" src="/static/mywebsite/img/documentation/admin-login.png"/>
</div>

Nachdem Du dich angemeldet hast, kannst Du das Hauptfenster mit einigen Modulen sehen, die Informationen über z.B. die zuletzt eingeloggten
Benutzer, zuletzt ausgeführte Aktionen und so weiter, anzeigen. Es gibt auch eine Seitenleiste, die links angeheftet und gelöst werden kann und die
Hauptfunktionen der Anwendung anzeigt.

<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-top.png"/>
  </div>
  <div class="col-md-7 text-justify">

Bietet eine Standardverlinkung, um

-   die Modulseite anzuzeigen
-   den Adminbereich zu verlassen und die Website zu besuchen
-   die Dokumentation dieser Webanwendung anzuzeigen
-   die Sprache zu wechseln, falls mehrere konfiguriert wurden (siehe Abschnitt [Konfiguration](#configuration))
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <img src="/static/mywebsite/img/documentation/admin-sidebar-auth.png"/>
      </div>
      <div class="col-md-7 text-justify">

Der Abschnitt _Authentication and Authorization_ stellt die Funktionen der Benutzerverwaltung bereit. Hier kannst Du Benutzer anlegen,
löschen, ändern und für diese Anwendung erforderlichen Rechte delegieren. Wenn Du einen 2FA-Code für ausgewählte Benutzer aktivieren möchtest,
kannst Du dies unter _Django OTP_ tun.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-filebrowser.png" />
  </div>
  <div class="col-md-7 text-justify">

Der _Filebrowser_ kann verwendet werden, um Inhalte auf Deine Website hochzuladen. Du kannst Ordner und Unterordner erstellen, die relativ zum
Pfad **/media** sind. Der Filebrowser ist auch im TinyMCE-Widget verfügbar, wenn Du Artikel und Nachrichten bearbeitest.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-hitcount.png" />
  </div>
  <div class="col-md-7 text-justify">

Jedes Mal, wenn eine Seite Deiner Website geladen wird, werden einige Informationen über den Benutzer (z.B. die IP-Adresse, der Agent des
Browsers usw.) gesammelt. Du kannst einige Schwellenwerte für das Zählen der gleichen IP/des Agenten innerhalb eines bestimmten Zeitraumes
definieren und wie lange die Daten in der Datenbank gespeichert werden. Die Einstellungen dazu findest Du in der Datei
**/website/settings/base.py**:

```python
HITCOUNT_KEEP_HIT_ACTIVE = {'minutes': 60}
HITCOUNT_HITS_PER_IP_LIMIT = 0  # unlimited
HITCOUNT_EXCLUDE_USER_GROUP = ()  # not used
HITCOUNT_KEEP_HIT_IN_DATABASE = {'seconds': 10}
```

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-revision.png" />
  </div>
  <div class="col-md-7 text-justify">

Wenn Du Änderungen an Artikeln und Nachrichten speicherst, wird eine Sicherungskopie der vorherigen Version erstellt. Auf diese Weise können
Änderungen bei Bedarf historisch verfolgt, überprüft und rückgängig gemacht werden.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-home.png" />
  </div>
  <div class="col-md-7 text-justify">

Dies ist das Hauptmodul der Website. News sind "brandheiße Notizen" auf der Indexseite, können aber nicht über Social-Media-Plugins geteilt
werden und enthalten nur einen Nachrichtenabschnitt. Unter **Website settings** kannst Du einige dynamische Elemente innerhalb der Website
definieren, z.B. Sidebar-Elemente, Banner-Elemente, Meta-Tags und so weiter. Die Website kann auch dynamische Textelemente behandeln, die
unter **Website settings locale** erstellt, gelöscht oder geändert werden können.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-blog.png" />
  </div>
  <div class="col-md-7 text-justify">

Dieses Modul dient zur Definition von Artikelkategorien und zum Schreiben von Artikeln, die über Social-Media-Plugins geteilt werden können.
Artikel verfügen über einen Teaser und ein Body-Abschnitt, die beide in der Gesamtansicht kombiniert werden. Der Teaser wird nur als eine Art
Intro auf den Listenseiten angezeigt.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-project.png" />
  </div>
  <div class="col-md-7 text-justify">

Mit diesem Modul kannst Du Deine eigenen Projekte auflisten und beschreiben. Du kannst einige Meilensteine und Tätigkeiten hinzufügen und
den Fortschritt Deiner Arbeit für andere Personen aufzeigen.

  </div>
</div>
<div class="row">
  <div class="col-md-5">
    <img src="/static/mywebsite/img/documentation/admin-sidebar-bottom.png" />
  </div>
  <div class="col-md-7 text-justify">

Hier kannst du deine eigenen Verlinkungen zu Websiten anlegen und verwalten.

  </div>
</div>

### <a name="website_settings"></a>Website Settings und Übersetzung

[Beispiel 1 - Meta-Tags erstellen](#websettings_example1)<br />
[Beispiel 2 - Slider Items erstellen](#websettings_example2)

**Website settings** können nach ihrem Inhalt _Title_ und _Value_ durchsucht werden. Die Listenansicht kann auch nach den Spalteneinträgen
_SettingsType_, _ValueType_ und _Attribut_ gefiltert werden. Wenn die Spalte _Active_ aktiviert ist, wird die Einstellung in den HTML-Vorlagen
für Deine Website verwendet. Diese Einstellungen können auf kreative Art und Weise verwendet werden. Denke beim Hinzufügen zu
Deinen HTML-Vorlagen einfach ein Wenig über den Tellerrand hinaus.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-view.png"/>
</div>

Es gibt mehrere _SettingsTypes_, die konfiguriert werden können:

1. Advertising Item
2. Menu Link
3. Meta Tag
4. Slider Item

Für jeden _SettingsType_ kannst Du einen _Name_, _Attribute_ und einen _ValueType_ auswählen.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-detail-view.png"/>
</div>

#### <a name="websettings_example1"></a>Beispiel 1 - Meta-Tags erstellen

Im obigen Screenshot haben wir ein einzelnes Meta-Tag Item für unsere Website definiert, das &laquo;Twitter Tags&raquo; heißt und über
ein _Attribute_ namens &laquo;twitter:description&raquo; verfügt. Der _Value_ des Attributs ist gleich &laquo;META*DESCRIPTION&raquo; was vom
\_ValueType* &laquo;Tranlation&raquo; ist. Werte dieses Typs werden von **Website settings locale** abgerufen, wobei _Value_ als Suchschlüssel
für _MSGID_ verwendet wird. **Website settings locale** können mittels _MSGID_ und _MSGSTR_ durchsucht werden.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-locals-view.png"/>
</div>

Du kannst den gleichen _Name_ für **Website settings** mehrfach festlegen. Dies bedeutet, dass Du mehrere Meta-Tag Items mit dem Namen
&laquo;Twitter Tags&raquo; definieren kannst, die dann von der Template-Tag Funktion
[_getCoreSettingItemsFor_](/admin/doc/tags/#websettings_tags-getCoreSettingItemsFor) gesammelt und nach _Name_ in der HTML-Vorlage
_/templates/toods/home/includes/meta-tags.html_ gruppiert werden. Als Ergebnis erhältst Du eine Liste von &laquo;Twitter Tags&raquo;, die
sequentiell verarbeitet werden kann. Die einzelnen Listenobjekte müssen in ihre einzelnen _Attribute_ mittels des Template-Filters
[_getWebSettingsAttribItems_](/admin/doc/filters/#websettings_tags-getWebSettingsAttribItems) aufgeteilt werden.

{% verbatim %}

```html
<!-- Twitter Meta Tags -->
{% getCoreSettingItemsFor _('Meta Tag') 'Twitter Tags' as object_list %} {% for
object in object_list %} {% for obj in object|getWebSettingsAttribItems %}
<meta
    name="{{ obj|getAttribItemKey }}"
    content="{{ obj|getAttribItemValue|handleWebSettingsType }}"
/>
{% endfor %} {% endfor %}}
```

{% endverbatim %}

Abhängig von Deinen Datenbankdaten, könnte dieses Snippet ähnlich wie dieser HTML-Code gerendert werden:

```html
<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://your-fancy-domain.name" />
<meta name="twitter:title" content="Your fancy website title" />
<meta name="twitter:description" content="Your fancy website description" />
<meta
    name="twitter:image"
    content="https://your-fancy-domain.name/media/images/logo.png"
/>
```

#### <a name="websettings_example2"></a>Beispiel 2 - Slider Items erstellen

Im ersten Screenshot von [Website Settings und Übersetzungen](#website_settings) können wir einige Elemente vom Typ &laquo;Parallax-Slider Item&raquo;
sehen. In der HTML-Vorlage _/templates/tods/home/includes/parallax-slider.html_ sammeln und gruppieren wir diese Elemente mithilfe der
Template-Tag Funktion [_getCoreSettingItemsFor_](/admin/doc/tags/#websettings_tags-getCoreSettingItemsFor). Hier ist ein vollständiges Beispiel:

{% verbatim %}

```html
{% getCoreSettingItemsFor _('Slider Item') 'Parallax-Slider Item' as object_list
%}
<ol class="carousel-indicators">
    {% for object in object_list %}
    <li
        data-target="#myCarouselIndicators"
        data-slide-to="{{ forloop.counter0 }}"
        class="{% if forloop.first %}active{% endif %}"
    ></li>
    {% endfor %}
</ol>
<div class="carousel-inner">
    {% for object in object_list %}
    <div
        class="carousel-item client-carousel-item {% if forloop.first %}active{% endif %}"
    >
        {% if object.text %}
        <figure>
            <i class="{{ object.class|handleWebSettingsType }}"></i>
        </figure>
        <h3>{{ object.header|handleWebSettingsType }}</h3>
        <p>{{ object.text|handleWebSettingsType }}</p>
        {% elif object.src %}
        <img
            class="{{ object.class|handleWebSettingsType }}"
            src="{{ object.src|handleWebSettingsType }}"
            alt=".."
        />
        {% endif %}
    </div>
    {% endfor %}
</div>
```

{% endverbatim %}

Die HTML-Vorlage von oben kann 2 verschiedene Arten von _Slider Items_ rendern:

1. _Text slider_, der die HTML-Attribute 'class', 'header' und 'text' benötigt
2. _Image slider_, der die HTML-Attribute 'class' und 'src' benötigt

Die benötigten _Attributes_ sind Eigenschaften der Elemente in _object_list_ und können mit dem Template-Filter
[_handleWebSettingsType_](/admin/doc/filters/#websettings_tags-handleWebSettingsType) extrahiert werden.

### <a name="menu_settings"></a>Menüeinträge für Navigation erstellen

Die Menüeinträge können in der Datei **/home/menus.py** konfiguriert werden, es gibt jedoch noch ein paar weitere Abhängigkeiten. Hier ein
kleiner Ausschnitt:

```python
...
# Define children for the ABOUT menu
disclosures_children = (
    MenuItem(_("MENU_DATA_PRIVACY"),
             reverse('home:data-privacy'),
             weight=10,
             kwargs={'reverse_url': 'home:data-privacy'}),
...
# Add items to our main menu
Menu.add_item("main", MenuItem(_("MENU_HOME"),
                               reverse('home:index'),
                               weight=10,
                               kwargs={'reverse_url': 'home:index'}))

...
```

Die Menüeinträge werden mit Hilfe des Django Paketes _django-simple-menu_ generiert. Hier kann man eine ausführliche
[Dokumentation](https://django-simple-menu.readthedocs.io/en/latest/) finden. Wichtig für die Konfiguration ist, dass man die URL im
Format 'namespace:url-name' einmal in der Zeile _reverse()_ angibt und ein anderes Mal in der Zeile für _kwargs_, als Wert für
'reverse_url'.

Hinter jedem URL-Namen steht ein View, welcher in der Datei **/home/views.py** konfiguriert ist. Als Beispiel schauen wir uns den View für
den Menüeintrag 'home:data-privacy' genauer an:

```python
...
def data_privacy(request):
    # Collect WebSettings for this menu link
    obj = getCoreSettingItemsFor(settingstype=_('Menu Link'), name='data_privacy')

    data = None
    if obj:
        # Get first object from list. Btw., we should always have only one object!
        obj = obj[0]

        if 'slug' in obj.keys():
            # We are using the blog app instead of a fixed template
            data = blogmodels.Article.objects.active_translations().get(
                slug__iexact=handleWebSettingsType(obj['slug']))

    # To return the correct article hits, we use the AJAX method of hitcount
    return render(request, 'blog/article_ajax.html', {'article': data})
...
```

Jeder View muss als URL in Django bekannt gemacht werden. Das Mapping zw. URL und View erfolgt in der Datei **/home/urls.py**. Hier ein
Ausschnitt:

```python
...
app_name = 'home'

urlpatterns = [
    path('', views.NewsListView.as_view(), name='index'),
    ...
    path('data-privacy', views.data_privacy, name='data-privacy'),
    path('contact', views.contact, name='contact'),
    ...
]
```

Bei einigen Menüeinträgen sind **Article** hinterlegt, wie oben im Beispiel beim Menüeintrag 'home:data-privacy'. Um welche Menüeinträge es
sich konkret handelt, sieht man in der Datei **/home/views.py**, oder wenn man in den **Website settings** nach dem _SettingsTypes_ =
'Menu Link' filtert. Um nun ein Mapping zw. dem Menüeintrag und einem vorhandenen **Article** herzustellen, konfiguiert man das 'Menu Link'
Element folgendermaßen:

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/settings-menu-item.png"/>
</div>

Der Menüeintrag 'home:data-privacy' verweist also auf einen **Article** mit dem slug-Attribut _data-privacy_.

::: hljs
**Hinweis:**
Bitte vergiss dabei nicht, die Übersetzungen entweder statisch mit Hilfe der
[LC_MESSAGES](https://djangobook.com/localization-create-language-files/) oder über die **Website Settings Locals** zu erzeugen, falls du
mehrere Sprachen konfiguriert hast. Als _Msgid_ verwendest du einen eindeutigen Namen in der Datei **\*/home/menus.py**, z.B. \_('NEW_MENU')
für LC_MESSAGES oder transFromWebSettings('NEW_MENU') für **Website Settings Locals**. Nach den Änderungen an den oben genannten Dateien,
musst Du deinen Webserver neustarten, damit die Änderungen aktiv werden.
:::

### <a name="editing"></a>News und Artikel bearbeiten

Du kannst Nachrichten und Artikel hinzufügen, löschen und ändern, indem Du in der Seitenleiste auf **News** oder **Articles** klickst.
Abhängig vom gewünschten Typ, welcher geändert werden soll, ist die Ansicht etwas unterschiedlich.

**News** können nach ihren Inhalten _Title_ und _Body_ durchsucht werden. Wenn die Spalte _Published_ markiert ist, wird die Nachricht auf
der Indexseite angezeigt. Wenn die Spalte _Archived_ markiert ist, wird die Nachricht auf der Indexseite <ins>nicht</ins> angezeigt, auch wenn
_Published_ markiert ist!

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/news-view.png"/>
</div>

**Articles** können nach den Inhalten _Title_, _Teaser_ und _Body_ durchsucht werden. Die Listenansicht kann auch nach den Spalteneinträgen
_Category_ und _OnNewsFeed_ gefiltert werden. Wenn die Spalte _Internal_ angekreuzt ist, wird der Artikel weder in der Artikel- noch in der
Kategorienliste angezeigt. Er wird aber durch die globale Website-Suche gefunden, wenn die Suchkriterien übereinstimmen. In den meisten Fällen
werden die internen Artikel im Menü verwendet. Wenn die Spalte _OnNewsFeed_ aktiviert ist, wird der Artikel auch auf der Indexseite angezeigt.
Die Spalten _Publish_ und _Archived_ funktionieren genauso für die Nachrichten.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/article-view.png"/>
</div>

Im Abschnitt [Konfiguration](#configuration) dieses Handbuchs siehst Du, dass für jede konfigurierte Sprache eigene Registerkarten vorhanden
sind. Damit ist es möglich, die Nachrichten und Artikel in verschiedenen Sprachen zu übersetzen. Ermöglicht wird das für die Felder _Title_,
_Teaser_ bei **News**, _Title_, _Teaser_, _Body_ und _Keywords_ bei **Articles** und _Name_ bei **Categories**.

::: hljs
**Hinweis:** Beim Bearbeiten von Nachrichten und Artikeln kannst Du auch das Feld _Author_ bearbeiten. Wenn Du das Feld leer lässt, wird der
aktuelle Benutzername automatisch eingefügt. Der echte Benutzername für das Admin-Konto wird aus Sicherheitsgründen immer auf &laquo;admin&raquo;
geändert, wenn die Zeichenfolge 'admin' enthalten ist, da das Autorenfeld auf den Webseiten angezeigt wird. Wenn jemand die URL für den
Administrationsbereich kennt, könnte ein Brute-Force-Angriff mit dem Namen des echten Admins gestartet werden. Daher wird empfohlen, den Namen
des Administrators in einen anderen Namen zu ändern, z.B. &laquo;SuperAdmin&raquo; oder &laquo;AdminFromHell&raquo;.
:::

### <a name="editing_projects"></a>Projekte bearbeiten

Du kannst **Projects** und damit zusammenhängende **Milestones** einschließlich abhängiger **WorkItems** hinzufügen, löschen und ändern.

**Projects** können nach _Tile_ und _Description_ durchsucht werden.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/project-view.png"/>
</div>

**Milestones** können nur nach ihrem _Title_ durchsucht werden. Sie können auch durch ihren _State_ gefiltert werden.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/milestone-view.png"/>
</div>

**WorkItems** können durch _Name_ und _Description_ gesucht werden. Es ist auch möglich, WorkItems nach _Milestone_ und _State_ zu filtern.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/workitem-view.png"/>
</div>

Im Abschnitt [Konfiguration](#configuration) dieses Handbuchs siehst Du, dass für jede konfigurierte Sprache eigene Registerkarten vorhanden
sind. Damit ist es möglich, die Projekte in verschiedenen Sprachen zu übersetzen. Ermöglicht wird das für die Felder _Title_, _Description_
bei **Projects**, _Title_ bei **Milestones** und _Name_, _Description_ bei **WorkItems**.

Die Daten werden auf der Website als Top-down-Timeline angezeigt, auf der Du detaillierte Informationen abrufen kannst.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/project-timeline.png"/>
</div>

### <a name="versionhistory"></a>Versionshistorie für geänderte Elemente

Änderungen der Elemente **News**, **Articles**, **Categories**, **Projects**, **Milestones** und **WorkItems** können in einem Versionsverlauf
nachverfolgt werden. Um diese _History_ zu öffnen, musst Du das gewünschten Element zum Bearbeiten auswählen. Jetzt kannst Du die _History_
Schaltfläche in der oberen rechten Ecke sehen.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-button.png"/>
</div>

Der Versionsverlauf zeigt eine Liste der letzten Änderungen für das ausgewählte Element an (siehe Bsp. oben). Du findest die Sprache des
geänderten Elements in der Spalte _Object_. Die Spalte _Comment_ gibt Dir dabei einen kleinen Hinweis, welche Informationen sich geändert
haben. Du kannst jetzt zwei Zeilen auswählen und diese gegeneinander vergleichen. Es ist sinnvoll zwei Spalten derselben Sprache auszuwählen!

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-view.png"/>
</div>

Normalerweise kannst Du jetzt sehen, welche Textzeilen geändert wurden. Im folgenden Beispiel wurde das Zeichen 3 am Ende der Zeile durch
einen ersetzt Punkt.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-details.png"/>
</div>

Du kannst im Versionsverlauf auch eine komplette Revision auswählen, indem Du in der Spalte _Date/Time_ auf den Link klickst. Danach kannst
Du die Speichern Schaltfläche drücken, um zu dieser Version zurückzukehren.

<div class="img-responsive">
  <img class="d-block img-fluid mt-3 mb-3 w-100" src="/static/mywebsite/img/documentation/version-history-revert.png"/>
</div>

### <a name="seo"></a>SEO - Search Engine Optimization

Es ist sehr wichtig, dass Du die richtigen _Meta Tags_ konfigurierst, sonst findet eine Suchmaschine wie Google Deine Website nicht. Die
globalen _Meta-Tags_ für Deine Website können in **Website settings** wie in [Beispiel 1 - Meta-Tags erstellen](#websettings_example1)
konfiguriert werden.

Die _Meta Tags_ für Deine **Articles** werden dynamisch erstellt. Hier ist eine Übersicht aller verfügbaren Tags:

{% verbatim %}

```html
{% block head %}
<meta name="keywords" content="{{ article.meta_keywords|safe }}" />
<meta name="description" content="{{ article.teaser|striptags|safe }}" />
<meta name="author" content="{{ article.author|safe }}" />

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="{{ article.title|safe }}" />
<meta itemprop="description" content="{{ article.teaser|striptags|safe }}" />
{% getCoreSettingItemsFor _('Meta Tag') 'Google Tags' as object_list %} {% with
object_list|getWebSettingsNamedAttrib:'image'|getAttribItemValue|handleWebSettingsType
as google_image %}
<meta itemprop="image" content="{{ google_image }}" />
{% endwith %}

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ request.build_absolute_uri }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ article.title|safe }}" />
<meta property="og:description" content="{{ article.teaser|striptags|safe }}" />
<meta
    property="og:image"
    content="https://your-fancy-domain.name/media/images/logo.png"
/>
<meta property="og:locale" content="de_DE" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="{{ request.build_absolute_uri }}" />
<meta name="twitter:title" content="{{ article.title|safe }}" />
<meta
    name="twitter:description"
    content="{{ article.teaser|striptags|safe }}"
/>
<meta
    name="twitter:image"
    content="https://your-fancy-domain.name/media/images/logo.png"
/>
{% endblock %}
```

{% endverbatim %}

Wie Du sehen kannst, gibt es folgende _Meta Tags_:

-   **title** ist hier nicht aufgeführt, da dieser immer aus dem Eintrag **Website settings locale** mit dem Namen _WEBSITE_TITLE_ übernommen wird
-   **keywords** werden aus dem Feld _Meta Keywords_ erstellt
-   **description** wird aus dem Feld _Teaser_ erstellt. Achtung: Der Inhalt wird nach 160 Zeichen von der Suchmaschine abgeschnitten!
-   **name** wird aus dem Feld _Title_ erstellt
-   **author** wird aus dem Feld _Author_ erstellt
-   **image** kann über dem **Website settings** Template-Filter [_getWebSettingsNamedAttrib_](/admin/doc/filters/#websettings_tags-getWebSettingsNamedAttrib) (siehe das obige Google-Tag-Beispiel) oder mit einer statischen URL festgelegt werden
